package bd.org.blast.sromikobhijog.response;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/20/2017.
 */


        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class ResponseCallLog {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("call_log_id")
    @Expose
    private Integer callLogId;
    @SerializedName("app_call_log_id")
    @Expose
    private String appCallLogId;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getCallLogId() {
        return callLogId;
    }

    public void setCallLogId(Integer callLogId) {
        this.callLogId = callLogId;
    }

    public String getAppCallLogId() {
        return appCallLogId;
    }

    public void setAppCallLogId(String appCallLogId) {
        this.appCallLogId = appCallLogId;
    }

}