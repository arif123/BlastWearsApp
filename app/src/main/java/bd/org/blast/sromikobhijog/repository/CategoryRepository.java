package bd.org.blast.sromikobhijog.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.util.List;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.model.Category;
import bd.org.blast.sromikobhijog.model.CategoryData;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/24/2017.
 */

public class CategoryRepository {

    private static final String TAG="CategoryRepository";


    public static void insert(Context context, List<Category> list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(Category item: list){
            insert(context, item);
        }
        Log.i(TAG, "insert: "+list.size() );
    }


    public static void insert(Context context, Category item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        ContentValues cv = new ContentValues();
        cv.put(DB.Category.infoSearchCategoryId, item.getInfoSearchCategoryId());
        cv.put(DB.Category.categoryNameEn, item.getCategoryNameEn());
        cv.put(DB.Category.categoryNameBn, item.getCategoryNameBn());
        cv.put(DB.Category.publishStatus, item.getPublishStatus());
        cv.put(DB.Category.updatedAt, item.getUpdatedAt());

        db.insert(DB.Category.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert: "+1 );

    }
}
