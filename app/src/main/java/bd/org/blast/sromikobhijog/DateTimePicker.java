package bd.org.blast.sromikobhijog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Arif on 9/3/2016.
 */

public class DateTimePicker {
    public static class TimePickerFragment extends DialogFragment {

        public void setOnTimeSetListener(TimePickerDialog.OnTimeSetListener onTimeSetListener) {
            this.onTimeSetListener = onTimeSetListener;
        }

        TimePickerDialog.OnTimeSetListener onTimeSetListener;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), onTimeSetListener, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            // Do something with the time chosen by the user
        }
    }

    public static class DatePickerFragment extends DialogFragment {



        DatePickerDialog.OnDateSetListener onDateSetListener;

        public void setOnDateSetListener(DatePickerDialog.OnDateSetListener onDateSetListener) {
            this.onDateSetListener = onDateSetListener;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int year = 0, month = 0, day  =0;


            if (getArguments()!=null ){

                ArrayList<Integer> integerArrayListDob = getArguments().getIntegerArrayList("dob");
                if (integerArrayListDob!=null && integerArrayListDob.size()>0){
                    year = integerArrayListDob.get(0);
                    month = integerArrayListDob.get(1);
                    day = integerArrayListDob.get(2);
                }

            }
            else{
                // Use the current date as the default date in the picker
                final Calendar c = Calendar.getInstance();
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH);
                day = c.get(Calendar.DAY_OF_MONTH);
            }



            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), onDateSetListener, year, month, day);
        }


    }

}
