package bd.org.blast.sromikobhijog.activities;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.List;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.adapter.ExpandableRecyclerViewAdapterInfo;
import bd.org.blast.sromikobhijog.adapter.ExpandableRecyclerViewAdapterSP;
import bd.org.blast.sromikobhijog.model.ServiceProviderCategory;
import bd.org.blast.sromikobhijog.repository.ServiceProviderRepository;

/**
 * Created by This pc on 10/18/2017.
 */

public class ServiceProviderActivity extends AppCompatActivity  {
    private RecyclerView mRecyclerView;
    private ExpandableRecyclerViewAdapterSP mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_recycler_header_footer);

       //enable up navigation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview);


        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        Drawable horizontalDivider = ContextCompat.getDrawable(this, R.drawable.horizontal_divider);
        horizontalDecoration.setDrawable(horizontalDivider);
        mRecyclerView.addItemDecoration(horizontalDecoration);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        List<ServiceProviderCategory> serviceProviderCategoryList = ServiceProviderRepository.getAll(this);

        // String myDataset[] = new String[]{"Task 1", "task 2"};
        // specify an adapter (see also next example)
        mAdapter = new ExpandableRecyclerViewAdapterSP(serviceProviderCategoryList);
        mAdapter.collapseAllSections();
        mRecyclerView.setAdapter(mAdapter);


    }







}
