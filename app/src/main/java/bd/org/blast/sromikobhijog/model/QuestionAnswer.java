
package bd.org.blast.sromikobhijog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuestionAnswer {

    @SerializedName("info_search_topics_id")
    @Expose
    private String infoSearchTopicsId;
    @SerializedName("topics_name_en")
    @Expose
    private String topicsNameEn;
    @SerializedName("topics_name_bn")
    @Expose
    private String topicsNameBn;
    @SerializedName("info_search_ques_ans_id")
    @Expose
    private String infoSearchQuesAnsId;
    @SerializedName("question_en")
    @Expose
    private String questionEn;
    @SerializedName("question_bn")
    @Expose
    private String questionBn;
    @SerializedName("ans_en")
    @Expose
    private String ansEn;
    @SerializedName("ans_bn")
    @Expose
    private String ansBn;
    @SerializedName("publish_status")
    @Expose
    private String publishStatus;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getInfoSearchTopicsId() {
        return infoSearchTopicsId;
    }

    public void setInfoSearchTopicsId(String infoSearchTopicsId) {
        this.infoSearchTopicsId = infoSearchTopicsId;
    }

    public String getTopicsNameEn() {
        return topicsNameEn;
    }

    public void setTopicsNameEn(String topicsNameEn) {
        this.topicsNameEn = topicsNameEn;
    }

    public String getTopicsNameBn() {
        return topicsNameBn;
    }

    public void setTopicsNameBn(String topicsNameBn) {
        this.topicsNameBn = topicsNameBn;
    }

    public String getInfoSearchQuesAnsId() {
        return infoSearchQuesAnsId;
    }

    public void setInfoSearchQuesAnsId(String infoSearchQuesAnsId) {
        this.infoSearchQuesAnsId = infoSearchQuesAnsId;
    }

    public String getQuestionEn() {
        return questionEn;
    }

    public void setQuestionEn(String questionEn) {
        this.questionEn = questionEn;
    }

    public String getQuestionBn() {
        return questionBn;
    }

    public void setQuestionBn(String questionBn) {
        this.questionBn = questionBn;
    }

    public String getAnsEn() {
        return ansEn;
    }

    public void setAnsEn(String ansEn) {
        this.ansEn = ansEn;
    }

    public String getAnsBn() {
        return ansBn;
    }

    public void setAnsBn(String ansBn) {
        this.ansBn = ansBn;
    }

    public String getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
