package bd.org.blast.sromikobhijog.adapter;
/**
 * Created by This pc on 10/11/2017.
 */


import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.afollestad.sectionedrecyclerview.SectionedViewHolder;

import java.util.List;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.activities.InformationDetailsActivity;
import bd.org.blast.sromikobhijog.model.CategoryQuestionList;
import bd.org.blast.sromikobhijog.model.CategoryTopics;
import bd.org.blast.sromikobhijog.model.Question;
import bd.org.blast.sromikobhijog.model.QuestionAnswer;
import bd.org.blast.sromikobhijog.model.Topic;


@SuppressLint("DefaultLocale")
public class ExpandableRecyclerViewAdapterInfo extends SectionedRecyclerViewAdapter<ExpandableRecyclerViewAdapterInfo.MainVH> {


    private final List<CategoryTopics> categoryTopicsList;
    /*String title[] =new String[]{"নিয়োগ",
            "শ্রমিক আইন",
            "মজুরি ",
            "ছুটি"};

    String subtitle[] =new String[]{"নিয়োগ পত্র",
            "সার্ভিস বই ",
            "পরিচয় পত্র",
            "ওভার টাইম"};*/

    public ExpandableRecyclerViewAdapterInfo(List<CategoryTopics> categoryTopicsList) {
        this.categoryTopicsList = categoryTopicsList;
    }


    @Override
    public int getSectionCount() {
        return categoryTopicsList.size();
    }

    @Override
    public int getItemCount(int section) {

        if (categoryTopicsList!= null ){
            CategoryTopics categoryQuestion = categoryTopicsList.get(section);
            if (categoryQuestion!= null) {
                List<Topic> topicList = categoryQuestion.getTopicList();
                if (topicList != null)
                    return  topicList.size();
            }
        }

        return 0;


    }


    @Override
    public void onBindHeaderViewHolder(MainVH holder, int section, boolean expanded) {



        try {
            if (categoryTopicsList.get(section) != null){
                holder.title.setText(categoryTopicsList.get(section).categoryNameBn);
                holder.caret.setImageResource(expanded ? R.drawable.ic_collapse : R.drawable.ic_expand);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {

        }


    }

    @Override
    public void onBindFooterViewHolder(MainVH holder, int section) {
        holder.title.setText(String.format("Section footer %d", section));
    }

    @Override
    public void onBindViewHolder(
            MainVH holder, int section, int relativePosition, int absolutePosition) {
        String questionBn = categoryTopicsList.get(section).getTopicList().get(relativePosition).getTopicsNameBn();
        holder.title.setText( questionBn);//("sub "+String.format("S:%d, P:%d, A:%d", section, relativePosition, absolutePosition));
        // holder.title.setText( categoryQuestionLists.get(section).getQuesList().get(relativePosition).getQuestionBn());
        String.format("S:%d, P:%d, A:%d", section, relativePosition, absolutePosition);




    }


    @Override
    public int getItemViewType(int section, int relativePosition, int absolutePosition) {
        if (section == 1) {
            // VIEW_TYPE_FOOTER is -3, VIEW_TYPE_HEADER is -2, VIEW_TYPE_ITEM is -1.
            // You can return 0 or greater.
            return 0;
        }
        return super.getItemViewType(section, relativePosition, absolutePosition);
    }

    @Override
    public MainVH onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                layout = R.layout.header_info;
                break;
            case VIEW_TYPE_ITEM:
                layout = R.layout.header_info_sub;
                break;
            case VIEW_TYPE_FOOTER:
                layout = R.layout.header_info;
                break;
            default:
                // Our custom item, which is the 0 returned in getItemViewType() above
                layout = R.layout.header_info_sub;
                break;
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new MainVH(v, this);
    }

    class MainVH extends SectionedViewHolder implements View.OnClickListener {

        final TextView title;
        final ImageView caret;
        final ExpandableRecyclerViewAdapterInfo adapter;
        Toast toast;

        MainVH(View itemView, ExpandableRecyclerViewAdapterInfo adapter) {
            super(itemView);
            this.title = itemView.findViewById(R.id.title);
            this.caret = itemView.findViewById(R.id.caret);
            this.adapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (isFooter()) {
                // ignore footer clicks
                return;
            }

            if (isHeader()) {
                adapter.toggleSectionExpanded(getRelativePosition().section());
            } else {
                if (toast != null) {
                    toast.cancel();
                }
                /*toast = Toast.makeText(view.getContext(), getRelativePosition().toString(), Toast.LENGTH_SHORT);
                toast.show();*/

                String topicsId = categoryTopicsList.get(getRelativePosition().section()).getTopicList().get(getRelativePosition().relativePos()).getInfoSearchTopicsId();
                String topics = categoryTopicsList.get(getRelativePosition().section()).getTopicList().get(getRelativePosition().relativePos()).getTopicsNameBn();
                String topicsDescription = categoryTopicsList.get(getRelativePosition().section()).getTopicList().get(getRelativePosition().relativePos()).getTopicsDescriptionBn();
                Intent intent = new Intent(view.getContext(), InformationDetailsActivity.class);
                intent.putExtra("topicsId", topicsId);
                intent.putExtra("topics", topics);
                intent.putExtra("topicsDescription", topicsDescription);

                view.getContext().startActivity(intent);


                //callBack.onItemSelected(questionId, questionTxt);

            }
        }
    }



}