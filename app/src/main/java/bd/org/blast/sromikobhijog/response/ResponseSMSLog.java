package bd.org.blast.sromikobhijog.response;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/26/2017.
 */




public class ResponseSMSLog {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("sms_log_id")
    @Expose
    private Integer smsLogId;
    @SerializedName("app_sms_log_id")
    @Expose
    private String appSmsLogId;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getSmsLogId() {
        return smsLogId;
    }

    public void setSmsLogId(Integer smsLogId) {
        this.smsLogId = smsLogId;
    }

    public String getAppSmsLogId() {
        return appSmsLogId;
    }

    public void setAppSmsLogId(String appSmsLogId) {
        this.appSmsLogId = appSmsLogId;
    }

}