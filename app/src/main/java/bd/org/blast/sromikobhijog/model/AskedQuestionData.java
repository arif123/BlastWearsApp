package bd.org.blast.sromikobhijog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/16/2017.
 */

public class AskedQuestionData {


    @SerializedName("my_questions")
    @Expose
    private List<AskedQuestion> myQuestions = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<AskedQuestion> getMyQuestions() {
        return myQuestions;
    }

    public void setMyQuestions(List<AskedQuestion> myQuestions) {
        this.myQuestions = myQuestions;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }
}
