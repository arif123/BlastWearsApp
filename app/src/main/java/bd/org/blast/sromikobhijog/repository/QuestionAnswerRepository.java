package bd.org.blast.sromikobhijog.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.model.CategoryTopics;
import bd.org.blast.sromikobhijog.model.QuestionAnswer;
import bd.org.blast.sromikobhijog.model.QuestionAnswerData;
import bd.org.blast.sromikobhijog.model.Topic;
import bd.org.blast.sromikobhijog.model.TopicsQuestionAnswer;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/31/2017.
 */

public class QuestionAnswerRepository {

    private static final String TAG = "QuestionAnswerRepo";


    public static void insert(Context context, QuestionAnswerData list) {
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for (QuestionAnswer item : list.getQuesAns()) {
            insert(context, item);
        }
        Log.i(TAG, "insert: " + list.getQuesAns().size());
    }


    public static void insert(Context context, QuestionAnswer item) {
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);


        ContentValues cv = new ContentValues();


        cv.put(DB.QuestionAnswer.infoSearchTopicsId, item.getInfoSearchTopicsId());
        cv.put(DB.QuestionAnswer.topicsNameEn, item.getTopicsNameEn());
        cv.put(DB.QuestionAnswer.topicsNameBn, item.getTopicsNameBn());
        cv.put(DB.QuestionAnswer.infoSearchQuesAnsId, item.getInfoSearchQuesAnsId());
        cv.put(DB.QuestionAnswer.questionEn, item.getQuestionEn());
        cv.put(DB.QuestionAnswer.questionBn, item.getQuestionBn());
        cv.put(DB.QuestionAnswer.ansEn, item.getAnsEn());
        cv.put(DB.QuestionAnswer.ansBn, item.getAnsBn());
        cv.put(DB.QuestionAnswer.publishStatus, item.getPublishStatus());
        cv.put(DB.QuestionAnswer.updatedAt, item.getUpdatedAt());


        db.insert(DB.QuestionAnswer.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert: " + 1);

    }


    public static List<CategoryTopics> getAll(Context context) {
        List<CategoryTopics> categoryTopisList = new ArrayList<>();

        BriteDatabase database = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = database.query("select s.info_search_category_id, s.category_name_en, s.category_name_bn,   \n" +
                "t.info_search_topics_id,   t.topics_name_en, t.topics_name_bn,\n" +
                "t.topics_description_en,\n" +
                "t.topics_description_bn\n" +
                "from search_category  s LEFT JOIN topics t \n" +
                "on s.info_search_category_id= t.info_search_category_id ", null);


        CategoryTopics temp = null;
        if (cursor != null & cursor.moveToFirst()) {
            do {

                int infoSearchCategoryId = cursor.getInt(cursor.getColumnIndex(DB.Category.infoSearchCategoryId));
                if (temp == null || temp.getInfoSearchCategoryId() != infoSearchCategoryId) {
                    temp = new CategoryTopics();

                    temp.setInfoSearchCategoryId(cursor.getInt(cursor.getColumnIndex(DB.Category.infoSearchCategoryId)));
                    temp.setCategoryNameEn(cursor.getString(cursor.getColumnIndex(DB.Category.categoryNameEn)));
                    temp.setCategoryNameBn(cursor.getString(cursor.getColumnIndex(DB.Category.categoryNameBn)));


                    List<Topic> topicList = new ArrayList<>();
                    Topic topic = new Topic();
                    topic.setInfoSearchTopicsId(cursor.getString(cursor.getColumnIndex(DB.Topic.infoSearchCategoryId)));
                    topic.setTopicsNameEn(cursor.getString(cursor.getColumnIndex(DB.Topic.topicsNameEn)));
                    topic.setTopicsNameBn(cursor.getString(cursor.getColumnIndex(DB.Topic.topicsNameBn)));
                    topic.setTopicsDescriptionEn(cursor.getString(cursor.getColumnIndex(DB.Topic.topicsDescriptionEn)));
                    topic.setTopicsDescriptionBn(cursor.getString(cursor.getColumnIndex(DB.Topic.topicsDescriptionBn)));
                    topicList.add(topic);

                    temp.setTopicList(topicList);

                    categoryTopisList.add(temp);

                } else {

                    Topic topic = new Topic();
                    topic.setInfoSearchTopicsId(cursor.getString(cursor.getColumnIndex(DB.Topic.infoSearchCategoryId)));
                    topic.setTopicsNameEn(cursor.getString(cursor.getColumnIndex(DB.Topic.topicsNameEn)));
                    topic.setTopicsNameBn(cursor.getString(cursor.getColumnIndex(DB.Topic.topicsNameBn)));
                    topic.setTopicsDescriptionEn(cursor.getString(cursor.getColumnIndex(DB.Topic.topicsDescriptionEn)));
                    topic.setTopicsDescriptionBn(cursor.getString(cursor.getColumnIndex(DB.Topic.topicsDescriptionBn)));
                    temp.getTopicList().add(topic);
                }

            } while (cursor.moveToNext());
            cursor.close();
        }

        return categoryTopisList;

    }


    public static  List<QuestionAnswer> getAllQA(Context context, String topics_id) {


        BriteDatabase database = BriteDbHelper.getBriteDatabase(context);
        String SQL = "select * from topics where info_search_topics_id =?";
        //Cursor cursor = database.query(SQL, new String[]{topics_id});
        Cursor cursor = database.query("select * from "+ DB.QuestionAnswer.TABLE+" where "+DB.Topic.infoSearchTopicsId+"=? ", topics_id+"");


        List<QuestionAnswer> questionAnswerList = new ArrayList<>();
        if (cursor != null & cursor.moveToFirst()) {
            do {




                QuestionAnswer qa = new QuestionAnswer();
                qa.setInfoSearchTopicsId(cursor.getString(cursor.getColumnIndex(DB.QuestionAnswer.infoSearchTopicsId)));
                qa.setTopicsNameEn(cursor.getString(cursor.getColumnIndex(DB.QuestionAnswer.topicsNameEn)));
                qa.setTopicsNameBn(cursor.getString(cursor.getColumnIndex(DB.QuestionAnswer.topicsNameBn)));
                qa.setInfoSearchQuesAnsId(cursor.getString(cursor.getColumnIndex(DB.QuestionAnswer.infoSearchQuesAnsId)));
                qa.setQuestionEn(cursor.getString(cursor.getColumnIndex(DB.QuestionAnswer.questionEn)));
                qa.setQuestionBn(cursor.getString(cursor.getColumnIndex(DB.QuestionAnswer.questionBn)));
                qa.setAnsEn(cursor.getString(cursor.getColumnIndex(DB.QuestionAnswer.ansEn)));
                qa.setAnsBn(cursor.getString(cursor.getColumnIndex(DB.QuestionAnswer.ansBn)));
                qa.setPublishStatus(cursor.getString(cursor.getColumnIndex(DB.QuestionAnswer.publishStatus)));
                qa.setUpdatedAt(cursor.getString(cursor.getColumnIndex(DB.QuestionAnswer.updatedAt)));

                questionAnswerList.add(qa);



            } while (cursor.moveToNext());
            cursor.close();
        }

        return questionAnswerList;

    }
}
