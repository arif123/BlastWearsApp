package bd.org.blast.sromikobhijog.fragment;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.adapter.ExpandableRecyclerViewAdapterInfoQA;
import bd.org.blast.sromikobhijog.model.QuestionAnswer;
import bd.org.blast.sromikobhijog.repository.QuestionAnswerRepository;

/**
 * Created by This pc on 8/10/2017.
 */

public class InformationQAFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private ExpandableRecyclerViewAdapterInfoQA mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    private static final String ARG_SECTION_NUMBER = "section_number";
    int type;


    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ServiceProviderFragment newInstance(int sectionNumber) {
        ServiceProviderFragment fragment = new ServiceProviderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.layout_info_qa, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);


        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        Drawable horizontalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.horizontal_divider);
        horizontalDecoration.setDrawable(horizontalDivider);
        mRecyclerView.addItemDecoration(horizontalDecoration);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        List<QuestionAnswer> questionAnswerList = QuestionAnswerRepository.getAllQA(getContext(), "");
        mAdapter = new ExpandableRecyclerViewAdapterInfoQA(questionAnswerList);
        mAdapter.collapseAllSections();
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }


}