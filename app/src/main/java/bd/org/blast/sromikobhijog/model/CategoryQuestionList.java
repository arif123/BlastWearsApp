
package bd.org.blast.sromikobhijog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryQuestionList extends Category{


    @SerializedName("ques_list")
    @Expose
    private List<Question> quesList = null;



    public List<Question> getQuesList() {
        return quesList;
    }

    public void setQuesList(List<Question> quesList) {
        this.quesList = quesList;
    }

}
