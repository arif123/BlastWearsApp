package bd.org.blast.sromikobhijog.receiver;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.model.AskedQuestion;
import bd.org.blast.sromikobhijog.model.SmsNotificationLog;
import bd.org.blast.sromikobhijog.repository.AskedQuestionRepository;
import bd.org.blast.sromikobhijog.repository.SmsNotificationLogRepository;
import bd.org.blast.sromikobhijog.response.AuthResponse;
import bd.org.blast.sromikobhijog.response.UserInfo;
import bd.org.blast.sromikobhijog.utilis.Utils;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/22/2017.
 */

public class SMSReceiver extends BroadcastReceiver
{

    public static final String ACTION_SENT = "bd.org.dnet.smsbackground.sent";
    public static final String ACTION_DELIVERED = "bd.org.dnet.smsbackground.delivered";

    @Override
    public void onReceive(Context context, Intent intent)
    {

        AuthResponse authResponse = Utils.getinfo(context);
        UserInfo userInfo = null;
        if (authResponse != null) {
            userInfo = authResponse.getUserInfo();
        }else return;

        if ( userInfo == null || userInfo.getRole()==null || !userInfo.getRole().equalsIgnoreCase(Constant.USER_ADMIN) )return;

        Bundle extras = intent.getExtras();

        Object[] pdus = (Object[])extras.get("pdus");
        for (Object pdu: pdus)
        {
            SmsMessage msg = SmsMessage.createFromPdu((byte[])pdu);

            String senderNumber = msg.getOriginatingAddress();
            String body = msg.getMessageBody();

            Toast.makeText(context, "Message Received", Toast.LENGTH_SHORT).show();



            if (body.startsWith(Constant.SMS_PREFIX)){
                reply(context,senderNumber);




                String questionId;
                String question;
                int start= body.indexOf("#")+1;
                int end= body.indexOf(":");
                questionId =body.substring(start, end);
                question = body.substring(end+1);
                addAskedQuestion(context, questionId, question,senderNumber);
            }

           /* // Parse the SMS body
            if (isMySpecialSMS)
            {
                // Stop it being passed to the main Messaging inbox
                abortBroadcast();
            }*/
        }
    }

    private void reply(Context context, String senderNumber) {
        /** Creating a pending intent which will be broadcasted when an sms message is successfully sent */
        PendingIntent piSent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_SENT) , 0);

        /** Creating a pending intent which will be broadcasted when an sms message is successfully delivered */
        PendingIntent piDelivered = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_DELIVERED), 0);

        /** Getting an instance of SmsManager to sent sms message from the application*/
        SmsManager smsManager = SmsManager.getDefault();

        /** Sending the Sms message to the intended party */
        String message="Thanks. Your query is received and you will get reply within 72 hours.";
        smsManager.sendTextMessage(senderNumber, null, message, piSent, piDelivered);
    }


    private void addAskedQuestion(Context context,String questionId, String question, String mobileNo){
        AskedQuestion aq = new AskedQuestion();
        aq.setQuestionText(question );
        aq.setQuestionId(questionId);
        String prefixMobileNo="88";
        aq.setFromMobile(mobileNo.startsWith("0")?prefixMobileNo+mobileNo:mobileNo);
        aq.setSync(null);
        aq.setCreatedAt(Utils.getCurrentDateTime());
        aq.setQuestionChannel(Constant.CHANNEL_SMS);
        AskedQuestionRepository.insert(context, aq);

    }




}
