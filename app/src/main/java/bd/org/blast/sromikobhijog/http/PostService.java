package bd.org.blast.sromikobhijog.http;

import bd.org.blast.sromikobhijog.model.AskedQuestion;
import bd.org.blast.sromikobhijog.response.AuthResponse;
import bd.org.blast.sromikobhijog.response.ResponseApplication;
import bd.org.blast.sromikobhijog.response.ResponseAskQuestion;
import bd.org.blast.sromikobhijog.response.ResponseAskQuestionReply;
import bd.org.blast.sromikobhijog.response.ResponseCallLog;
import bd.org.blast.sromikobhijog.response.ResponseSMSLog;
import bd.org.blast.sromikobhijog.response.ResponseSmsNotificationLog;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/25/2017.
 */

public interface PostService {


     /*apps_application_id
    api_key [required]
	user_name [required] e.g admin
    question_id [required]
	question_text [required]
	applicant_name [required]
	mobile [required]
	alternative_mobile
	designation
	salary_range
	gender [required]
	age
	joining_date
	factory_name
	factory_address
	area_id [required]*/

    @FormUrlEncoded
    @POST("application/submit_application")
    Call<ResponseApplication> submitApplication(@Field("apps_application_id") int apps_application_id,
                                                @Field("api_key") String api_key,
                                                @Field("user_name") String user_name,
                                                @Field("question_id") Integer question_id,
                                                @Field("question_text") String question_text,
                                                @Field("applicant_name") String applicant_name,
                                                @Field("mobile") String mobile,
                                                @Field("alternative_mobile") String alternative_mobile,
                                                @Field("designation") String designation,
                                                @Field("salary_range") String salary_range,
                                                @Field("gender") String gender,
                                                @Field("age") int age,
                                                @Field("joining_date") String joining_date,
                                                @Field("factory_name") String factory_name,
                                                @Field("factory_address") String factory_address,
                                                @Field("area_id") int area_id);


    @FormUrlEncoded
    @POST("question/ask_question")
    Call<ResponseAskQuestion> submitAskedQuestion(
            @Field("api_key") String api_key,
            @Field("user_name") String user_name,
            @Field("question_id") String question_id,
            @Field("question_text") String question_text,
            @Field("mobile") String mobile,
            @Field("apps_ask_question_id") String apps_ask_question_id,
            @Field("question_channel") String question_channel);


    @FormUrlEncoded
    @POST("application/change_application_status")
    Call<ResponseApplication> updateApplication(
            @Field("api_key") String api_key,
            @Field("user_name") String user_name,
            @Field("application_id") int application_id,
            @Field("apps_application_id") int apps_application_id,
            @Field("application_status") String application_status);


    @FormUrlEncoded
    @POST("sms_call_log/call_log_sync")
    Call<ResponseCallLog> callLogSync(
            @Field("api_key") String api_key,
            @Field("user_name") String user_name,
            @Field("phone_number") String phoneNumber,
            @Field("call_type") String callType,
            @Field("call_duration") int callDuration,
            @Field("call_datetime") String callDatetime,
            @Field("app_call_log_id") int appCallLogId);


    @FormUrlEncoded
    @POST("sms_call_log/sms_log_sync")
    Call<ResponseSMSLog> smsLogSync(
            @Field("api_key") String api_key,
            @Field("user_name") String user_name,
            @Field("phone_number")  String phoneNumber,
            @Field("sms_type") String  sms_type, //[required] e.g INCOMING or OUTGOING
            @Field("sms_text") String sms_text ,
            @Field("sms_datetime") String sms_datetime, // [required] e.g 2017-11-20 11:31:10
            @Field("app_sms_log_id") String app_sms_log_id

    );

    @FormUrlEncoded
    @POST("question/ask_ques_reply")
    Call<ResponseAskQuestionReply> askQuestionReply(
            @Field("api_key") String api_key,
            @Field("user_name") String user_name,
            @Field("ask_question_id") String ask_question_id,
            @Field("reply_channel")String reply_channel, // SMS
            @Field("response_text") String response_text,
            @Field("reply_datetime") String reply_datetime,//2017-11-20 11:31:10
            @Field("apps_ask_ques_response_id") String apps_ask_ques_response_id
    );

    @FormUrlEncoded
    @POST( "notification/send_sms_notification")
    Call<ResponseSmsNotificationLog> sendSmsNotificationLog(

            @Field("api_key") String api_key,// [required]
            @Field("user_name") String   user_name,// [required]
            @Field("sms_from") String   sms_from, //[required] e.g 8801675794194
            @Field("sms_to") String   sms_to, //[required] e.g 8801675794194
            @Field("notification_for") String   notification_for,// [required] e.g APPLICATION or QUESTION
            @Field("sms_text") String   sms_text,// [required] e.g 2017-11-20 11:31:10
            @Field("app_notification_log_id") String   app_notification_log_id,
            @Field("application_or_question_id") String   application_or_question_id

    );


}
