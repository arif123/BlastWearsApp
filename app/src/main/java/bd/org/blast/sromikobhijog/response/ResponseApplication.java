
package bd.org.blast.sromikobhijog.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseApplication {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("application_id")
    @Expose
    private Integer applicationId;
    @SerializedName("apps_application_id")
    @Expose
    private String appsApplicationId;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(Integer applicationId) {
        this.applicationId = applicationId;
    }

    public String getAppsApplicationId() {
        return appsApplicationId;
    }

    public void setAppsApplicationId(String appsApplicationId) {
        this.appsApplicationId = appsApplicationId;
    }

}
