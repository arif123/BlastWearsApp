
package bd.org.blast.sromikobhijog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryQuestionData {

    @SerializedName("category_list")
    @Expose
    private List<CategoryQuestionList> categoryQuestionList = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<CategoryQuestionList> getCategoryQuestionList() {
        return categoryQuestionList;
    }

    public void setCategoryQuestionList(List<CategoryQuestionList> categoryQuestionList) {
        this.categoryQuestionList = categoryQuestionList;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
