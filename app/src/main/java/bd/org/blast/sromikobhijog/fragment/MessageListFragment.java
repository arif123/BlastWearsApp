package bd.org.blast.sromikobhijog.fragment;

/**
 * Created by This pc on 8/10/2017.
 */


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.List;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.activities.MessageActivity;
import bd.org.blast.sromikobhijog.adapter.ApplicationAdapter;
import bd.org.blast.sromikobhijog.adapter.MessageAdapter;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.model.AskedQuestion;
import bd.org.blast.sromikobhijog.repository.AskedQuestionRepository;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;

/**
 * A ApplicationListFragment fragment containing a simple view.
 */
public class MessageListFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    int type;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.main_swipe)
    WaveSwipeRefreshLayout mainSwipe;
    Unbinder unbinder;

       /* public PlaceholderFragment(int type) {
            this.type = type;
        }*/

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static MessageListFragment newInstance(int sectionNumber) {
        MessageListFragment fragment = new MessageListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_info_and_service_tab, container, false);
        unbinder = ButterKnife.bind(this, rootView);

        list = (ListView) rootView.findViewById(R.id.list);


        final List<AskedQuestion> messages = AskedQuestionRepository.getAll(getActivity());
        final MessageAdapter messageAdapter = new MessageAdapter(getContext(), messages);
        list.setAdapter(messageAdapter);
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                messageAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                Intent intent = new Intent(getContext(), MessageActivity.class);
                AskedQuestion askedQuestion = messages.get(position);
                intent.putExtra("AskedQuestion", askedQuestion);
                                /*intent.putExtra(DB.AskedQuestion.askQuestionId,askedQuestion.getAskQuestionId());
                                intent.putExtra(DB.AskedQuestion.questionText, askedQuestion.getQuestionText());
                                intent.putExtra(DB.AskedQuestion.questionChannel, askedQuestion.getQuestionChannel());*/

                getActivity().startActivity(intent);
            }
        });





        mainSwipe.setColorSchemeColors(Color.WHITE, Color.WHITE);
        mainSwipe.setWaveColor(Color.argb(255, 203, 32, 38));
        mainSwipe.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        final MessageAdapter messageAdapter = new MessageAdapter(getContext(), messages);
                        list.setAdapter(messageAdapter);

                        etSearch.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                                // When user changed the Text
                                messageAdapter.getFilter().filter(cs);
                            }

                            @Override
                            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                                          int arg3) {
                                // TODO Auto-generated method stub
                            }

                            @Override
                            public void afterTextChanged(Editable arg0) {
                                // TODO Auto-generated method stub
                            }
                        });



                        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                                Intent intent = new Intent(getContext(), MessageActivity.class);
                                AskedQuestion askedQuestion = messages.get(position);
                                intent.putExtra("AskedQuestion", askedQuestion);
                                /*intent.putExtra(DB.AskedQuestion.askQuestionId,askedQuestion.getAskQuestionId());
                                intent.putExtra(DB.AskedQuestion.questionText, askedQuestion.getQuestionText());
                                intent.putExtra(DB.AskedQuestion.questionChannel, askedQuestion.getQuestionChannel());*/

                                getActivity().startActivity(intent);
                            }
                        });

                        mainSwipe.setRefreshing(false);
                    }
                }, 2000);
            }
        });


        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}