package bd.org.blast.sromikobhijog.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.model.UserArea;
import bd.org.blast.sromikobhijog.model.UserAreaData;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/31/2017.
 */

public class UserAreaRepository {

    private static final String TAG="UserAreaRepository";


    public static void insert(Context context, UserAreaData list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(UserArea item: list.getUserAreaList()){
            insert(context, item);
        }
        Log.i(TAG, "insert: "+list.getUserAreaList().size() );
    }


    public static void insert(Context context, UserArea item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);


        ContentValues cv = new ContentValues();



        cv.put(DB.UserArea.id , item.getAreaId());
        cv.put(DB.UserArea.username  , item.getUsername());
        cv.put(DB.UserArea.email , item.getEmail());
        cv.put(DB.UserArea.areaId , item.getAreaId());
        cv.put(DB.UserArea.areaName , item.getAreaName());
        cv.put(DB.UserArea.areaLocation , item.getAreaLocation());
        cv.put(DB.UserArea.areaLatitude , item.getAreaLatitude());
        cv.put(DB.UserArea.areaLongitude , item.getAreaLongitude());
        cv.put(DB.UserArea.areaDivision , item.getAreaDivision());
        cv.put(DB.UserArea.areaDistrict , item.getAreaDistrict());
        cv.put(DB.UserArea.areaUpazila , item.getAreaUpazila());
        cv.put(DB.UserArea.areaUnion , item.getAreaUnion());
        cv.put(DB.UserArea.areaDescription , item.getAreaDescription());
        cv.put(DB.UserArea.publishStatus , item.getPublishStatus());
        



        db.insert(DB.UserArea.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert: "+1 );

    }
}
