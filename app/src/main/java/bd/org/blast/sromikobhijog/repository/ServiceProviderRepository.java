package bd.org.blast.sromikobhijog.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.model.ServiceProviderCategory;
import bd.org.blast.sromikobhijog.model.ServicesProvider;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/24/2017.
 */

public class ServiceProviderRepository {

    private static final String TAG=ServiceProviderRepository.class.getSimpleName();


    public static void insert(Context context, List<ServicesProvider> list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(ServicesProvider item: list){
            insert(context, item);
        }
        Log.i(TAG, "insert: "+list.size() );
    }


    public static void insertCat(Context context, List<ServiceProviderCategory> list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(ServiceProviderCategory item: list){
            insertCat(context, item);
            insert(context, item.getServicesProviderList());
        }
        Log.i(TAG, "insert: "+list.size() );
    }


    public static void insert(Context context, ServicesProvider item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        ContentValues cv = new ContentValues();

        cv.put(DB.ServiceProvider.serviceProviderId, item.getServiceProviderId());
        cv.put(DB.ServiceProvider.serviceProviderCategoryId , item.getServiceProviderCategoryId());
        cv.put(DB.ServiceProvider.serviceProviderNameEn, item.getServiceProviderNameEn());
        cv.put(DB.ServiceProvider.serviceProviderNameBn, item.getServiceProviderNameBn());
        cv.put(DB.ServiceProvider.businessDescriptionEn, item.getBusinessDescriptionEn());
        cv.put(DB.ServiceProvider.businessDescriptionBn, item.getBusinessDescriptionBn());
        cv.put(DB.ServiceProvider.serviceProviderPhone, item.getServiceProviderPhone());
        cv.put(DB.ServiceProvider.serviceProviderEmail, item.getServiceProviderEmail());
        cv.put(DB.ServiceProvider.serviceProviderWebsite, item.getServiceProviderWebsite());
        cv.put(DB.ServiceProvider.serviceProviderLogo, item.getServiceProviderLogo());
        cv.put(DB.ServiceProvider.publishStatus, item.getPublishStatus());
        cv.put(DB.ServiceProvider.isGovt, item.getIsGovt());
        cv.put(DB.ServiceProvider.createdBy, item.getCreatedBy());
        cv.put(DB.ServiceProvider.createdAt, item.getCreatedAt());
        cv.put(DB.ServiceProvider.updatedBy , item.getUpdatedBy());
        cv.put(DB.ServiceProvider.updatedAt , item.getUpdatedAt());

        db.insert(DB.ServiceProvider.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert: "+1 );

    } public static void insertCat(Context context, ServiceProviderCategory item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        ContentValues cv = new ContentValues();

        cv.put(DB.ServiceProviderCategory.servicesProviderCategoryId, item.getServicesProviderCategoryId());
        cv.put(DB.ServiceProviderCategory.categoryNameEn, item.getCategoryNameEn());
        cv.put(DB.ServiceProviderCategory.categoryNameBn, item.getCategoryNameBn());
        cv.put(DB.ServiceProviderCategory.publishStatus, item.getPublishStatus());
        cv.put(DB.ServiceProviderCategory.updatedAt, item.getUpdatedAt());




        db.insert(DB.ServiceProviderCategory.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert: "+1 );

    }



    public static List<ServiceProviderCategory> getAll(Context context){
        List<ServiceProviderCategory> categoryQuestionList  =new ArrayList<>();

        BriteDatabase database = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = database.query("select * from "+DB.ServiceProvider.TABLE+" s,  "+DB.ServiceProviderCategory.TABLE+" q " +
                "where s."+DB.ServiceProvider.serviceProviderCategoryId+"= q."+DB.ServiceProviderCategory.servicesProviderCategoryId, null);


        ServiceProviderCategory temp=null;
        if (cursor!= null & cursor.moveToFirst()){
            do {

                String servicesProviderCategoryId=cursor.getString(cursor.getColumnIndex(DB.ServiceProviderCategory.servicesProviderCategoryId));
                if (temp == null || temp.getServicesProviderCategoryId() != servicesProviderCategoryId){
                    temp = new ServiceProviderCategory();




                    temp.setServicesProviderCategoryId(cursor.getString(cursor.getColumnIndex(DB.ServiceProviderCategory.servicesProviderCategoryId)));
                    temp.setCategoryNameEn(cursor.getString(cursor.getColumnIndex(DB.ServiceProviderCategory.categoryNameEn)));
                    temp.setCategoryNameBn(cursor.getString(cursor.getColumnIndex(DB.ServiceProviderCategory.categoryNameBn)));
                    temp.setPublishStatus(cursor.getString(cursor.getColumnIndex(DB.ServiceProviderCategory.publishStatus)));
                    temp.setUpdatedAt(cursor.getString(cursor.getColumnIndex(DB.ServiceProviderCategory.updatedAt)));


                    List<ServicesProvider> servicesProviderList = new ArrayList<>();
                    ServicesProvider servicesProvider = new ServicesProvider();

                    servicesProvider.setServiceProviderId(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderId)));
                    servicesProvider.setServiceProviderCategoryId(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderCategoryId)));
                    servicesProvider.setServiceProviderNameEn(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderNameEn)));
                    servicesProvider.setServiceProviderNameBn(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderNameBn)));
                    servicesProvider.setBusinessDescriptionEn(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.businessDescriptionEn)));
                    servicesProvider.setBusinessDescriptionBn(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.businessDescriptionBn)));
                    servicesProvider.setServiceProviderPhone(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderPhone)));
                    servicesProvider.setServiceProviderPhone(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderPhone)));
                    servicesProvider.setServiceProviderEmail(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderEmail)));
                    servicesProvider.setServiceProviderWebsite(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderWebsite)));
                    servicesProvider.setServiceProviderLogo(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderLogo)));
                    servicesProvider.setPublishStatus(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.publishStatus)));
                    servicesProvider.setIsGovt(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.isGovt)));
                    servicesProvider.setCreatedBy(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.createdBy)));
                    servicesProvider.setCreatedAt(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.createdAt)));
                    servicesProvider.setUpdatedBy(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.updatedBy)));
                    servicesProvider.setUpdatedAt(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.updatedAt)));
                    servicesProviderList.add(servicesProvider);

                    temp.setServicesProviderList(servicesProviderList);

                    categoryQuestionList.add(temp);

                }else {

                    ServicesProvider servicesProvider = new ServicesProvider();

                    servicesProvider.setServiceProviderId(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderId)));
                    servicesProvider.setServiceProviderCategoryId(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderCategoryId)));
                    servicesProvider.setServiceProviderNameEn(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderNameEn)));
                    servicesProvider.setServiceProviderNameBn(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderNameBn)));
                    servicesProvider.setBusinessDescriptionEn(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.businessDescriptionEn)));
                    servicesProvider.setBusinessDescriptionBn(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.businessDescriptionBn)));
                    servicesProvider.setServiceProviderPhone(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderPhone)));
                    servicesProvider.setServiceProviderPhone(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderPhone)));
                    servicesProvider.setServiceProviderEmail(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderEmail)));
                    servicesProvider.setServiceProviderWebsite(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderWebsite)));
                    servicesProvider.setServiceProviderLogo(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.serviceProviderLogo)));
                    servicesProvider.setPublishStatus(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.publishStatus)));
                    servicesProvider.setIsGovt(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.isGovt)));
                    servicesProvider.setCreatedBy(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.createdBy)));
                    servicesProvider.setCreatedAt(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.createdAt)));
                    servicesProvider.setUpdatedBy(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.updatedBy)));
                    servicesProvider.setUpdatedAt(cursor.getString(cursor.getColumnIndex(DB.ServiceProvider.updatedAt)));

                    temp.getServicesProviderList().add(servicesProvider);
                }






                //application.setJoiningDate(cursor.getString(cursor.getColumnIndex(DB.Applications.joiningDate)));



            }while (cursor.moveToNext());
            cursor.close();
        }

        return categoryQuestionList;

    }
}
