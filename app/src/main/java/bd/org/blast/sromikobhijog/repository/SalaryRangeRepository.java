package bd.org.blast.sromikobhijog.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.db.DbHelper;
import bd.org.blast.sromikobhijog.model.SalaryRange;
import bd.org.blast.sromikobhijog.model.SalaryRangeData;
import bd.org.blast.sromikobhijog.model.UserArea;
import bd.org.blast.sromikobhijog.model.UserAreaData;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/31/2017.
 */

public class SalaryRangeRepository {

    private static final String TAG="SalaryRangeRepository";


    public static void insert(Context context, SalaryRangeData list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(SalaryRange item: list.getSalaryRangeList()){
            insert(context, item);
        }
        Log.i(TAG, "insert: "+list.getSalaryRangeList().size() );
    }


    public static void insert(Context context, SalaryRange item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);


        ContentValues cv = new ContentValues();




         cv.put(DB.SalaryRange.salaryRangeId , item.getSalaryRangeId());
         cv.put(DB.SalaryRange.salaryRangeEn , item.getSalaryRangeEn());
         cv.put(DB.SalaryRange.salaryRangeBn , item.getSalaryRangeBn());
         cv.put(DB.SalaryRange.publishStatus , item.getPublishStatus());
         cv.put(DB.SalaryRange.updatedAt , item.getUpdatedAt());
        

        db.insert(DB.SalaryRange.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert: "+1 );

    }



    private static List<SalaryRange> list = new ArrayList<>();
    public static List<SalaryRange> getAll(Context context){
        SQLiteDatabase db = DbHelper.getDbHelper(context).getReadableDatabase();
        list.clear();
        Cursor cursor = db.rawQuery("select * from "+DB.SalaryRange.TABLE+" ORDER BY "+DB.SalaryRange.salaryRangeEn, null);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                SalaryRange sr = new SalaryRange();

                sr.setSalaryRangeId(cursor.getString(cursor.getColumnIndex(DB.SalaryRange.salaryRangeId )));
                sr.setSalaryRangeEn(cursor.getString(cursor.getColumnIndex(DB.SalaryRange.salaryRangeEn )));
                sr.setSalaryRangeBn(cursor.getString(cursor.getColumnIndex(DB.SalaryRange.salaryRangeBn )));
                sr.setPublishStatus(cursor.getString(cursor.getColumnIndex(DB.SalaryRange.publishStatus )));
                sr.setUpdatedAt(cursor.getString(cursor.getColumnIndex(DB.SalaryRange.updatedAt )));


                list.add(sr);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }
}
