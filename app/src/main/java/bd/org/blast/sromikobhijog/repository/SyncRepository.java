package bd.org.blast.sromikobhijog.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.db.DbHelper;
import bd.org.blast.sromikobhijog.model.Area;
import bd.org.blast.sromikobhijog.model.AreaData;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/31/2017.
 */

public class SyncRepository {

    private static final String TAG=SyncRepository.class.getSimpleName();


    public static void DEFAULT(Context context){

        String defaultTime="0000-00-00 00:00:00";

        Map<String, String > map = new HashMap<>();
        map.put(DB.Category.TABLE, defaultTime);
        map.put(DB.Topic.TABLE, defaultTime);
        map.put(DB.QuestionAnswer.TABLE, defaultTime);
        map.put(DB.Area.TABLE, defaultTime);
        map.put(DB.UserArea.TABLE, defaultTime);
        map.put(DB.SalaryRange.TABLE, defaultTime);
        map.put(DB.Question.TABLE, defaultTime);
        map.put(DB.Applications.TABLE, defaultTime);
        map.put(DB.AskedQuestion.TABLE, defaultTime);
        map.put(DB.ServiceProviderCategory.TABLE, defaultTime);
        map.put(DB.ServiceProvider.TABLE, defaultTime);
        insert(context, map);

        Log.i(TAG, "DEFAULT: "+map.size() );
    }




    public static void insert(Context context, Map<String, String> item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        for (Map.Entry entry:item.entrySet()) {

            ContentValues cv = new ContentValues();
            cv.put(DB.Sync.tableName , entry.getKey().toString());
            cv.put(DB.Sync.updated_at , entry.getValue().toString());
            db.insert(DB.Sync.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

            Log.i(TAG, "insert: "+1 );
        }


    }

    public static void insert(Context context, String tableName){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);


        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTime(new Date());
        //cal.add(Calendar.HOUR, -1);
        cal.add(Calendar.MINUTE, -5);




            ContentValues cv = new ContentValues();
            cv.put(DB.Sync.tableName , tableName);
            cv.put(DB.Sync.updated_at , formatter.format(cal.getTime()));
            db.insert(DB.Sync.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

            Log.i(TAG, "insert: "+1 );



    }

    public static void insert(Context context, String tableName, String dateTime){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);


        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTime(new Date());
        //cal.add(Calendar.HOUR, -1);
        cal.add(Calendar.MINUTE, -5);




            ContentValues cv = new ContentValues();
            cv.put(DB.Sync.tableName , tableName);
            cv.put(DB.Sync.updated_at , formatter.format(cal.getTime()));
            db.insert(DB.Sync.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

            Log.i(TAG, "insert: "+1 );



    }

    public static void clear(Context context){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        db.delete(DB.Sync.TABLE, null, null);
    }



    private static List<Area> list = new ArrayList<>();
    public static List<Area> getAll(Context context){
        SQLiteDatabase db = DbHelper.getDbHelper(context).getReadableDatabase();
        list.clear();
        Cursor cursor = db.rawQuery("select * from "+DB.Area.TABLE+" ORDER BY "+DB.Area.areaName, null);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                Area sr = new Area();


                sr.setAreaId(cursor.getString(cursor.getColumnIndex(DB.Area.areaId )));
                sr.setAreaName(cursor.getString(cursor.getColumnIndex(DB.Area.areaName )));
                sr.setAreaLocation(cursor.getString(cursor.getColumnIndex(DB.Area.areaLocation )));
                sr.setAreaLatitude(cursor.getString(cursor.getColumnIndex(DB.Area.areaLatitude )));
                sr.setAreaLongitude(cursor.getString(cursor.getColumnIndex(DB.Area.areaLongitude )));
                sr.setPublishStatus(cursor.getString(cursor.getColumnIndex(DB.Area.publishStatus )));
                sr.setUpdatedAt(cursor.getString(cursor.getColumnIndex(DB.Area.updatedAt )));




                list.add(sr);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static int  getAreaId(Context context, String areaName){

        int areaId=0;

        SQLiteDatabase db = DbHelper.getDbHelper(context).getReadableDatabase();
        list.clear();
        Cursor cursor = db.query(DB.Area.TABLE, new String[]{DB.Area.areaId}, DB.Area.areaName, new String[]{areaName},null, null, null);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                Area sr = new Area();


                areaId= cursor.getInt(cursor.getColumnIndex(DB.Area.areaId ));





                list.add(sr);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return areaId;

    }


    public static String getRow(Context context, String areaName){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("select * from "+DB.Sync.TABLE+" WHERE "+DB.Sync.tableName+" LIKE ? LIMIT 1",new String[]{areaName+""});
        if(cursor.getCount()>0){
            cursor.moveToFirst();




            String syncTime =(cursor.getString(cursor.getColumnIndex(DB.Sync.updated_at )));

            cursor.close();

            return syncTime;
        }

        return null;
    }
}
