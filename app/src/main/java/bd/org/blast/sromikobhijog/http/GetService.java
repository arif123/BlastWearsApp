package bd.org.blast.sromikobhijog.http;

import bd.org.blast.sromikobhijog.model.ApplicationData;
import bd.org.blast.sromikobhijog.model.AreaData;
import bd.org.blast.sromikobhijog.model.AskedQuestion;
import bd.org.blast.sromikobhijog.model.AskedQuestionData;
import bd.org.blast.sromikobhijog.model.AskedQuestionReplyData;
import bd.org.blast.sromikobhijog.model.CategoryData;
import bd.org.blast.sromikobhijog.model.CategoryQuestionData;
import bd.org.blast.sromikobhijog.model.QuestionAnswerData;
import bd.org.blast.sromikobhijog.model.SalaryRangeData;
import bd.org.blast.sromikobhijog.model.ServiceProviderList;
import bd.org.blast.sromikobhijog.model.SmsNotificationLogData;
import bd.org.blast.sromikobhijog.model.TopicData;
import bd.org.blast.sromikobhijog.model.UserAreaData;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/25/2017.
 */

public interface GetService {

    @GET("information/get_category")
    Call<CategoryData> getCategory();

    @FormUrlEncoded
    @POST("information/get_topics_of_category")
    Call<TopicData> getTopic(@Field("category_id") int category_id);

    @FormUrlEncoded
    @POST("information/get_ques_ans_of_topic")
    Call<QuestionAnswerData> getQuestionAnswer(@Field("topic_id") int topic_id);

    @FormUrlEncoded
    @POST("area/get_area")
    Call<AreaData> getArea(@Field("api_key") String apiKey);

    @FormUrlEncoded
    @POST("area/get_user_area")
    Call<UserAreaData> getUserArea(@Field("api_key") String apiKey, @Field("user_name") String UserName);


    @FormUrlEncoded
    @POST("application/get_salary_range_list")
    Call<SalaryRangeData> getSalaryRangeList(@Field("api_key") String apiKey);


   @GET("application/get_category_wise_questions_list")
    Call<CategoryQuestionData> getCategoryWiseQuestionsList();


   @GET("services_provider/get_category_wise_service_provider_list")
    Call<ServiceProviderList> getCategoryWiseServiceProviderList();


   /*@param dateForm date formate 2017-11-08 11:05:12*/
   @FormUrlEncoded
    @POST("application/get_my_applications")
    Call<ApplicationData> getMyApplication(@Field("api_key") String apiKey,
                                           @Field("user_name") String userName,
                                           @Field("date_from") String dateFrom );


    @FormUrlEncoded
    @POST("application/get_mylocation_applications")
    Call<ApplicationData> getMyLocationApplication(@Field("api_key") String apiKey,
                                           @Field("user_name") String userName,
                                           @Field("date_from") String dateFrom );


    @FormUrlEncoded
    @POST("question/get_my_questions")
    Call<AskedQuestionData> getMyQuestion(
            @Field("api_key") String api_key,
            @Field("mobile") String mobile,
            @Field("date_from") String dateFrom);

    @FormUrlEncoded
    @POST("question/get_all_ask_ques_response")
    Call<AskedQuestionReplyData> getAllAskedQuestionReply(
            @Field("api_key") String api_key,
            @Field("user_name") String user_name,
            @Field("date_from") String dateFrom);



    @FormUrlEncoded
    @POST("question/get_my_ask_ques_response")
    Call<AskedQuestionReplyData> getMyAskedQuestionReply(
            @Field("mobile") String mobile,
            @Field("date_from") String date_from);


    @FormUrlEncoded
    @POST("notification/get_all_notifications")
    Call<SmsNotificationLogData> getSmsNotifications(

            @Field("api_key") String api_key,
            @Field("user_name") String user_name ,
            @Field("date_from") String date_from
    );


}
