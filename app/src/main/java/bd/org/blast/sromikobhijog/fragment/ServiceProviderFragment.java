package bd.org.blast.sromikobhijog.fragment;

/**
 * Created by This pc on 8/10/2017.
 */

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.adapter.ExpandableRecyclerViewAdapterSP;
import bd.org.blast.sromikobhijog.model.ServiceProviderCategory;
import bd.org.blast.sromikobhijog.repository.ServiceProviderRepository;

/**
 * A ServiceProviderList fragment containing a simple view.
 */
public class ServiceProviderFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private ExpandableRecyclerViewAdapterSP mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;


    private static final String ARG_SECTION_NUMBER = "section_number";
    int type;


    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ServiceProviderFragment newInstance(int sectionNumber) {
        ServiceProviderFragment fragment = new ServiceProviderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.layout_recycler_header_footer, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);


        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        Drawable horizontalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.horizontal_divider);
        horizontalDecoration.setDrawable(horizontalDivider);
        mRecyclerView.addItemDecoration(horizontalDecoration);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        List<ServiceProviderCategory> serviceProviderCategoryList = ServiceProviderRepository.getAll(getContext());

        mAdapter = new ExpandableRecyclerViewAdapterSP(serviceProviderCategoryList);
        mAdapter.collapseAllSections();
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }


}


/*extends Fragment {
    *//**
 * The fragment argument representing the section number for this
 * fragment.
 * <p>
 * Returns a new instance of this fragment for the given section
 * number.
 *//*
    private static final String ARG_SECTION_NUMBER = "section_number";
    int type;

       *//* public PlaceholderFragment(int type) {
            this.type = type;
        }*//*

    *//**
 * Returns a new instance of this fragment for the given section
 * number.
 *//*
    public static ServiceProviderFragment newInstance(int sectionNumber) {
        ServiceProviderFragment fragment = new ServiceProviderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_info_and_service_tab, container, false);
        ListView listView = (ListView) rootView.findViewById(R.id.list);


        ArrayList<ServiceProviderList> serviceProviders = new ArrayList<>();
        ServiceProviderList serviceProvider = new ServiceProviderList("Service Provider ", "Workplace issue", "+880123456789", "email@example.com", "1/1 Pioneer Road, Kakrail, Dhaka-1000, Bangladesh");
        for (int i=0; i<10; i++)
            serviceProviders.add(serviceProvider);
        ServiceProviderAdapter serviceProviderAdapter = new ServiceProviderAdapter(getContext(), serviceProviders);
        listView.setAdapter(serviceProviderAdapter);

        final WaveSwipeRefreshLayout mWaveSwipeRefreshLayout = (WaveSwipeRefreshLayout) rootView.findViewById(R.id.main_swipe);
        mWaveSwipeRefreshLayout.setWaveColor(0xFF029f13);
        mWaveSwipeRefreshLayout.setColorSchemeColors(Color.WHITE, Color.WHITE);
        mWaveSwipeRefreshLayout.setMaxDropHeight((int) (mWaveSwipeRefreshLayout.getHeight() * 0.5));
        mWaveSwipeRefreshLayout.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // 更新が終了したらインジケータ非表示
                        mWaveSwipeRefreshLayout.setRefreshing(false);
                    }
                }, 3000);

            }
        });



        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(view.getContext(), ((TextView) view).getText(),
                        Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }
}
*/