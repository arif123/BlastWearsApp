package bd.org.blast.sromikobhijog.fragment;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.DateTimePicker;
import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.activities.ApplicationFormActivity;
import bd.org.blast.sromikobhijog.model.Application;
import bd.org.blast.sromikobhijog.model.Area;
import bd.org.blast.sromikobhijog.model.DataModel;
import bd.org.blast.sromikobhijog.model.SalaryRange;
import bd.org.blast.sromikobhijog.repository.ApplicationRepository;
import bd.org.blast.sromikobhijog.repository.AreaRepository;
import bd.org.blast.sromikobhijog.repository.SalaryRangeRepository;
import bd.org.blast.sromikobhijog.sync.DataLoader;
import bd.org.blast.sromikobhijog.utilis.AnimationTween;
import bd.org.blast.sromikobhijog.utilis.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by This pc on 10/18/2017.
 */

public class ApplicationFormFragment extends Fragment {


    private static final String TAG = ApplicationFormFragment.class.getSimpleName();
    @BindView(R.id.btnApplicationList)
    Button btnApplicationList;
    @BindView(R.id.etName)
    TextInputEditText etApplicantName;
    @BindView(R.id.etDesignation)
    TextInputEditText etDesignation;
    @BindView(R.id.etMobileNo)
    TextInputEditText etMobileNo;
    @BindView(R.id.etMobileNoAlternative)
    TextInputEditText etMobileNoAlternative;
    @BindView(R.id.etSalaryRang)
    TextInputEditText etSalaryRang;
    @BindView(R.id.etGender)
    TextInputEditText etGender;
    @BindView(R.id.etAge)
    TextInputEditText etAge;
    @BindView(R.id.etWorkPlace)
    TextInputEditText etWorkPlace;
    @BindView(R.id.etWorkAddress)
    TextInputEditText etWorkAddress;
    @BindView(R.id.etArea)
    TextInputEditText etArea;
    @BindView(R.id.btnSubmitApplication)
    Button btnSubmitApplication;
    Unbinder unbinder;
    @BindView(R.id.etapplication_question)
    EditText etQuestion;
    @BindView(R.id.etJoiningDate)
    TextInputEditText etJoiningDate;
    @BindView(R.id.svform)
    ScrollView svform;

    Vibrator vibe;

    String question = "";
    String questionId = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = (View) inflater.inflate(R.layout.layout_aplication_form, container, false);

        unbinder = ButterKnife.bind(this, view);

        question = getArguments().getString(ApplicationFormActivity.TAG_QUESTION);
        questionId = getArguments().getString(ApplicationFormActivity.TAG_QUESTION_ID);

        etQuestion.setText(question);
        etJoiningDate.setFocusable(false);

        List<SalaryRange> salaryRanges = SalaryRangeRepository.getAll(getContext());
        List<Area> areas = AreaRepository.getAll(getContext());

        Utils.popupSingle(getActivity(), etSalaryRang, etSalaryRang, convertSRTodataModel(salaryRanges));

        List<DataModel> genders = new ArrayList<>();
        genders.add(new DataModel().setId("1").setValueEN("Male").setValueBN("পুরুষ"));
        genders.add(new DataModel().setId("2").setValueEN("Female").setValueBN("মহিলা"));
        genders.add(new DataModel().setId("3").setValueEN("other").setValueBN("অন্যান্য"));

        Utils.popupSingle(getActivity(), etGender, etGender, genders);
        Utils.popupSingle(getActivity(), etArea, etArea, convertAreaToDataModel(areas));

        Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());
        etMobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith("880")) {
                    etMobileNo.setText("880");
                    Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());

                }
                if (s.toString().length() >= 4) {
                    String ss = s.toString().substring(3, 4);
                    Log.i(TAG, "afterTextChanged: " + ss);
                    if (!"1".contains(ss)) {
                        Log.i(TAG, "in: " + ss);
                        etMobileNo.setText(s.toString().substring(0, 3));
                        Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());
                    }
                }

                if (s.toString().length() >= 5) {
                    String ss = s.toString().substring(4, 5);
                    Log.i(TAG, "afterTextChanged: " + ss);
                    if (!"156789".contains(ss)) {
                        Log.i(TAG, "in: " + ss);
                        etMobileNo.setText(s.toString().substring(0, 4));
                        Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());
                    }
                }


                if (s.toString().length() > 13) {
                    etMobileNo.setText(s.toString().substring(0, 13));
                    Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());


                }

            }
        });


        Selection.setSelection(etMobileNoAlternative.getText(), etMobileNoAlternative.getText().length());
        etMobileNoAlternative.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith("880")) {
                    etMobileNoAlternative.setText("880");
                    Selection.setSelection(etMobileNoAlternative.getText(), etMobileNoAlternative.getText().length());

                }
                if (s.toString().length() >= 4) {
                    String ss = s.toString().substring(3, 4);
                    Log.i(TAG, "afterTextChanged: " + ss);
                    if (!"1".contains(ss)) {
                        Log.i(TAG, "in: " + ss);
                        etMobileNoAlternative.setText(s.toString().substring(0, 3));
                        Selection.setSelection(etMobileNoAlternative.getText(), etMobileNoAlternative.getText().length());
                    }
                }

                if (s.toString().length() >= 5) {
                    String ss = s.toString().substring(4, 5);
                    Log.i(TAG, "afterTextChanged: " + ss);
                    if (!"156789".contains(ss)) {
                        Log.i(TAG, "in: " + ss);
                        etMobileNoAlternative.setText(s.toString().substring(0, 4));
                        Selection.setSelection(etMobileNoAlternative.getText(), etMobileNoAlternative.getText().length());
                    }
                }


                if (s.toString().length() > 13) {
                    etMobileNoAlternative.setText(s.toString().substring(0, 13));
                    Selection.setSelection(etMobileNoAlternative.getText(), etMobileNoAlternative.getText().length());


                }

            }
        });


        vibe = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);


        return view;
    }

    private List<DataModel> convertSRTodataModel(List<SalaryRange> salaryRanges) {

        List<DataModel> dataModels = new ArrayList<>();


        for (SalaryRange sr :
                salaryRanges) {

            DataModel model = new DataModel();
            model.setId(sr.getSalaryRangeId());
            model.setValueEN(sr.getSalaryRangeEn());
            model.setValueBN(sr.getSalaryRangeBn());
            dataModels.add(model);


        }
        return dataModels;
    }

    private List<DataModel> convertAreaToDataModel(List<Area> areas) {

        List<DataModel> dataModels = new ArrayList<>();
        for (Area area :
                areas) {

            DataModel model = new DataModel();
            model.setId(area.getAreaId());
            model.setValueEN(area.getAreaName());
            model.setValueBN(area.getAreaName());
            dataModels.add(model);


        }
        return dataModels;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    private void sentApplication() {

        if (etQuestion.getText().toString().isEmpty()) {

            etQuestion.setError(getString(R.string.required));
            etQuestion.requestFocus();
            focusOnView(etQuestion);
            AnimationTween.shakeOnError(getActivity(), etQuestion);
            vibe.vibrate(50); // 50 is time in ms
            return;

        } else if (etApplicantName.getText().toString().isEmpty()) {

            etApplicantName.setError(getString(R.string.required));
            etApplicantName.requestFocus();
            focusOnView(etApplicantName);
            AnimationTween.shakeOnError(getActivity(), etApplicantName);
            vibe.vibrate(50); // 50 is time in ms
            return;

        } else if (etMobileNo.getText().toString().isEmpty()) {

            etMobileNo.setError(getString(R.string.required));
            etMobileNo.requestFocus();
            focusOnView(etMobileNo);
            AnimationTween.shakeOnError(getActivity(), etMobileNo);
            vibe.vibrate(50); // 50 is time in ms
            return;

        }  else if (etMobileNo.getText().length()!=13) {

            etMobileNo.setError(getString(R.string.invalid));
            etMobileNo.requestFocus();
            focusOnView(etMobileNo);
            AnimationTween.shakeOnError(getActivity(), etMobileNo);
            vibe.vibrate(50); // 50 is time in ms
            return;

        } else if (etGender.getText().toString().isEmpty()) {

            etGender.setError(getString(R.string.required));
            etGender.requestFocus();
            focusOnView(etGender);
            AnimationTween.shakeOnError(getActivity(), etGender);
            vibe.vibrate(50); // 50 is time in ms
            return;

        } else if (etArea.getText().toString().isEmpty()) {

            etArea.setError(getString(R.string.required));
            etArea.requestFocus();
            focusOnView(etArea);
            AnimationTween.shakeOnError(getActivity(), etArea);
            vibe.vibrate(50); // 50 is time in ms
            return;
        }


        Application application = new Application();
        application.setAppsApplicationId(0);
        application.setApplicantName(etApplicantName.getText().toString());
        application.setDesignation(etDesignation.getText().toString());
        application.setMobileNumber(etMobileNo.getText().toString());
        application.setAlternativeMobile(etMobileNoAlternative.getText().toString());
        application.setSalaryRange(etSalaryRang.getText().toString());
        application.setGender(etGender.getText().toString());
        application.setAge(Integer.parseInt(etAge.getText().toString()));
        application.setFactoryName(etWorkPlace.getText().toString());
        application.setFactoryAddress(etWorkAddress.getText().toString());
        String areaId = AreaRepository.getRow(getActivity(), etArea.getText().toString()).getAreaId();
        application.setAreaId(Integer.parseInt(areaId));
        application.setQuestionId(Integer.parseInt(questionId));
        application.setQuestionText(etQuestion.getText().toString());
        application.setJoiningDate(etJoiningDate.getText().toString());
        application.setUserName(Utils.getinfo(getContext()).getUserInfo().getUsername());
        application.setCreatedAt(Utils.getCurrentDateTime());
        application.setApplicationStatus(ApplicationRepository.SYNC_PENDING);


        long rowid= ApplicationRepository.insert(getActivity(), application);

        if (rowid!= -1){
            getActivity().finish();
            Toast.makeText(getActivity(), R.string.sent_application_success, Toast.LENGTH_SHORT).show();

            //sent sync requestZ only for submitted application
            DataLoader.syncApplication(getActivity().getApplicationContext());

        }else {
            Toast.makeText(getActivity(), R.string.save_application_error, Toast.LENGTH_SHORT).show();
        }





    }


    private final void focusOnView(final View view) {
        svform.post(new Runnable() {
            @Override
            public void run() {
                svform.smoothScrollTo(0, view.getBottom());
            }
        });
    }


    @OnClick(R.id.btnApplicationList)
    public void onBtnApplicationListClicked() {
        ((ApplicationFormActivity) getActivity()).showApplicationList();
    }

    @OnClick(R.id.btnSubmitApplication)
    public void onBtnSubmitApplicationClicked() {

        sentApplication();
    }

    @OnClick(R.id.etJoiningDate)
    public void getDate() {

        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        DateTimePicker.DatePickerFragment datePickerFragment = new DateTimePicker.DatePickerFragment();
        Bundle bundle = new Bundle();

        try {
            Date date = sdf.parse(etJoiningDate.getText().toString());

            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            int year = cal.get(Calendar.YEAR);
            ArrayList<Integer> dobInt = new ArrayList<>();
            dobInt.add(year);
            dobInt.add(month);
            dobInt.add(day);
            bundle.putIntegerArrayList("dob", dobInt);
            datePickerFragment.setArguments(bundle);

        } catch (ParseException e) {
            e.printStackTrace();
        }


        datePickerFragment.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                String y = "" + year;
                String mm = "" + (month + Constant.MONTH_OFFSET);
                mm = mm.length() == 1 ? ("0" + mm) : mm;
                String day = "" + dayOfMonth;
                day = day.length() == 1 ? ("0" + day) : day;

                String fulldate = y + "-" + mm + "-" + day;


                Date strDate = null;
                try {
                    strDate = sdf.parse(fulldate);
                    if (new Date().after(strDate)) {
                        etJoiningDate.setText(fulldate);
                    } else {
                        Toast.makeText(getActivity(), R.string.msg_invalid_date, Toast.LENGTH_SHORT).show();
                        etJoiningDate.setText("");
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            }
        });
        datePickerFragment.show(getFragmentManager(), "datePickerFragment");


    }


}

