package bd.org.blast.sromikobhijog.repository;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.telephony.SmsManager;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.db.DbHelper;
import bd.org.blast.sromikobhijog.model.AskedQuestion;
import bd.org.blast.sromikobhijog.model.SmsNotificationLog;
import bd.org.blast.sromikobhijog.response.ResponseAskQuestion;
import bd.org.blast.sromikobhijog.utilis.Utils;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/24/2017.
 */

public class AskedQuestionRepository {

    private static final String TAG=AskedQuestionRepository.class.getSimpleName();


    public static void insert(Context context, List<AskedQuestion> list){





        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(AskedQuestion item: list){

            if ( Utils.isAdmin(context) && !SmsNotificationLogRepository.isNotificationSent(context, String.valueOf(item.getAskQuestionId()), Constant.NOTIFICATION_TYPE_QUESTION)){

                String message="Thanks. Your query is received and you will get reply within 72 hours.";
                sendSms(context, item.getFromMobile(), message, item);
            }


            insert(context, item);
        }
        Log.i(TAG, "insert: "+list.size() );
    }


    public static long insert(Context context, AskedQuestion item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        ContentValues cv = itemToContentValue(item);

        if (item.getAppsAskQuestionId()==null || item.getAppsAskQuestionId().equals("0")){
            long newAppsApplicationId= getLastRowID(context, DB.AskedQuestion.TABLE)+1;
            cv.put(DB.AskedQuestion.appsAskQuestionId , newAppsApplicationId);

        }
        else
            cv.put(DB.AskedQuestion.appsAskQuestionId  , item.getAppsAskQuestionId());



         long l=  db.insert(DB.AskedQuestion.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert id: "+l );

        return l;

    }




    private static void sendSms(Context context, String phone, String message, AskedQuestion item) {
        SmsManager sms = SmsManager.getDefault();
        // if message length is too long messages are divided
        List<String> messages = sms.divideMessage(message);
        for (String msg : messages) {

            PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, new Intent("SMS_SENT"), 0);
            PendingIntent deliveredIntent = PendingIntent.getBroadcast(context, 0, new Intent("SMS_DELIVERED"), 0);
            sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);

        }


        SmsNotificationLog nflg = new SmsNotificationLog();


        nflg.setSmsFrom(Utils.getHelpLineNumberWithoutPlus(context));
        nflg.setSmsTo(item.getFromMobile());
        nflg.setSmsText(message);
        nflg.setNotificationFor(Constant.NOTIFICATION_TYPE_QUESTION);
        nflg.setApplicationOrQuestionId(""+item.getAskQuestionId());
        nflg.setCreatedBy("");
        nflg.setCreatedAt(Utils.getCurrentDateTime());

        SmsNotificationLogRepository.insert(context, nflg);
    }



    public static ContentValues itemToContentValue(AskedQuestion item){
        ContentValues cv = new ContentValues();


        cv.put(DB.AskedQuestion.appsAskQuestionId , item.getAppsAskQuestionId());


        cv.put(DB.AskedQuestion.askQuestionId , item.getAskQuestionId());
        cv.put(DB.AskedQuestion.questionId , item.getQuestionId());
        cv.put(DB.AskedQuestion.fromMobile , item.getFromMobile());
        cv.put(DB.AskedQuestion.questionText , item.getQuestionText());
        cv.put(DB.AskedQuestion.questionChannel , item.getQuestionChannel());
        cv.put(DB.AskedQuestion.createdBy , item.getCreatedBy());
        cv.put(DB.AskedQuestion.createdAt , item.getCreatedAt());
        cv.put(DB.AskedQuestion.updatedBy , item.getUpdatedBy());
        cv.put(DB.AskedQuestion.updatedAt , item.getUpdatedAt());
        cv.put(DB.AskedQuestion.askQuestionStatus , item.getAskQuestionStatus());
        cv.put(DB.AskedQuestion.syncDatetime , item.getSyncDatetime());
        cv.put(DB.AskedQuestion.sync , item.getSync());

        return  cv;
    }

    public static void update(Context context, ResponseAskQuestion body) {

        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        ContentValues cv = new ContentValues();



        cv.put(DB.AskedQuestion.askQuestionId  , body.getAskQuestionId());

        cv.put(DB.AskedQuestion.sync, "y");


        db.update(DB.AskedQuestion.TABLE, cv, DB.AskedQuestion.appsAskQuestionId+"=?" , new String[]{body.getAppsAskQuestionId()});

        Log.i(TAG, "updated: "+1 );
    }

    public static List<AskedQuestion> getAll(Context context, String condition, String... args){
        List<AskedQuestion> list = new ArrayList<>();
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        Cursor cursor = null;

        if (null== condition)
            cursor = db.query("select * from "+DB.AskedQuestion.TABLE , args);
        else
            cursor = db.query("select * from "+DB.AskedQuestion.TABLE+ " where "+condition, args);

        System.out.println("select * from "+DB.AskedQuestion.TABLE+" where "+condition);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                AskedQuestion aq = new AskedQuestion();



                aq.setAppsAskQuestionId( cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.appsAskQuestionId  )));
                aq.setAskQuestionId(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.askQuestionId )));
                aq.setQuestionId(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.questionId )));
                aq.setFromMobile(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.fromMobile)));
                aq.setQuestionText(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.questionText)));



                aq.setQuestionChannel(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.questionChannel)));

                aq.setCreatedBy(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.createdBy)));
                aq.setCreatedAt(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.createdAt)));
                aq.setUpdatedBy(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.updatedBy)));
                aq.setUpdatedAt(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.updatedAt)));
                aq.setAskQuestionStatus(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.askQuestionStatus)));
                aq.setSyncDatetime(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.syncDatetime)));
                aq.setSync(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.sync)));


                list.add(aq);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static List<AskedQuestion> getUnsynced(Context context){
        return getAll(context, DB.AskedQuestion.sync+" IS NULL ", null);
    }



    public static List<AskedQuestion>  getAll(Context context){
        List<AskedQuestion> list = new ArrayList<>();

        SQLiteDatabase db = DbHelper.getDbHelper(context).getReadableDatabase();
        list.clear();
        Cursor cursor = db.rawQuery("select * from "+DB.AskedQuestion.TABLE+" ORDER BY "+DB.AskedQuestion.appsAskQuestionId, null);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                AskedQuestion aq = new AskedQuestion();

                aq.setAppsAskQuestionId( cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.appsAskQuestionId  )));
                aq.setAskQuestionId(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.askQuestionId )));
                aq.setQuestionId(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.questionId )));
                aq.setFromMobile(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.fromMobile)));
                aq.setQuestionText(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.questionText)));
                aq.setQuestionChannel(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.questionChannel)));
                aq.setCreatedBy(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.createdBy)));
                aq.setCreatedAt(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.createdAt)));
                aq.setUpdatedBy(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.updatedBy)));
                aq.setUpdatedAt(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.updatedAt)));
                aq.setAskQuestionStatus(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.askQuestionStatus)));
                aq.setSyncDatetime(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.syncDatetime)));
                aq.setSync(cursor.getString(cursor.getColumnIndex(DB.AskedQuestion.sync)));


                list.add(aq);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }


    public static  synchronized long getLastRowID(Context context,String Table){
        long lastId = 0;
        String query = "SELECT ROWID from "+ Table+ " order by ROWID DESC limit 1";
        BriteDatabase db =BriteDbHelper.getBriteDatabase(context);
        Cursor c = null;
        try {
            c = db.query(query);
            if (c != null && c.moveToFirst()) {
                lastId = c.getLong(0); //The 0 is the column index, we only have 1 column, so the index is 0
            }
        }catch (Exception e){e.printStackTrace();}
        finally {
            if (c!=null)
                c.close();
        }

        return lastId;
    }
}
