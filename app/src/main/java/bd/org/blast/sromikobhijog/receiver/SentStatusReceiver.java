package bd.org.blast.sromikobhijog.receiver;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

import bd.org.blast.sromikobhijog.activities.AskQuestionActivity;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/22/2017.
 */

public class SentStatusReceiver extends BroadcastReceiver
{

    @Override
    public void onReceive(Context context, Intent intent) {
        String s = "Unknown Error";
        switch (getResultCode()) {
            case Activity.RESULT_OK:
                s = "Message Sent Successfully !!";
                break;
            case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                s = "Generic Failure Error";
                break;
            case SmsManager.RESULT_ERROR_NO_SERVICE:
                s = "Error : No Service Available";
                break;
            case SmsManager.RESULT_ERROR_NULL_PDU:
                s = "Error : Null PDU";
                break;
            case SmsManager.RESULT_ERROR_RADIO_OFF:
                s = "Error : Radio is off";
                break;
            default:
                break;
        }
        //sendStatus
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

    }

}
