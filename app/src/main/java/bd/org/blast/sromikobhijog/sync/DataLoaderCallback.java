package bd.org.blast.sromikobhijog.sync;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/31/2017.
 */

public interface DataLoaderCallback {

     void onSuccess();
     void onFailure();

}
