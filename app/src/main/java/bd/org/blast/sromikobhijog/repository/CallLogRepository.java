package bd.org.blast.sromikobhijog.repository;

import android.Manifest;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CallLog;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.model.Calllog;
import bd.org.blast.sromikobhijog.response.ResponseAskQuestion;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/20/2017.
 */

public class CallLogRepository {

    private static final String TAG = CallLogRepository.class.getSimpleName();
    public static  String tableName="CallLog";




    public static List<Calllog> getAll(Context context, String condition) {


        //format datetime
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        long milliseconds;
        String selection=null;
        String selectionArgs[]=null;

        if (!TextUtils.isEmpty(condition)){

            try {
                Date d = formatter.parse(condition);
                 milliseconds = d.getTime();
                 selection=CallLog.Calls.DATE+" >? ";
                selectionArgs= new String[]{milliseconds+""};
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }



        List<Calllog> list = new ArrayList<>();

        ContentResolver contentResolver = context.getContentResolver();


        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_CALL_LOG) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return list;
        }
        Cursor cursor = contentResolver.query(CallLog.Calls.CONTENT_URI, null, selection, selectionArgs, CallLog.Calls.DATE + " asc");


        StringBuffer sb = new StringBuffer();
        int id = cursor.getColumnIndex(CallLog.Calls._ID);
        int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);
        sb.append("Call Details :");
        while (cursor.moveToNext()) {
            int log_id = cursor.getInt(id);
            String phNumber = cursor.getString(number);
            String callType = cursor.getString(type);
            String callDate = cursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            int callDuration = cursor.getInt(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }

            sb.append("\n id: " + log_id + "\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");

            Calllog calllog = new Calllog();
            calllog.setAppCallLogId(id);

            //format datetime
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTime(callDayTime);


            calllog.setCallDatetime( formatter.format(cal.getTime()));

            calllog.setCallDuration(callDuration);
            calllog.setCallType(dir);
            calllog.setPhoneNumber(phNumber);
            list.add(calllog);


        }





        return list;
    }




}

