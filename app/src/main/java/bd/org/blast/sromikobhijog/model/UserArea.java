
package bd.org.blast.sromikobhijog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserArea {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("area_id")
    @Expose
    private String areaId;
    @SerializedName("area_name")
    @Expose
    private String areaName;
    @SerializedName("area_location")
    @Expose
    private String areaLocation;
    @SerializedName("area_latitude")
    @Expose
    private String areaLatitude;
    @SerializedName("area_longitude")
    @Expose
    private String areaLongitude;
    @SerializedName("area_division")
    @Expose
    private String areaDivision;
    @SerializedName("area_district")
    @Expose
    private String areaDistrict;
    @SerializedName("area_upazila")
    @Expose
    private String areaUpazila;
    @SerializedName("area_union")
    @Expose
    private String areaUnion;
    @SerializedName("area_description")
    @Expose
    private String areaDescription;
    @SerializedName("publish_status")
    @Expose
    private String publishStatus;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getAreaLocation() {
        return areaLocation;
    }

    public void setAreaLocation(String areaLocation) {
        this.areaLocation = areaLocation;
    }

    public String getAreaLatitude() {
        return areaLatitude;
    }

    public void setAreaLatitude(String areaLatitude) {
        this.areaLatitude = areaLatitude;
    }

    public String getAreaLongitude() {
        return areaLongitude;
    }

    public void setAreaLongitude(String areaLongitude) {
        this.areaLongitude = areaLongitude;
    }

    public String getAreaDivision() {
        return areaDivision;
    }

    public void setAreaDivision(String areaDivision) {
        this.areaDivision = areaDivision;
    }

    public String getAreaDistrict() {
        return areaDistrict;
    }

    public void setAreaDistrict(String areaDistrict) {
        this.areaDistrict = areaDistrict;
    }

    public String getAreaUpazila() {
        return areaUpazila;
    }

    public void setAreaUpazila(String areaUpazila) {
        this.areaUpazila = areaUpazila;
    }

    public String getAreaUnion() {
        return areaUnion;
    }

    public void setAreaUnion(String areaUnion) {
        this.areaUnion = areaUnion;
    }

    public String getAreaDescription() {
        return areaDescription;
    }

    public void setAreaDescription(String areaDescription) {
        this.areaDescription = areaDescription;
    }

    public String getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

}
