package bd.org.blast.sromikobhijog.model;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/26/2017.
 */

public class SmsLog {
    String phoneNumber;// phone_number
    String smsType ; //sms_type [required] e.g INCOMING or OUTGOING
    String smsText; //sms_text
    String smsDatetime; //sms_datetime [required] e.g 2017-11-20 11:31:10
    String appSmsLogId; //app_sms_log_id

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSmsType() {
        return smsType;
    }

    public void setSmsType(String smsType) {
        this.smsType = smsType;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public String getSmsDatetime() {
        return smsDatetime;
    }

    public void setSmsDatetime(String smsDatetime) {
        this.smsDatetime = smsDatetime;
    }

    public String getAppSmsLogId() {
        return appSmsLogId;
    }

    public void setAppSmsLogId(String appSmsLogId) {
        this.appSmsLogId = appSmsLogId;
    }

    @Override
    public String toString() {
        return phoneNumber+ smsText;
    }
}
