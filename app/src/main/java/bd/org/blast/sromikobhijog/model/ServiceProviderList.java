
package bd.org.blast.sromikobhijog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceProviderList {

    @SerializedName("category_list")
    @Expose
    private List<ServiceProviderCategory> serviceProviderCategory = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<ServiceProviderCategory> getServiceProviderCategory() {
        return serviceProviderCategory;
    }

    public void setServiceProviderCategory(List<ServiceProviderCategory> serviceProviderCategory) {
        this.serviceProviderCategory = serviceProviderCategory;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
