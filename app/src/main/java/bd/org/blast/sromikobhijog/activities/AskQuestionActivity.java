package bd.org.blast.sromikobhijog.activities;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.fragment.FragmentAskQuestion;
import bd.org.blast.sromikobhijog.fragment.FragmentSendQuestion;
import bd.org.blast.sromikobhijog.fragment.MessageListFragment;
import bd.org.blast.sromikobhijog.receiver.DeliveredStatusReceiver;
import bd.org.blast.sromikobhijog.receiver.SMSReceiver;
import bd.org.blast.sromikobhijog.receiver.SentStatusReceiver;
import bd.org.blast.sromikobhijog.utilis.Utils;

import static android.Manifest.permission.SEND_SMS;

/**
 * Created by This pc on 10/18/2017.
 */

public class AskQuestionActivity extends AppCompatActivity  implements  FragmentAskQuestion.OnItemSelectListener{


    private static final String CURRENT_TAG = "Ask Question";
    public static final String TAG_QUESTION = "question";
    private static final String TAG = AskQuestionActivity.class.getSimpleName();

    SMSReceiver smsReceiver = new SMSReceiver();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    try {
        if (Utils.getinfo(this).getUserInfo().getRole().equalsIgnoreCase(Constant.USER_ADMIN ))
            showPreviousQuestionList();
        else
            showAskedQuestionFragment();

    }catch (Exception e){
        e.printStackTrace();
        showAskedQuestionFragment();
    }

    }



    private void showAskedQuestionFragment() {
        Fragment fragment = new FragmentAskQuestion();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.fragment_placeholder, fragment, CURRENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onItemSelected(String question_id, String question) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasSMSPermission = checkSelfPermission(Manifest.permission.SEND_SMS);
            if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.SEND_SMS)) {
                    showMessageOKCancel("You need to allow access to Send SMS",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[] {Manifest.permission.SEND_SMS},
                                                REQUEST_SMS);
                                    }
                                }
                            });
                    return;
                }
                requestPermissions(new String[] {Manifest.permission.SEND_SMS},
                        REQUEST_SMS);
                return;
            }

        }


        Fragment fragment = new FragmentSendQuestion();
        Bundle args= new Bundle();
        args.putString(ApplicationFormActivity.TAG_QUESTION_ID, question_id);
        args.putString(ApplicationFormActivity.TAG_QUESTION, question);

        fragment.setArguments(args);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.fragment_placeholder, fragment, CURRENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();

    }

    public void showPreviousQuestionList() {



        Fragment fragment = new MessageListFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.fragment_placeholder, fragment, CURRENT_TAG);
        fragmentTransaction.commitAllowingStateLoss();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.form_action_menu, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {

            //showApplicationList();
            if (item.getTitle().toString().equals(getString(R.string.list))){
                item.setTitle(getString(R.string.form));
                item.setIcon(R.mipmap.ic_application);
                showPreviousQuestionList();
            }else{
                item.setTitle(getString(R.string.list));
                item.setIcon(R.mipmap.ic_application_list);
                showAskedQuestionFragment();
            }
            Log.e(TAG, "onOptionsItemSelected: "+ item.getTitle().toString() );


            return true;

        }

        return super.onOptionsItemSelected(item);
    }


    private static final int REQUEST_SMS = 0;
    private static final int REQ_PICK_CONTACT = 2 ;
    private BroadcastReceiver sentStatusReceiver, deliveredStatusReceiver;



    public void sendSMSButtonClick(String phone ,
                                   String message){


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasSMSPermission = checkSelfPermission(Manifest.permission.SEND_SMS);
            if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.SEND_SMS)) {
                    showMessageOKCancel("You need to allow access to Send SMS",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[] {Manifest.permission.SEND_SMS},
                                                REQUEST_SMS);
                                    }
                                }
                            });
                    return;
                }
                requestPermissions(new String[] {Manifest.permission.SEND_SMS},
                        REQUEST_SMS);
                return;
            }
            sendMySMS(phone, message);
        }
    }


    public void sendMySMS(String phone ,
                          String message) {



        //Check if the phoneNumber is empty
        if (phone.isEmpty()) {

            Toast.makeText(getApplicationContext(), "Please Enter a Valid Phone Number", Toast.LENGTH_SHORT).show();

        } else {

            SmsManager sms = SmsManager.getDefault();
            // if message length is too long messages are divided
            List<String> messages = sms.divideMessage(message);
            for (String msg : messages) {

                PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
                PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
                sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);

            }
        }
    }
    public void onResume() {
        super.onResume();
        sentStatusReceiver= new SentStatusReceiver();
        deliveredStatusReceiver= new DeliveredStatusReceiver();


        registerReceiver(sentStatusReceiver, new IntentFilter("SMS_SENT"));
        registerReceiver(deliveredStatusReceiver, new IntentFilter("SMS_DELIVERED"));

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            registerReceiver(smsReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
    }
    public void onPause() {
        super.onPause();

        unregisterReceiver(sentStatusReceiver);
        unregisterReceiver(deliveredStatusReceiver);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            unregisterReceiver(smsReceiver);
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_SMS:
                if (grantResults.length > 0 &&  grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(getApplicationContext(), "Permission Granted, Now you can send sms. Try now", Toast.LENGTH_LONG).show();
                    //sendMySMS();

                }else {

                    Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and sms", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(SEND_SMS)) {
                            showMessageOKCancel("You need to allow access to both the permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{SEND_SMS},
                                                        REQUEST_SMS);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQ_PICK_CONTACT) {
            if (resultCode == RESULT_OK) {
                Uri contactData = data.getData();
                Cursor cursor = managedQuery(contactData, null, null, null, null);
                cursor.moveToFirst();

                String number = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Phone.NUMBER));
                //phoneEditText.setText(number);
            }
        }
    }
}
