package bd.org.blast.sromikobhijog.db;

import android.content.ContentResolver;
import android.content.Context;

import com.squareup.sqlbrite2.BriteContentResolver;
import com.squareup.sqlbrite2.BriteDatabase;
import com.squareup.sqlbrite2.SqlBrite;

import io.reactivex.schedulers.Schedulers;


/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/24/2017.
 */

public class BriteDbHelper  {
    private static SqlBrite sqlBrite;
    private static BriteDatabase briteDatabase;
    private static BriteContentResolver briteContentResolver;

    public  static  SqlBrite getSqlBrite(){
        if (sqlBrite == null)
            sqlBrite = new SqlBrite.Builder().build();

        return sqlBrite;
    }

    public  static BriteDatabase getBriteDatabase(Context context){

        if (briteDatabase == null){
            briteDatabase = getSqlBrite().wrapDatabaseHelper(DbHelper.getDbHelper(context), Schedulers.io());
        }

        return briteDatabase;
    }

    public static BriteContentResolver getBriteContentResolver(ContentResolver contentResolver){

        if (briteContentResolver == null)
            briteContentResolver = getSqlBrite().wrapContentProvider(contentResolver, Schedulers.io());

        return  briteContentResolver;
    }


}
