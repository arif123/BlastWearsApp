
package bd.org.blast.sromikobhijog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AskedQuestion implements Serializable{

    @SerializedName("ask_question_id")
    @Expose
    private String askQuestionId;
    @SerializedName("question_id")
    @Expose
    private String questionId;
    @SerializedName("from_mobile")
    @Expose
    private String fromMobile;
    @SerializedName("question_text")
    @Expose
    private String questionText;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("ask_question_status")
    @Expose
    private String askQuestionStatus;
    @SerializedName("apps_ask_question_id")
    @Expose
    private String appsAskQuestionId;

    @SerializedName("question_channel ")
    @Expose
    private String questionChannel ;

    @SerializedName("sync_datetime")
    @Expose
    private String syncDatetime;

    @Expose
    private String sync;


    public String getAskQuestionId() {
        return askQuestionId;
    }

    public void setAskQuestionId(String askQuestionId) {
        this.askQuestionId = askQuestionId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getFromMobile() {
        return fromMobile;
    }

    public void setFromMobile(String fromMobile) {
        this.fromMobile = fromMobile;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getAskQuestionStatus() {
        return askQuestionStatus;
    }

    public void setAskQuestionStatus(String askQuestionStatus) {
        this.askQuestionStatus = askQuestionStatus;
    }

    public String getAppsAskQuestionId() {
        return appsAskQuestionId;
    }

    public void setAppsAskQuestionId(String appsAskQuestionId) {
        this.appsAskQuestionId = appsAskQuestionId;
    }

    public String getSyncDatetime() {
        return syncDatetime;
    }

    public void setSyncDatetime(String syncDatetime) {
        this.syncDatetime = syncDatetime;
    }


    public String getQuestionChannel() {
        return questionChannel;
    }

    public void setQuestionChannel(String questionChannel) {
        this.questionChannel = questionChannel;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    @Override
    public String toString() {
        return questionText+ createdAt + updatedAt;
    }
}
