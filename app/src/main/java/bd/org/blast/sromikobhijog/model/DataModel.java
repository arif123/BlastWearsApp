package bd.org.blast.sromikobhijog.model;

import android.text.TextUtils;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/2/2017.
 */

public class DataModel {
    private String id;
    private  String valueEN;
    private String valueBN;

    public String getLocal() {
        return local;
    }

    public void setLocal(String local) {
        this.local = local;
    }

    private String local;
    public  static final String EN="en", BN="bn";

    public String getId() {
        return id;
    }

    public DataModel setId(String id) {
        this.id = id;
        return this;
    }

    public String getValueEN() {
        return valueEN;
    }

    public DataModel setValueEN(String valueEN) {
        this.valueEN = valueEN;
        return this;
    }

    public String getValueBN() {
        return valueBN;
    }

    public DataModel setValueBN(String valueBN) {
        this.valueBN = valueBN;
        return this;
    }

    @Override
    public String toString() {
        return TextUtils.isEmpty(local)|| local.equals(BN) ?valueBN : valueEN  ;
    }
}
