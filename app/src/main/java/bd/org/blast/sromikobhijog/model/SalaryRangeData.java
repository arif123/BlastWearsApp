
package bd.org.blast.sromikobhijog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalaryRangeData {

    @SerializedName("salary_range_list")
    @Expose
    private List<SalaryRange> salaryRangeList = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<SalaryRange> getSalaryRangeList() {
        return salaryRangeList;
    }

    public void setSalaryRangeList(List<SalaryRange> salaryRange) {
        this.salaryRangeList = salaryRange;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
