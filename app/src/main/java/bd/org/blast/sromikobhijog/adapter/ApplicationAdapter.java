package bd.org.blast.sromikobhijog.adapter;

/**
 * Created by This pc on 8/7/2017.
 */


import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.model.Application;
import bd.org.blast.sromikobhijog.repository.ApplicationRepository;

// ServiceProviderAdapter

public class ApplicationAdapter extends ArrayAdapter<Application> {
    // View lookup cache
    private static class ViewHolder {
        TextView tvTitle;
        TextView tvDescription;
        TextView tvDesignation;
        TextView tvContactNo;
        TextView tvStatus;
        TextView tvSubmitDate;
        ImageView ivSync;


    }

    private   List<Application> allitems;
    private   List<Application> data;

    public ApplicationAdapter(Context context, List<Application> items) {
        super(context, R.layout.list_item_application, items);

        allitems = new ArrayList<>(items);
        data = new ArrayList<>(items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Application item = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_application, parent, false);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            viewHolder.tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
            viewHolder.tvDesignation = (TextView) convertView.findViewById(R.id.tvDesignation);
            viewHolder.tvContactNo = (TextView) convertView.findViewById(R.id.tvContactNo);
            viewHolder.tvStatus = (TextView) convertView.findViewById(R.id.tvStatus);
            viewHolder.ivSync = (ImageView) convertView.findViewById(R.id.ivSync);
            viewHolder.tvSubmitDate = (TextView) convertView.findViewById(R.id.tvSubmitDate);

            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.tvTitle.setText(item.getApplicantName());
        viewHolder.tvDesignation.setText(item.getDesignation());
        viewHolder.tvDescription.setText(item.getQuestionText());
        viewHolder.tvContactNo.setText(item.getMobileNumber());
        String status = item.getApplicationStatus();
        viewHolder.tvStatus.setText(item.getApplicationStatus()==null ? ApplicationRepository.SYNC_PENDING: status );












        if (!TextUtils.isEmpty(status)) {

            if (status.equals(ApplicationRepository.SYNC_PENDING)) {

                viewHolder.tvStatus.setBackgroundResource(R.drawable.button_back_light_green);

            } else if (status.equals(ApplicationRepository.ACCEPTED)) {

                viewHolder.tvStatus.setBackgroundResource(R.drawable.button_back_paste);

            } else if (status.equals(ApplicationRepository.PANDING_FOR_DOC)) {

                viewHolder.tvStatus.setBackgroundResource(R.drawable.button_back_orange);

            } else  if (status.equals(ApplicationRepository.RECEIVED)) {

                viewHolder.tvStatus.setBackgroundResource(R.drawable.button_back_purple);

            } else  if (status.equals(ApplicationRepository.ON_PROGRESS)) {

                viewHolder.tvStatus.setBackgroundResource(R.drawable.button_back_green);

            } else  if (status.equals(ApplicationRepository.DISPOSAL)) {

                viewHolder.tvStatus.setBackgroundResource(R.drawable.button_back_blue);

            } else  if (status.equals(ApplicationRepository.REJECTED)) {

                viewHolder.tvStatus.setBackgroundResource(R.drawable.button_back_red);

            } else if (status.equals(ApplicationRepository.SETTLED)) {

                viewHolder.tvStatus.setBackgroundResource(R.drawable.button_back_cyan);

            }
        }else{


                viewHolder.tvStatus.setBackgroundResource(R.drawable.button_back_light_green);
        }



        if (!TextUtils.isEmpty(item.getCreatedAt())){
            viewHolder.tvSubmitDate.setText("");
            try {
                viewHolder.tvSubmitDate.setText(item.getCreatedAt());

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

                Calendar cal = Calendar.getInstance(Locale.ENGLISH);
                cal.setTime(new Date());
                Date createdAt = formatter.parse(item.getCreatedAt());

                Calendar c=Calendar.getInstance();
                c.setTime(createdAt);
                c.add(Calendar.DATE,3);

                Date toDate =new Date();
                if(status.equals(ApplicationRepository.RECEIVED) && c.getTime().compareTo(toDate)<0){


                    viewHolder.tvSubmitDate.setTextColor(Color.RED);
                }else {
                    viewHolder.tvSubmitDate.setTextColor(Color.GREEN);
                }



            }catch (Exception e){
                e.printStackTrace();
            }
        }


        if (item.getSync()==null ||   (item.getStatusSync()!=null && item.getStatusSync().equals("n"))){
            viewHolder.ivSync.setVisibility(View.VISIBLE);
        }else{
            viewHolder.ivSync.setVisibility(View.GONE);
        }


        // Return the completed view to render on screen
        return convertView;
    }


    private Filter filter;
    @NonNull
    @Override
    public Filter getFilter() {
        if (null == filter){
            filter = new ItemFilter();
        }
        return filter;
    }




    private class ItemFilter extends Filter
    {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if(constraint != null && constraint.toString().length() > 0)
            {
                ArrayList<Application> filteredItems = new ArrayList<>();

                for(int i = 0, l = allitems.size(); i < l; i++)
                {
                    Application nameList = allitems.get(i);
                    if(nameList.toString().toLowerCase().contains(constraint))
                        filteredItems.add(nameList);
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            }
            else
            {
                synchronized(this)
                {
                    result.values = allitems;
                    result.count = allitems.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            data = (ArrayList<Application>)results.values;
            notifyDataSetChanged();
            clear();
            for(int i = 0, l = data.size(); i < l; i++)
                add(data.get(i));
            notifyDataSetInvalidated();
        }
    }
}
