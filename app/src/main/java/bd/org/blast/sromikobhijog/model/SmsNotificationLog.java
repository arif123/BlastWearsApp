
package bd.org.blast.sromikobhijog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SmsNotificationLog {

    @SerializedName("log_id")
    @Expose
    private String logId;
    @SerializedName("sms_from")
    @Expose
    private String smsFrom;
    @SerializedName("sms_to")
    @Expose
    private String smsTo;
    @SerializedName("sms_text")
    @Expose
    private String smsText;
    @SerializedName("notification_for")
    @Expose
    private String notificationFor;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("apps_notification_log_id")
    @Expose
    private String appsNotificationLogId;

    @SerializedName("application_or_question_id")
    @Expose
    private String applicationOrQuestionId;


    @Expose
    private String sync;

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getSmsFrom() {
        return smsFrom;
    }

    public void setSmsFrom(String smsFrom) {
        this.smsFrom = smsFrom;
    }

    public String getSmsTo() {
        return smsTo;
    }

    public void setSmsTo(String smsTo) {
        this.smsTo = smsTo;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public String getNotificationFor() {
        return notificationFor;
    }

    public void setNotificationFor(String notificationFor) {
        this.notificationFor = notificationFor;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getAppsNotificationLogId() {
        return appsNotificationLogId;
    }

    public void setAppsNotificationLogId(String appsNotificationLogId) {
        this.appsNotificationLogId = appsNotificationLogId;
    }

    public String getApplicationOrQuestionId() {
        return applicationOrQuestionId;
    }

    public void setApplicationOrQuestionId(String applicationOrQuestionId) {
        this.applicationOrQuestionId = applicationOrQuestionId;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }

    public String getSync() {
        return sync;
    }
}
