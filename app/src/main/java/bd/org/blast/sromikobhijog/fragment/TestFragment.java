package bd.org.blast.sromikobhijog.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bd.org.blast.sromikobhijog.R;

/**
 * Created by This pc on 10/16/2017.
 */

public class TestFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.layout_aplication_form, container, false);

        return view;

    }
}
