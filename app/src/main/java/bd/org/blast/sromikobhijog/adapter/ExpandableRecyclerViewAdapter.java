package bd.org.blast.sromikobhijog.adapter;
/**
 * Created by This pc on 10/11/2017.
 */


import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.afollestad.sectionedrecyclerview.SectionedViewHolder;

import java.util.List;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.fragment.FragmentAskQuestion;
import bd.org.blast.sromikobhijog.model.CategoryQuestionList;
import bd.org.blast.sromikobhijog.model.Question;


@SuppressLint("DefaultLocale")
public class ExpandableRecyclerViewAdapter extends SectionedRecyclerViewAdapter<ExpandableRecyclerViewAdapter.MainVH> {

    private FragmentAskQuestion.OnItemSelectListener callBack;
    private List<CategoryQuestionList> categoryQuestionLists;

    public ExpandableRecyclerViewAdapter(List<CategoryQuestionList> categoryQuestionLists) {
        this.categoryQuestionLists = categoryQuestionLists;
    }

    @Override
    public int getSectionCount() {
         return categoryQuestionLists.size();
    }

    @Override
    public int getItemCount(int section) {

        if (categoryQuestionLists!= null ){
            CategoryQuestionList categoryQuestion = categoryQuestionLists.get(section);
            if (categoryQuestion!= null) {
                List<Question> quesList = categoryQuestion.getQuesList();
                if (quesList != null)
                    return  quesList.size();
            }
        }

        return 0;

        /*try {
            return categoryQuestionLists.get(section).getQuesList().size();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            return 0;
        }*/




        /*switch (section) {
            case 0:
                return 5;
            case 1:
                return 2;
            case 2:
                return 3;
            default:
                return 4;
        }*/
    }

    /*String title[] =new String[]{"শ্রম", "শ্রমিক আইন ", "মজুরি", "নিয়োগ", "চাকরি", "ছুটি", "শ্রমিক আইন "};
    String subtitle[] =new String[]{
            "আমাকে জোরপূর্বক কারখানা থেকে বের করে দিয়েছে, আইনত আমি কি কি পেতে পারি?",
            "সুপারভাইজার আমাকে ভয়ভীতি দেখিয়ে আইডি কেড়ে নিয়েছে, আমি এখন কি করবো?",
            "পিএম আমাকে সাদা কাগজে সই করার জন্য চাপ দি”েছ, সাদা কাগজে সই করলে কি কোন সমস্যা হবে?",
            "আমাকে চাকুরী হতে সাময়িকভাবে বরখাস্ত করা হয়েছে, এখন আমি কি কোন টাকা পয়সা পাবো না?",
            "আমি কি চাকুরী হতে তাৎক্ষনিক রিজাইন দিতে পারবো?"
    };*/



    @Override
    public void onBindHeaderViewHolder(MainVH holder, int section, boolean expanded) {



        try {
            if (categoryQuestionLists.get(section) != null){
                holder.title.setText(categoryQuestionLists.get(section).categoryNameBn);
                holder.caret.setImageResource(expanded ? R.drawable.ic_collapse : R.drawable.ic_expand);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {

        }


    }

    @Override
    public void onBindFooterViewHolder(MainVH holder, int section) {
        holder.title.setText(String.format("Section footer %d", section));
    }

    @Override
    public void onBindViewHolder(
            MainVH holder, int section, int relativePosition, int absolutePosition) {
        String questionBn = categoryQuestionLists.get(section).getQuesList().get(relativePosition).getQuestionBn();
        holder.title.setText( questionBn);//("sub "+String.format("S:%d, P:%d, A:%d", section, relativePosition, absolutePosition));
       // holder.title.setText( categoryQuestionLists.get(section).getQuesList().get(relativePosition).getQuestionBn());
                String.format("S:%d, P:%d, A:%d", section, relativePosition, absolutePosition);




    }

    @Override
    public int getItemViewType(int section, int relativePosition, int absolutePosition) {
        if (section == 1) {
            // VIEW_TYPE_FOOTER is -3, VIEW_TYPE_HEADER is -2, VIEW_TYPE_ITEM is -1.
            // You can return 0 or greater.
            return 0;
        }
        return super.getItemViewType(section, relativePosition, absolutePosition);
    }

    @Override
    public MainVH onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                layout = R.layout.header;
                break;
            case VIEW_TYPE_ITEM:
                layout = R.layout.list_item_main;
                break;
            case VIEW_TYPE_FOOTER:
                layout = R.layout.header;
                break;
            default:
                // Our custom item, which is the 0 returned in getItemViewType() above
                layout = R.layout.list_item_main;
                break;
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new MainVH(v, this, callBack);
    }

    public void setCallBack(FragmentAskQuestion.OnItemSelectListener callBack) {
        this.callBack = callBack;
    }

     class MainVH extends SectionedViewHolder implements View.OnClickListener {

        final TextView title;
        final ImageView caret;
        final ExpandableRecyclerViewAdapter adapter;
        Toast toast;
        FragmentAskQuestion.OnItemSelectListener callBack;

        MainVH(View itemView, ExpandableRecyclerViewAdapter adapter) {
            super(itemView);
            this.title = itemView.findViewById(R.id.title);
            this.caret = itemView.findViewById(R.id.caret);
            this.adapter = adapter;
            itemView.setOnClickListener(this);
        }

        MainVH(View itemView, ExpandableRecyclerViewAdapter adapter, FragmentAskQuestion.OnItemSelectListener callBack) {
            super(itemView);
            this.title = itemView.findViewById(R.id.title);
            this.caret = itemView.findViewById(R.id.caret);
            this.adapter = adapter;
            this.callBack = callBack;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {



            if (isFooter()) {
                // ignore footer clicks
                return;
            }

            if (isHeader()) {
                adapter.toggleSectionExpanded(getRelativePosition().section());
            } else {
                if (toast != null) {
                    toast.cancel();
                }
                /*toast = Toast.makeText(view.getContext(), getRelativePosition().toString(), Toast.LENGTH_SHORT);
                toast.show();*/

                String questionId = categoryQuestionLists.get(getRelativePosition().section()).getQuesList().get(getRelativePosition().relativePos()).getQuestionId();
                String questionTxt = categoryQuestionLists.get(getRelativePosition().section()).getQuesList().get(getRelativePosition().relativePos()).getQuestionBn();
                callBack.onItemSelected(questionId, questionTxt);



            }
        }
    }
}