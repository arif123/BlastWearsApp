package bd.org.blast.sromikobhijog.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.activities.ApplicationFormActivity;
import bd.org.blast.sromikobhijog.activities.AskQuestionActivity;
import bd.org.blast.sromikobhijog.model.AskedQuestion;
import bd.org.blast.sromikobhijog.receiver.SMSReceiver;
import bd.org.blast.sromikobhijog.repository.AskedQuestionRepository;
import bd.org.blast.sromikobhijog.sync.DataLoader;
import bd.org.blast.sromikobhijog.utilis.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Created by This pc on 10/18/2017.
 */

public class FragmentSendQuestion extends Fragment {

    public static final String TAG = FragmentSendQuestion.class.getSimpleName();


    @BindView(R.id.etQuestion)
    EditText etQuestion;
    @BindView(R.id.txtWarningMsg)
    TextView txtWarningMsg;
    @BindView(R.id.btnAskQuestion)
    Button btnAskQuestion;
    @BindView(R.id.btnAskQuestionSendSMS)
    Button btnAskQuestionSendSMS;
    @BindView(R.id.btnCancel)
    Button btnCancel;

    Unbinder unbinder;

    @BindView(R.id.etMobileNo)
    AppCompatEditText etMobileNo;

    private String questionId;
    private String MobilePhoneNo;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.layout_ask_question_send, container, false);

        unbinder = ButterKnife.bind(this, view);


        String question = getArguments().getString(ApplicationFormActivity.TAG_QUESTION);
        questionId = getArguments().getString(ApplicationFormActivity.TAG_QUESTION_ID);


        Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());
        etMobileNo.addTextChangedListener( new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith("880")) {
                    etMobileNo.setText("880");
                    Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());

                }
                if (s.toString().length()>=4){
                    String ss = s.toString().substring(3,4);
                    Log.i(TAG, "afterTextChanged: "+ss);
                    if (!"1".contains(ss)){
                        Log.i(TAG, "in: "+ss);
                        etMobileNo.setText(s.toString().substring(0,3));
                        Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());
                    }
                }

                if (s.toString().length()>=5){
                    String ss = s.toString().substring(4,5);
                    Log.i(TAG, "afterTextChanged: "+ss);
                    if (!"156789".contains(ss)){
                        Log.i(TAG, "in: "+ss);
                        etMobileNo.setText(s.toString().substring(0,4));
                        Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());
                    }
                }




                if (s.toString().length()>13){
                    etMobileNo.setText(s.toString().substring(0,13));
                    Selection.setSelection(etMobileNo.getText(), etMobileNo.getText().length());


                }

            }
        });

        etMobileNo.setText(Utils.getMobileNo(getContext()));



        etQuestion.setText(question);

        if (Utils.checkInternetConnection(getContext())) {
            txtWarningMsg.setVisibility(View.INVISIBLE);
            btnAskQuestionSendSMS.setVisibility(View.INVISIBLE);
            btnAskQuestion.setVisibility(View.VISIBLE);
        } else {
            txtWarningMsg.setVisibility(View.VISIBLE);
            btnAskQuestionSendSMS.setVisibility(View.VISIBLE);
            btnAskQuestion.setVisibility(View.INVISIBLE);
        }


        return view;
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.btnAskQuestion, R.id.btnAskQuestionSendSMS, R.id.btnCancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnAskQuestion:
                askQuestionViaInternet();
                break;
            case R.id.btnAskQuestionSendSMS:
                askQuestionViaSMS();
                break;
            case R.id.btnCancel:
                getActivity().finish();
                break;
        }
    }



    private AskedQuestion validate(){

        /*1. user_name[Optional, If login]e.g admin
        2. question_id[required]
        3. question_text[required]
        4. mobile[required] e.g 8801756144149
        5. apps_ask_question_id[Optional, For sync]

*/
        String mobileNo = etMobileNo.getText().toString();
        if (TextUtils.isEmpty(mobileNo)){
            etQuestion.requestFocus();
            etMobileNo.setError(getString(R.string.required));
            return null;
        }

        String question = etQuestion.getText().toString();
        if (TextUtils.isEmpty(mobileNo)){
            etQuestion.requestFocus();
            etQuestion.setError(getString(R.string.required));
            return null;
        }

        Utils.setMobileNo(getContext(), mobileNo);

        AskedQuestion aq = new AskedQuestion();
        aq.setQuestionText(question );
        aq.setQuestionId(questionId);
        aq.setFromMobile(mobileNo);
        aq.setSync(null);
        aq.setCreatedAt(Utils.getCurrentDateTime());

        return aq;

    }

    private void askQuestionViaInternet() {

        if (TextUtils.isEmpty(etMobileNo.getText())  || etMobileNo.getText().length()<=12 ){
            etMobileNo.setError("Mobile No. Invalid");
            etMobileNo.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(etQuestion.getText())  || etQuestion.getText().length()<=0 ){
            etQuestion.setError("Not Empty");
            etQuestion.requestFocus();
            return;
        }



        AskedQuestion aq = validate();
        if (aq == null) return;

        String prefixMobileNo="88";
        aq.setFromMobile(aq.getFromMobile().startsWith("0")? prefixMobileNo+aq.getFromMobile():aq.getFromMobile());
        aq.setQuestionChannel(Constant.CHANNEL_APP);
        long rowId = AskedQuestionRepository.insert(getContext(), aq);

        if (rowId != -1){
            Toast.makeText(getActivity(), R.string.sent_msg_success, Toast.LENGTH_SHORT).show();
            getActivity().finish();
            //sent sync requestZ only for submitted application
            DataLoader.syncAskedQuestion(getActivity().getApplicationContext());
        }else {
            Toast.makeText(getActivity(), "Try again.", Toast.LENGTH_SHORT).show();
        }



    }

    private void askQuestionViaSMS() {

        if (TextUtils.isEmpty(etQuestion.getText())  || etQuestion.getText().length()<=0 ){
            etQuestion.setError("Not Empty");
            etQuestion.requestFocus();
            return;
        }

        AskedQuestion aq = validate();
        if (aq == null) return;

        aq.setQuestionChannel(Constant.CHANNEL_SMS);
        ((AskQuestionActivity)getActivity()).sendSMSButtonClick(getString(R.string.blast_helpline_no), Constant.SMS_PREFIX+"#"+aq.getQuestionId()+": "+aq.getQuestionText());

        AskedQuestionRepository.insert(getContext(), aq);

        Toast.makeText(getActivity(), "Sending SMS", Toast.LENGTH_SHORT).show();
        getActivity().finish();
    }



    public void onViewClicked() {
        ((AskQuestionActivity) getActivity()).showPreviousQuestionList();
    }
}

