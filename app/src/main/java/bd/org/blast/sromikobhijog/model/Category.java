
package bd.org.blast.sromikobhijog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("info_search_category_id")
    @Expose
    public int infoSearchCategoryId;
    @SerializedName("category_name_en")
    @Expose
    public String categoryNameEn;
    @SerializedName("category_name_bn")
    @Expose
    public String categoryNameBn;
    @SerializedName("publish_status")
    @Expose
    public String publishStatus;
    @SerializedName("updated_at")
    @Expose
    public String updatedAt;

    public int getInfoSearchCategoryId() {
        return infoSearchCategoryId;
    }

    public void setInfoSearchCategoryId(int infoSearchCategoryId) {
        this.infoSearchCategoryId = infoSearchCategoryId;
    }

    public String getCategoryNameEn() {
        return categoryNameEn;
    }

    public void setCategoryNameEn(String categoryNameEn) {
        this.categoryNameEn = categoryNameEn;
    }

    public String getCategoryNameBn() {
        return categoryNameBn;
    }

    public void setCategoryNameBn(String categoryNameBn) {
        this.categoryNameBn = categoryNameBn;
    }

    public String getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
