package bd.org.blast.sromikobhijog.model;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/20/2017.
 */

public class Calllog {

    String phoneNumber; //e.g 8801675794194
    String callType;// e.g INCOMING or OUTGOING
    int callDuration; // [in second]
    String callDatetime;//[required] e.g 2017-11-20 11:31:10
    int appCallLogId;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public int getCallDuration() {
        return callDuration;
    }

    public void setCallDuration(int callDuration) {
        this.callDuration = callDuration;
    }

    public String getCallDatetime() {
        return callDatetime;
    }

    public void setCallDatetime(String callDatetime) {
        this.callDatetime = callDatetime;
    }

    public int getAppCallLogId() {
        return appCallLogId;
    }

    public void setAppCallLogId(int appCallLogId) {
        this.appCallLogId = appCallLogId;
    }

    @Override
    public String toString() {
        return callDuration +" "+phoneNumber + " type:"+callType + " sce: "+callDuration + " <"+callDatetime+">";
    }
}
