
package bd.org.blast.sromikobhijog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AskedQuestionReplyData {

    @SerializedName("all_reply")
    @Expose
    private List<AskedQuestionReply> askedQuestionReply = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<AskedQuestionReply> getAskedQuestionReply() {
        return askedQuestionReply;
    }

    public void setAskedQuestionReply(List<AskedQuestionReply> askedQuestionReply) {
        this.askedQuestionReply = askedQuestionReply;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
