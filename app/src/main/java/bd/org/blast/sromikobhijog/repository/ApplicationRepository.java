package bd.org.blast.sromikobhijog.repository;

import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.telephony.SmsManager;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.model.Application;
import bd.org.blast.sromikobhijog.model.Category;
import bd.org.blast.sromikobhijog.model.SmsNotificationLog;
import bd.org.blast.sromikobhijog.response.ResponseApplication;
import bd.org.blast.sromikobhijog.utilis.Utils;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/24/2017.
 */

public class ApplicationRepository {


    public static final String RECEIVED="RECEIVED";
    public static final String RECEIVED_BN="গৃহীত";
    public static final String ACCEPTED= "ACCEPTED";
    public static final String ACCEPTED_BN= "অনুমত";
    public static final String REJECTED= "REJECTED";
    public static final String REJECTED_BN= "প্রত্যাখ্যাত";
    public static final String ON_PROGRESS= "ON-PROGRESS";
    public static final String ON_PROGRESS_BN= "চলোমান";
    public static final String PANDING_FOR_DOC = "PANDING-FOR-DOC";
    public static final String PANDING_FOR_DOC_BN = "অপেক্ষমান ";
    public static final String SETTLED = "SETTLED";
    public static final String SETTLED_BN = "সম্পূর্ণ নিষ্পত্তি";
    public static final String DISPOSAL = "DISPOSAL";
    public static final String DISPOSAL_BN = "ডিস্পোজাল";
    public static final String SYNC_PENDING = "SYNC PENDING";

    private static final String TAG=ApplicationRepository.class.getSimpleName();



    public static void insert(Context context, List<Application> list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(Application item: list){

            if ( Utils.isAdmin(context) && !SmsNotificationLogRepository.isNotificationSent(context, String.valueOf(item.getApplicationId()), Constant.NOTIFICATION_TYPE_APPLICATION)){
                String message="Thanks. Your application is received and you will get reply within 72 hours.";
                sendSms(context, item.getMobileNumber(), message, item);
            }


            insert(context, item);
        }
        Log.i(TAG, "insert: "+list.size() );
    }

    private static void sendSms(Context context, String phone, String message, Application item) {
        SmsManager sms = SmsManager.getDefault();
        // if message length is too long messages are divided
        List<String> messages = sms.divideMessage(message);
        for (String msg : messages) {

            PendingIntent sentIntent = PendingIntent.getBroadcast(context, 0, new Intent("SMS_SENT"), 0);
            PendingIntent deliveredIntent = PendingIntent.getBroadcast(context, 0, new Intent("SMS_DELIVERED"), 0);
            sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);

        }


        SmsNotificationLog nflg = new SmsNotificationLog();


        nflg.setSmsFrom(Utils.getHelpLineNumberWithoutPlus(context));
        nflg.setSmsTo(item.getMobileNumber());
        nflg.setSmsText(message);
        nflg.setNotificationFor(Constant.NOTIFICATION_TYPE_APPLICATION);
        nflg.setApplicationOrQuestionId(""+item.getApplicationId());
        nflg.setCreatedBy("");
        nflg.setCreatedAt(Utils.getCurrentDateTime());

        SmsNotificationLogRepository.insert(context, nflg);
    }


    public static long insert(Context context, Application item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        ContentValues cv = new ContentValues();




        cv.put(DB.Applications.applicationId  , item.getApplicationId());

        if (item.getAppsApplicationId()==0){
            long newAppsApplicationId= getLastRowID(context, DB.Applications.TABLE)+1;
            cv.put(DB.Applications.appsApplicationId , newAppsApplicationId);

        }
        else
        cv.put(DB.Applications.appsApplicationId  , item.getAppsApplicationId());

        cv.put(DB.Applications.userName  , item.getUserName());
        cv.put(DB.Applications.questionId  , item.getQuestionId());
        cv.put(DB.Applications.questionText  , item.getQuestionText());
        cv.put(DB.Applications.applicantName  , item.getApplicantName());
        cv.put(DB.Applications.mobileNumber  , item.getMobileNumber());
        cv.put(DB.Applications.alternativeMobile  , item.getAlternativeMobile());
        cv.put(DB.Applications.designation  , item.getDesignation());
        cv.put(DB.Applications.salaryRange  , item.getSalaryRange());
        cv.put(DB.Applications.gender  , item.getGender());
        cv.put(DB.Applications.age  , item.getAge());
        cv.put(DB.Applications.joiningDate  , item.getJoiningDate());
        cv.put(DB.Applications.factoryName  , item.getFactoryName());
        cv.put(DB.Applications.factoryAddress  , item.getFactoryAddress());
        cv.put(DB.Applications.areaId  , item.getAreaId());
        cv.put(DB.Applications.updatedAt  , item.getUpdatedAt());
        cv.put(DB.Applications.updatedBy  , item.getUpdatedBy());
        cv.put(DB.Applications.createdAt  , item.getCreatedAt());
        cv.put(DB.Applications.createdBy  , item.getCreatedBy());
        cv.put(DB.Applications.applicationSubmittedBy  , item.getApplicationSubmittedBy());
        cv.put(DB.Applications.syncDatetime  , item.getSyncDatetime());
        cv.put(DB.Applications.sync,  item.getSync());
        cv.put(DB.Applications.applicationStatus,  item.getApplicationStatus());
        cv.put(DB.Applications.statusSync,  item.getStatusSync());


        long l = db.insert(DB.Applications.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert id: "+l );

        return l;

    }

    public static void update(Context context, ResponseApplication body) {

        try {
            BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
            ContentValues cv = new ContentValues();
            cv.put(DB.Applications. applicationId  , body.getApplicationId());
            cv.put(DB.Applications.sync, "y");
            cv.put(DB.Applications.statusSync, "y");
            cv.putNull(DB.Applications. updatedAt );

            int value = db.update(DB.Applications.TABLE, cv, DB.Applications.appsApplicationId + "=?", new String[]{body.getAppsApplicationId()});

            Log.i(TAG, "updated: "+value  );
        }catch (Exception e){
            e.printStackTrace();
        }


    }


    public static void updateStatus(Context context,  Application application) {

        try{

            BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

            ContentValues cv = new ContentValues();
            cv.put(DB.Applications. applicationId  , application.getApplicationId());
            cv.put(DB.Applications.applicationStatus, application.getApplicationStatus() );
            cv.put(DB.Applications.statusSync, application.getStatusSync());
            cv.putNull(DB.Applications.updatedAt );

            int update = db.update(DB.Applications.TABLE, cv, DB.Applications.appsApplicationId + "=?", new String[]{String.valueOf(application.getAppsApplicationId())});

            Log.i(TAG, "updated: "+update );

        }catch (Exception e){
            e.printStackTrace();
        }

    }




    public static List<Application> getAll(Context context, String condition, String... args){
        List<Application> list = new ArrayList<>();
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        Cursor cursor = null;

        if (null== condition)
             cursor = db.query("select * from "+DB.Applications.TABLE +" order by "+DB.Applications.createdAt+" desc"   , args);
        else
             cursor = db.query("select * from "+DB.Applications.TABLE+ " where "+condition +" order by "+DB.Applications.createdAt+" desc", args);

        System.out.println("select * from "+DB.Applications.TABLE+" where "+condition);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                Application application = new Application();

                application.setJoiningDate(cursor.getString(cursor.getColumnIndex(DB.Applications.joiningDate)));
                application.setQuestionText(cursor.getString(cursor.getColumnIndex(DB.Applications.questionText)));
                application.setAreaId(cursor.getInt(cursor.getColumnIndex(DB.Applications.areaId)));
                application.setFactoryAddress(cursor.getString(cursor.getColumnIndex(DB.Applications.factoryAddress)));
                application.setFactoryName(cursor.getString(cursor.getColumnIndex(DB.Applications.factoryName)));
                application.setAge(cursor.getInt(cursor.getColumnIndex(DB.Applications.age)));
                application.setGender(cursor.getString(cursor.getColumnIndex(DB.Applications.gender)));
                application.setSalaryRange(cursor.getString(cursor.getColumnIndex(DB.Applications.salaryRange)));
                application.setAlternativeMobile(cursor.getString(cursor.getColumnIndex(DB.Applications.alternativeMobile)));
                application.setMobileNumber(cursor.getString(cursor.getColumnIndex(DB.Applications.mobileNumber)));
                application.setDesignation(cursor.getString(cursor.getColumnIndex(DB.Applications.designation)));
                application.setApplicantName(cursor.getString(cursor.getColumnIndex(DB.Applications.applicantName)));
                application.setApplicationId(cursor.getInt(cursor.getColumnIndex(DB.Applications.applicationId)));
                application.setAppsApplicationId(cursor.getInt(cursor.getColumnIndex(DB.Applications.appsApplicationId)));
                application.setQuestionId(cursor.getInt(cursor.getColumnIndex(DB.Applications.questionId)));
                application.setUserName(cursor.getString(cursor.getColumnIndex(DB.Applications.userName)));
                application.setApplicationStatus(cursor.getString(cursor.getColumnIndex(DB.Applications.applicationStatus)));
                application.setStatusSync(cursor.getString(cursor.getColumnIndex(DB.Applications.statusSync)));
                application.setUpdatedAt(cursor.getString(cursor.getColumnIndex(DB.Applications.updatedAt)));
                application.setUpdatedBy(cursor.getString(cursor.getColumnIndex(DB.Applications.updatedBy)));
                application.setCreatedAt(cursor.getString(cursor.getColumnIndex(DB.Applications.createdAt)));
                application.setCreatedBy(cursor.getString(cursor.getColumnIndex(DB.Applications.createdBy)));
                application.setApplicationSubmittedBy(cursor.getString(cursor.getColumnIndex(DB.Applications.applicationSubmittedBy)));
                application.setSyncDatetime(cursor.getString(cursor.getColumnIndex(DB.Applications.syncDatetime)));
                application.setSync(cursor.getString(cursor.getColumnIndex(DB.Applications.sync)));


                list.add(application);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static List<Application> getUnsynced(Context context){
        return getAll(context, DB.Applications.sync+" IS NULL ", null);
    }

    public static boolean isExist(Context context, String applicationId){
        List<Application> all = getAll(context, DB.Applications.applicationId + " like ? ", new String[]{applicationId});
        if (all != null && !all.isEmpty() && all.get(0) != null && all.get(0).getApplicationId() != 0)
            return true;
            else
                return false;

    }


    public static List<Application> getUnsyncedStatus(Context context){
        return getAll(context, DB.Applications.statusSync+" LIKE 'n' ", null);
    }

    /*public static int updateSync(Context context, int saleId, int serverSaleId){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        ContentValues cv = new ContentValues();
        cv.put(DB.Sale.COL_SERVER_SALE_ID, serverSaleId);
        cv.put(DB.Sale.COL_SYNC, "done");

        return db.update(DB.Sale.TABLE, cv, DB.Sale.COL_SALE_ID+"=?",new String[]{saleId+""});
    }*/

    public static  synchronized long getLastRowID(Context context,String Table){
        long lastId = 0;
        String query = "SELECT ROWID from "+ Table+ " order by ROWID DESC limit 1";
        BriteDatabase db =BriteDbHelper.getBriteDatabase(context);
        Cursor c = null;
        try {
             c = db.query(query);
            if (c != null && c.moveToFirst()) {
                lastId = c.getLong(0); //The 0 is the column index, we only have 1 column, so the index is 0
            }
        }catch (Exception e){e.printStackTrace();}
        finally {
            if (c!=null)
            c.close();
        }

        return lastId;
    }
}
