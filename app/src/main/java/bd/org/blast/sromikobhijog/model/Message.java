package bd.org.blast.sromikobhijog.model;

/**
 * Created by This pc on 10/19/2017.
 */

public class Message {

   public String sender;
    public String receiver;
    public String msg;
    public String date;

    public Message(String sender, String receiver, String msg, String date) {
        this.sender = sender;
        this.receiver = receiver;
        this.msg = msg;
        this.date = date;
    }
}
