package bd.org.blast.sromikobhijog.repository;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.CallLog;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bd.org.blast.sromikobhijog.model.Calllog;
import bd.org.blast.sromikobhijog.model.SmsLog;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/20/2017.
 */

public class SMSLogRepository {

    private static final String TAG = SMSLogRepository.class.getSimpleName();
    public static  String tableName="SMSLog";




    public static List<SmsLog> getAll(Context context, String condition) {


        //format datetime
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
        long milliseconds;
        String selection=null;
        String selectionArgs[]=null;

        if (!TextUtils.isEmpty(condition)){

            try {
                Date d = formatter.parse(condition);
                 milliseconds = d.getTime();
                 selection=" date >? ";
                selectionArgs= new String[]{milliseconds+""};
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }



        List<SmsLog> list = new ArrayList<>();

        ContentResolver contentResolver = context.getContentResolver();


        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return list;
        }

        // Create Inbox box URI
        Uri inboxURI = Uri.parse("content://sms");

        // List required columns
        String[] reqCols = new String[]{"_id", "address", "body"};

        // Get Content Resolver object, which will deal with Content
        // Provider


        // Fetch Inbox SMS Message from Built-in Content Provider
        Cursor cursor = contentResolver.query(inboxURI                 , null, selection, selectionArgs, "date asc");



        int id = cursor.getColumnIndex(Telephony.Sms._ID);
        int number = cursor.getColumnIndex(Telephony.Sms.ADDRESS);
        int type = cursor.getColumnIndex(Telephony.Sms.TYPE);
        int date = cursor.getColumnIndex(Telephony.Sms.DATE);
        int body = cursor.getColumnIndex(Telephony.Sms.BODY);
        while (cursor.moveToNext()) {

            int log_id = cursor.getInt(id);
            String phNumber = cursor.getString(number);
            String callType = cursor.getString(type);
            String callDate = cursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String smsText = cursor.getString(body);

            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case 2:
                    dir = "OUTGOING";
                    break;

                case 1:
                    dir = "INCOMING";
                    break;

            }


            SmsLog smsLog = new SmsLog();
            smsLog.setAppSmsLogId(""+log_id);

            //format datetime
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTime(callDayTime);


            smsLog.setSmsDatetime( formatter.format(cal.getTime()));
            smsLog.setSmsText(smsText);
            smsLog.setSmsType(dir);
            smsLog.setPhoneNumber(phNumber);
            list.add(smsLog);


        }





        return list;
    }




}

