package bd.org.blast.sromikobhijog.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseAskQuestionReply {

    @SerializedName("ask_question_response_id")
    @Expose
    private Integer askQuestionResponseId;
    @SerializedName("ask_question_id")
    @Expose
    private String askQuestionId;
    @SerializedName("apps_ask_ques_response_id")
    @Expose
    private String appsAskQuesResponseId;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getAskQuestionResponseId() {
        return askQuestionResponseId;
    }

    public void setAskQuestionResponseId(Integer askQuestionResponseId) {
        this.askQuestionResponseId = askQuestionResponseId;
    }

    public String getAskQuestionId() {
        return askQuestionId;
    }

    public void setAskQuestionId(String askQuestionId) {
        this.askQuestionId = askQuestionId;
    }

    public String getAppsAskQuesResponseId() {
        return appsAskQuesResponseId;
    }

    public void setAppsAskQuesResponseId(String appsAskQuesResponseId) {
        this.appsAskQuesResponseId = appsAskQuesResponseId;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}