package bd.org.blast.sromikobhijog.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import bd.org.blast.sromikobhijog.R;

/**
 * Created by This pc on 10/15/2017.
 */

public class TestActivity  extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_test);
        setContentView(R.layout.layout_test);
    }
}
