package bd.org.blast.sromikobhijog.fragment;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.adapter.ExpandableRecyclerViewAdapter;
import bd.org.blast.sromikobhijog.model.CategoryQuestionList;
import bd.org.blast.sromikobhijog.repository.QuestionRepository;


/**
 * Created by Arif on 10/15/2017.
 */


public class FragmentAskQuestion extends Fragment {

    private RecyclerView mRecyclerView;
    private ExpandableRecyclerViewAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    OnItemSelectListener mCallback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        ViewGroup view = (ViewGroup) inflater.inflate(
                R.layout.layout_recycler_header_footer, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview);



        DividerItemDecoration horizontalDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                DividerItemDecoration.VERTICAL);
        Drawable horizontalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.horizontal_divider);
        horizontalDecoration.setDrawable(horizontalDivider);
        mRecyclerView.addItemDecoration(horizontalDecoration);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);


        // specify an adapter
        List<CategoryQuestionList> categoryQuestionLists = QuestionRepository.getAll(getContext());

        mAdapter = new ExpandableRecyclerViewAdapter(categoryQuestionLists);
        mAdapter.collapseAllSections();
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setCallBack(mCallback);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnItemSelectListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }






    public interface OnItemSelectListener{
        public void onItemSelected(String question_id, String msg);
    }






}
