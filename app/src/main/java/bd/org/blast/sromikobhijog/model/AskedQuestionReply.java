
package bd.org.blast.sromikobhijog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AskedQuestionReply {

    @SerializedName("ask_question_id")
    @Expose
    private String askQuestionId;
    @SerializedName("question_text")
    @Expose
    private String questionText;
    @SerializedName("from_mobile")
    @Expose
    private String fromMobile;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("question_channel")
    @Expose
    private String questionChannel;
    @SerializedName("ask_question_status")
    @Expose
    private String askQuestionStatus;
    @SerializedName("response_text")
    @Expose
    private String responseText;
    @SerializedName("response_id")
    @Expose
    private String responseId;
    @SerializedName("ask_ques_status")
    @Expose
    private String askQuesStatus;
    @SerializedName("apps_ask_ques_response_id")
    @Expose
    private String appsAskQuesResponseId;

    public String getAskQuestionId() {
        return askQuestionId;
    }

    public void setAskQuestionId(String askQuestionId) {
        this.askQuestionId = askQuestionId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getFromMobile() {
        return fromMobile;
    }

    public void setFromMobile(String fromMobile) {
        this.fromMobile = fromMobile;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getQuestionChannel() {
        return questionChannel;
    }

    public void setQuestionChannel(String questionChannel) {
        this.questionChannel = questionChannel;
    }

    public String getAskQuestionStatus() {
        return askQuestionStatus;
    }

    public void setAskQuestionStatus(String askQuestionStatus) {
        this.askQuestionStatus = askQuestionStatus;
    }

    public String getResponseText() {
        return responseText;
    }

    public void setResponseText(String responseText) {
        this.responseText = responseText;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }

    public String getAskQuesStatus() {
        return askQuesStatus;
    }

    public void setAskQuesStatus(String askQuesStatus) {
        this.askQuesStatus = askQuesStatus;
    }

    public String getAppsAskQuesResponseId() {
        return appsAskQuesResponseId;
    }

    public void setAppsAskQuesResponseId(String appsAskQuesResponseId) {
        this.appsAskQuesResponseId = appsAskQuesResponseId;
    }

}
