package bd.org.blast.sromikobhijog.utilis;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Selection;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.model.DataModel;
import bd.org.blast.sromikobhijog.response.AuthResponse;
import bd.org.blast.sromikobhijog.response.UserInfo;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by This pc on 10/18/2017.
 */

public class Utils {



    private static final String PREFS_NAME = "wears";


    public static final String KEY_INIT = "init";

    public static final String USER_TYPE = "user_type";
    public static final String ADMIN = "admin";
    public static final String SELFHELP = "selfhelp";
    public static final String LAWYER = "lawyer";


    public static final String  username =     "username";
    public static final String  email="email";
    public static final String  name="name";
    public static final String  role="role";
    public static final String  api_key="api_key";

    public static final String  mobileNoKey="mobile_no_key";

    public static void setMobileNo(Context context, String mobileNo){
        savePreference(context, mobileNoKey, mobileNo);
    }

    public static String getMobileNo(Context context){
        return getPreference(context, mobileNoKey);
    }


    public static boolean checkInternetConnection(Context context){
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }


    public static  void savePreference(Context context, AuthResponse authResponse){
        savePreference(context, username, authResponse.getUserInfo().getUsername());
        savePreference(context, email, authResponse.getUserInfo().getEmail());
        savePreference(context, name, authResponse.getUserInfo().getName());
        savePreference(context, role, authResponse.getUserInfo().getRole());
        savePreference(context, api_key, authResponse.getApiKey());
    }

    public static  void savePreference(Context context, String key, String value){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static  String getPreference(Context context, String key){
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        return prefs.getString(key, null);

    }

    public static AuthResponse getinfo(Context context){

        AuthResponse authResponse =new AuthResponse();
        authResponse.setApiKey(getPreference(context, api_key));

        UserInfo userInfo =new UserInfo();
        userInfo.setUsername(getPreference(context, username));
        userInfo.setEmail(getPreference(context, email));
        userInfo.setName(getPreference(context, name));
        userInfo.setRole(getPreference(context, role));

        authResponse.setUserInfo(userInfo);

        return authResponse;

    }

    public static boolean isAdmin(Context context){


        try {
            String role= Utils.getinfo(context).getUserInfo().getRole();
            if (role.equalsIgnoreCase(Constant.USER_ADMIN))
                return true;

        }catch (Exception e){
            e.printStackTrace();
        }

        return false;
    }

    public static  void clearPreference(Context context){
        SharedPreferences.Editor editor = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();

    }

    public static String getPrefsName() {
        return PREFS_NAME;
    }


    public static String getCurrentDateTime(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTime(new Date());
        //cal.add(Calendar.HOUR, -1);
        //cal.add(Calendar.MINUTE, -5);

        return formatter.format(cal.getTime());
    }


    public static void popupSingle(final Context ctx, final TextView textView, final View viewForClick, List<DataModel> allLabels) {


        if (null != ctx && ctx instanceof Activity)
            hideKeyBoard((Activity) ctx);


        String values[] = new String[allLabels.size()];
        for (int i = 0; i < values.length; i++) values[i] = allLabels.get(i).toString();

        //TODO
        String selectedItemValue = textView.getText().toString();


        final AlertDialog.Builder builderSingle = new AlertDialog.Builder(ctx);
        builderSingle.setIcon(R.mipmap.ic_application);
        builderSingle.setTitle(R.string.select_any);


        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                ctx,
                android.R.layout.select_dialog_singlechoice);
        arrayAdapter.addAll(values);

        builderSingle.setNegativeButton(
                R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


        if (TextUtils.isEmpty(textView.getText())) {
            builderSingle.setAdapter(
                    arrayAdapter,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String strName = arrayAdapter.getItem(which);
                            textView.setText(strName);
                            dialog.dismiss();

                        }
                    });
        } else {

            int selectedItemIndex = 0;
            for (int i = 0; i < values.length; i++) {
                if (values[i].equals(selectedItemValue)) {
                    selectedItemIndex = i;
                    break;
                }
            }

            builderSingle.setSingleChoiceItems(values, selectedItemIndex, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);
                    textView.setText(strName);
                    dialog.dismiss();

                }
            });

        }


        //builderSingle.show();
        if (null != viewForClick) {
            viewForClick.setFocusable(false);
            viewForClick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    builderSingle.show();
                }
            });
        }
    }




    public static void hideKeyBoard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            if (activity.getCurrentFocus() != null)
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    public static void changeNumber(final Context context){

        final String TAG ="changeNumber";

        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogLayout = inflater.inflate(R.layout.customalertdialog_layout, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogLayout);
        final TextView indtruction = (TextView)dialogLayout.findViewById(R.id.text_instruction);
        final EditText userInput = (EditText)dialogLayout.findViewById(R.id.user_input);
        userInput.setText(TextUtils.isEmpty(Utils.getMobileNo(context))?"":Utils.getMobileNo(context));

        Selection.setSelection(userInput.getText(), userInput.getText().length());
        userInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().startsWith("880")) {
                    userInput.setText("880");
                    Selection.setSelection(userInput.getText(), userInput.getText().length());

                }
                if (s.toString().length() >= 4) {
                    String ss = s.toString().substring(3, 4);
                    Log.i(TAG, "afterTextChanged: " + ss);
                    if (!"1".contains(ss)) {
                        Log.i(TAG, "in: " + ss);
                        userInput.setText(s.toString().substring(0, 3));
                        Selection.setSelection(userInput.getText(), userInput.getText().length());
                    }
                }

                if (s.toString().length() >= 5) {
                    String ss = s.toString().substring(4, 5);
                    Log.i(TAG, "afterTextChanged: " + ss);
                    if (!"156789".contains(ss)) {
                        Log.i(TAG, "in: " + ss);
                        userInput.setText(s.toString().substring(0, 4));
                        Selection.setSelection(userInput.getText(), userInput.getText().length());
                    }
                }


                if (s.toString().length() > 13) {
                    userInput.setText(s.toString().substring(0, 13));
                    Selection.setSelection(userInput.getText(), userInput.getText().length());


                }

            }
        });


        builder.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.setPositiveButton(context.getString(R.string.chage), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                String userInputContent = userInput.getText().toString();
                if(!TextUtils.isEmpty(userInputContent)){
                    Toast.makeText(context, context.getString(R.string.change_cell_no_msg), Toast.LENGTH_LONG).show();
                    Utils.setMobileNo(context, userInputContent);
                    return;
                }
                //indtruction.setText(userInputContent);
            }
        });
        AlertDialog customAlertDialog = builder.create();
        customAlertDialog.setTitle(context.getString(R.string.chage_cellphone_no));
        customAlertDialog.show();
    }

    public static String getHelpLineNumber(Context context){
       return context.getString(R.string.blast_helpline_no);

    }

    public static String getHelpLineNumberWithoutPlus(Context context){
        String helpLineNo = context.getString(R.string.blast_helpline_no);
        if (helpLineNo.startsWith("+"))
            return helpLineNo.substring(1);
        else
            return helpLineNo;


    }


}
