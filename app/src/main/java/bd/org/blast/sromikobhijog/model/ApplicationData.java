package bd.org.blast.sromikobhijog.model;


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/12/2017.
 */




public class ApplicationData {

    @SerializedName("my_applications")
    @Expose
    private List<Application> myApplications = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<Application> getMyApplications() {
        return myApplications;
    }

    public void setMyApplications(List<Application> myApplications) {
        this.myApplications = myApplications;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}