
package bd.org.blast.sromikobhijog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AreaData {

    @SerializedName("area_list")
    @Expose
    private List<Area> areaList = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<Area> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<Area> areaList) {
        this.areaList = areaList;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
