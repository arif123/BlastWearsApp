package bd.org.blast.sromikobhijog.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.db.DbHelper;
import bd.org.blast.sromikobhijog.model.Area;
import bd.org.blast.sromikobhijog.model.AreaData;
import bd.org.blast.sromikobhijog.model.SalaryRange;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/31/2017.
 */

public class AreaRepository {

    private static final String TAG="AreaRepository";


    public static void insert(Context context, AreaData list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(Area item: list.getAreaList()){
            insert(context, item);
        }
        Log.i(TAG, "insert: "+list.getAreaList().size() );
    }


    public static void insert(Context context, Area item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);


        ContentValues cv = new ContentValues();

        cv.put(DB.Area.areaId , item.getAreaId());
        cv.put(DB.Area.areaName , item.getAreaName());
        cv.put(DB.Area.areaLocation , item.getAreaLocation());
        cv.put(DB.Area.areaLatitude , item.getAreaLatitude());
        cv.put(DB.Area.areaLongitude , item.getAreaLongitude());
        cv.put(DB.Area.publishStatus , item.getPublishStatus());
        cv.put(DB.Area.updatedAt , item.getUpdatedAt());



        db.insert(DB.Area.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert: "+1 );

    }



    private static List<Area> list = new ArrayList<>();
    public static List<Area> getAll(Context context){
        SQLiteDatabase db = DbHelper.getDbHelper(context).getReadableDatabase();
        list.clear();
        Cursor cursor = db.rawQuery("select * from "+DB.Area.TABLE+" ORDER BY "+DB.Area.areaName, null);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                Area sr = new Area();


                sr.setAreaId(cursor.getString(cursor.getColumnIndex(DB.Area.areaId )));
                sr.setAreaName(cursor.getString(cursor.getColumnIndex(DB.Area.areaName )));
                sr.setAreaLocation(cursor.getString(cursor.getColumnIndex(DB.Area.areaLocation )));
                sr.setAreaLatitude(cursor.getString(cursor.getColumnIndex(DB.Area.areaLatitude )));
                sr.setAreaLongitude(cursor.getString(cursor.getColumnIndex(DB.Area.areaLongitude )));
                sr.setPublishStatus(cursor.getString(cursor.getColumnIndex(DB.Area.publishStatus )));
                sr.setUpdatedAt(cursor.getString(cursor.getColumnIndex(DB.Area.updatedAt )));




                list.add(sr);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static int  getAreaId(Context context, String areaName){

        int areaId=0;

        SQLiteDatabase db = DbHelper.getDbHelper(context).getReadableDatabase();
        list.clear();
        Cursor cursor = db.query(DB.Area.TABLE, new String[]{DB.Area.areaId}, DB.Area.areaName, new String[]{areaName},null, null, null);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                Area sr = new Area();


                areaId= cursor.getInt(cursor.getColumnIndex(DB.Area.areaId ));





                list.add(sr);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return areaId;

    }


    public static Area getRow(Context context, String areaName){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = db.query("select * from "+DB.Area.TABLE+" WHERE "+DB.Area.areaName+" LIKE ? LIMIT 1",new String[]{areaName+""});
        if(cursor.getCount()>0){
            cursor.moveToFirst();

            Area area = new Area();


            area.setAreaId(cursor.getString(cursor.getColumnIndex(DB.Area.areaId )));
            area.setAreaName(cursor.getString(cursor.getColumnIndex(DB.Area.areaName )));
            area.setAreaLocation(cursor.getString(cursor.getColumnIndex(DB.Area.areaLocation )));
            area.setAreaLatitude(cursor.getString(cursor.getColumnIndex(DB.Area.areaLatitude )));
            area.setAreaLongitude(cursor.getString(cursor.getColumnIndex(DB.Area.areaLongitude )));
            area.setPublishStatus(cursor.getString(cursor.getColumnIndex(DB.Area.publishStatus )));
            area.setUpdatedAt(cursor.getString(cursor.getColumnIndex(DB.Area.updatedAt )));

            cursor.close();

            return area;
        }

        return null;
    }
}
