
package bd.org.blast.sromikobhijog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceProviderCategory {

    @SerializedName("services_provider_category_id")
    @Expose
    private String servicesProviderCategoryId;
    @SerializedName("category_name_en")
    @Expose
    private String categoryNameEn;
    @SerializedName("category_name_bn")
    @Expose
    private String categoryNameBn;
    @SerializedName("publish_status")
    @Expose
    private String publishStatus;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("services_provider_list")
    @Expose
    private List<ServicesProvider> servicesProviderList = null;

    public String getServicesProviderCategoryId() {
        return servicesProviderCategoryId;
    }

    public void setServicesProviderCategoryId(String servicesProviderCategoryId) {
        this.servicesProviderCategoryId = servicesProviderCategoryId;
    }

    public String getCategoryNameEn() {
        return categoryNameEn;
    }

    public void setCategoryNameEn(String categoryNameEn) {
        this.categoryNameEn = categoryNameEn;
    }

    public String getCategoryNameBn() {
        return categoryNameBn;
    }

    public void setCategoryNameBn(String categoryNameBn) {
        this.categoryNameBn = categoryNameBn;
    }

    public String getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<ServicesProvider> getServicesProviderList() {
        return servicesProviderList;
    }

    public void setServicesProviderList(List<ServicesProvider> servicesProviderList) {
        this.servicesProviderList = servicesProviderList;
    }

}
