package bd.org.blast.sromikobhijog.sync;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.http.GetService;
import bd.org.blast.sromikobhijog.http.PostService;
import bd.org.blast.sromikobhijog.http.RetrofitClient;
import bd.org.blast.sromikobhijog.model.Application;
import bd.org.blast.sromikobhijog.model.ApplicationData;
import bd.org.blast.sromikobhijog.model.AreaData;
import bd.org.blast.sromikobhijog.model.AskedQuestion;
import bd.org.blast.sromikobhijog.model.AskedQuestionData;
import bd.org.blast.sromikobhijog.model.AskedQuestionReply;
import bd.org.blast.sromikobhijog.model.AskedQuestionReplyData;
import bd.org.blast.sromikobhijog.model.Calllog;
import bd.org.blast.sromikobhijog.model.CategoryData;
import bd.org.blast.sromikobhijog.model.CategoryQuestionData;
import bd.org.blast.sromikobhijog.model.QuestionAnswerData;
import bd.org.blast.sromikobhijog.model.SalaryRangeData;
import bd.org.blast.sromikobhijog.model.ServiceProviderList;
import bd.org.blast.sromikobhijog.model.SmsLog;
import bd.org.blast.sromikobhijog.model.SmsNotificationLog;
import bd.org.blast.sromikobhijog.model.SmsNotificationLogData;
import bd.org.blast.sromikobhijog.model.TopicData;
import bd.org.blast.sromikobhijog.model.UserAreaData;
import bd.org.blast.sromikobhijog.repository.ApplicationRepository;
import bd.org.blast.sromikobhijog.repository.AreaRepository;
import bd.org.blast.sromikobhijog.repository.AskedQuestionReplyRepository;
import bd.org.blast.sromikobhijog.repository.AskedQuestionRepository;
import bd.org.blast.sromikobhijog.repository.CallLogRepository;
import bd.org.blast.sromikobhijog.repository.CategoryRepository;
import bd.org.blast.sromikobhijog.repository.QuestionAnswerRepository;
import bd.org.blast.sromikobhijog.repository.QuestionRepository;
import bd.org.blast.sromikobhijog.repository.SMSLogRepository;
import bd.org.blast.sromikobhijog.repository.SalaryRangeRepository;
import bd.org.blast.sromikobhijog.repository.ServiceProviderRepository;
import bd.org.blast.sromikobhijog.repository.SmsNotificationLogRepository;
import bd.org.blast.sromikobhijog.repository.SyncRepository;
import bd.org.blast.sromikobhijog.repository.TopicRepository;
import bd.org.blast.sromikobhijog.repository.UserAreaRepository;
import bd.org.blast.sromikobhijog.response.ResponseApplication;
import bd.org.blast.sromikobhijog.response.ResponseAskQuestion;
import bd.org.blast.sromikobhijog.response.ResponseAskQuestionReply;
import bd.org.blast.sromikobhijog.response.ResponseCallLog;
import bd.org.blast.sromikobhijog.response.ResponseSMSLog;
import bd.org.blast.sromikobhijog.response.ResponseSmsNotificationLog;
import bd.org.blast.sromikobhijog.utilis.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/31/2017.
 */

public class DataLoader {

    private static final String TAG = DataLoader.class.getSimpleName();
    private static int loadInit = 0;
    private static int loadCount = 0;

    private static int isSuccess = 1;
    private static int isFail = 0;


    private static int applicationRequestCount = 0;
    private static int applicationResponseCount = 0;

    private static int applicationStatusRequestCount = 0;
    private static int applicationStatusResponseCount = 0;

    private static int askQuestionRequestCount = 0;
    private static int askQuestionResponseCount = 0;

    private static int askQuestionReplyRequestCount = 0;
    private static int askQuestionReplyResponseCount = 0;
    private static int notificationLogRequestCount = 0;
    private static int notificationLognResponseCount = 0;

    public static boolean isLoadFinished() {
        return loadInit == loadCount;
    }

    public static void loadAll(Context context) {




        try {
            String userRole=Utils.getinfo(context).getUserInfo().getRole();
            if (userRole.equalsIgnoreCase(Constant.USER_ADMIN))
                loadForAdmin(context);
            else if (userRole.equalsIgnoreCase(Constant.USER_LAWYER))
                loadForLawyer(context);
            else if (userRole.equalsIgnoreCase(Constant.USER_TU_LEADER))
                loadForTULeader(context);
            else
                loadForNormal(context);

        }catch (Exception e){
            e.printStackTrace();
            loadForNormal(context);
        }



    }

    private static void loadForNormal(Context context) {
        Log.i(TAG, "Normal User......... " );
        loadServiceProvider(context);
        loadSearchCategory(context);
        loadTopicsOfCategory(context);
        loadQuestionAnswer(context);
        loadCategoryWiseQuestionsList(context);

        syncAskedQuestion(context);
        loadAskedQuestion(context);
        syncAskedQuestionReply(context);
        loadMyAskedQuestionReply(context);
    }

    private static void loadForTULeader(Context context) {
        Log.i(TAG, "TU Leader......... " );

        loadServiceProvider(context);
        loadSearchCategory(context);
        loadTopicsOfCategory(context);
        loadQuestionAnswer(context);
        loadCategoryWiseQuestionsList(context);
        loadArea(context);
        loadUserArea(context);
        loadSalaryRange(context);


        syncApplication(context);
        getApplication(context);

    }

    private static void loadForLawyer(Context context) {
        Log.i(TAG, "Lawyer......... " );

        loadServiceProvider(context);
        loadSearchCategory(context);
        loadTopicsOfCategory(context);
        loadQuestionAnswer(context);
        loadCategoryWiseQuestionsList(context);



        syncApplicationStatus(context);
        getApplication(context);

    }

    private static void loadForAdmin(Context context) {

        Log.i(TAG, "Helpline/Admin......... " );

        loadServiceProvider(context);
        loadSearchCategory(context);
        loadTopicsOfCategory(context);
        loadQuestionAnswer(context);
        loadCategoryWiseQuestionsList(context);
        loadArea(context);
        loadUserArea(context);
        loadSalaryRange(context);
        syncCallLog(context);
        syncSMSLog(context);
        getNotificationLog(context);
        syncNotificationLog(context);
        syncApplicationStatus(context);
        syncApplication(context);
        getApplication(context);
        syncAskedQuestion(context);
        loadAskedQuestion(context);
        syncAskedQuestionReply(context);
        loadAllAskedQuestionReply(context);

    }


    public static void loadAll(Context context, DataLoaderCallback callback) {
        // SyncRepository.DEFAULT(context);

        loadServiceProvider(context, callback);
        loadSearchCategory(context, callback);
        loadTopicsOfCategory(context, callback);
        loadQuestionAnswer(context, callback);
        loadArea(context, callback);
        loadUserArea(context, callback);
        loadSalaryRange(context, callback);
        loadCategoryWiseQuestionsList(context, callback);


        syncCallLog(context, callback);
        syncSMSLog(context, callback);
        getNotificationLog(context, callback);
        syncNotificationLog(context, callback);

        syncApplicationStatus(context, callback);
        syncApplication(context, callback);
        getApplication(context, callback);
        syncAskedQuestion(context, callback);
        loadAskedQuestion(context, callback);

        syncAskedQuestionReply(context, callback);
        loadAllAskedQuestionReply(context, callback);
        loadMyAskedQuestionReply(context, callback);




    }


    public static void getNotificationLog(Context context) {
        getNotificationLog(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "getNotificationLog");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "getNotificationLog");

            }
        });
    }

    public static void getNotificationLog(final Context context, final DataLoaderCallback callback) {
        String api_key = Utils.getinfo(context).getApiKey();
        String user_name = Utils.getinfo(context).getUserInfo().getUsername();
        String lastSync = Utils.getinfo(context).getUserInfo().getUsername();


        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);
        Call<SmsNotificationLogData> call = getService.getSmsNotifications(api_key, user_name, lastSync);
        call.enqueue(new Callback<SmsNotificationLogData>() {
            @Override
            public void onResponse(Call<SmsNotificationLogData> call, Response<SmsNotificationLogData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {


                    List<SmsNotificationLog> allSmsNotification = response.body().getAllSmsNotification();
                    if (allSmsNotification != null){
                        for (SmsNotificationLog application : allSmsNotification) {
                            application.setSync("y");

                        }

                        SmsNotificationLogRepository.insert(context, allSmsNotification);
                    }

                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.SmsNotificationLog.TABLE);
            }

            @Override
            public void onFailure(Call<SmsNotificationLogData> call, Throwable t) {

                loadCount++;
                callback.onFailure();

            }
        });
    }


    public static void syncNotificationLog(Context context) {
        syncNotificationLog(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "syncNotificationLog");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "syncNotificationLog");

            }
        });
    }

    public static void syncNotificationLog(final Context context, final DataLoaderCallback callback) {

        if (!isSyncNotificationLogDone()) {
            Log.i(TAG, "syncNotificationLog: old sync in progress");
            return;
        }

        Log.i(TAG, "syncNotificationLog: new sync ...");

        //
        List<SmsNotificationLog> smsNotificationLogs = SmsNotificationLogRepository.getUnsynced(context);
        String api_key = Utils.getinfo(context).getApiKey();
        String user_name = Utils.getinfo(context).getUserInfo().getUsername();


        notificationLogRequestCount = smsNotificationLogs.size();
        notificationLognResponseCount = 0;

        for (SmsNotificationLog smsNotificationLog : smsNotificationLogs) {

            PostService postService = RetrofitClient.getInstance().create(PostService.class);
            Call<ResponseSmsNotificationLog> call = postService.sendSmsNotificationLog(
                    api_key,
                    user_name,
                    smsNotificationLog.getSmsFrom(),
                    smsNotificationLog.getSmsTo(),
                    smsNotificationLog.getNotificationFor(),
                    smsNotificationLog.getSmsText(),
                    smsNotificationLog.getAppsNotificationLogId(),
                    smsNotificationLog.getApplicationOrQuestionId()
                    );
            call.enqueue(new Callback<ResponseSmsNotificationLog>() {
                @Override
                public void onResponse(Call<ResponseSmsNotificationLog> call, Response<ResponseSmsNotificationLog> response) {

                    if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                        SmsNotificationLogRepository.update(context, response.body());
                    }

                    notificationLognResponseCount++;
                    callback.onSuccess();
                }

                @Override
                public void onFailure(Call<ResponseSmsNotificationLog> call, Throwable t) {

                    notificationLognResponseCount++;
                    callback.onFailure();

                }
            });

        }


    }


    public static void syncSMSLog(Context context) {
        syncSMSLog(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "syncSMSLog");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "syncSMSLog");

            }
        });
    }

    public static void syncSMSLog(final Context context, final DataLoaderCallback callback) {


        Log.i(TAG, "syncSMSLog: new sync ...");

        //


        String condition = SyncRepository.getRow(context, SMSLogRepository.tableName);
        final List<SmsLog> calllogList = SMSLogRepository.getAll(context, condition);
        String api_key = Utils.getinfo(context).getApiKey();
        String user_name = Utils.getinfo(context).getUserInfo().getUsername();


        for (final SmsLog smsLog :
                calllogList) {

            PostService postService = RetrofitClient.getInstance().create(PostService.class);


            Call<ResponseSMSLog> call = postService.smsLogSync(

                    api_key,
                    user_name,
                    smsLog.getPhoneNumber(),
                    smsLog.getSmsType(),
                    smsLog.getSmsText(),
                    smsLog.getSmsDatetime(),
                    smsLog.getAppSmsLogId()
            );
            call.enqueue(new Callback<ResponseSMSLog>() {
                @Override
                public void onResponse(Call<ResponseSMSLog> call, Response<ResponseSMSLog> response) {

                    if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {


                        SyncRepository.insert(context, SMSLogRepository.tableName, smsLog.getSmsDatetime());
                    }

                    callback.onSuccess();
                }

                @Override
                public void onFailure(Call<ResponseSMSLog> call, Throwable t) {

                    callback.onFailure();

                }
            });

        }


    }


    public static void syncCallLog(Context context) {
        syncCallLog(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "syncCallLog");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "syncCallLog");

            }
        });
    }

    public static void syncCallLog(final Context context, final DataLoaderCallback callback) {


        Log.i(TAG, "syncCallLog: new sync ...");

        //


        String condition = SyncRepository.getRow(context, CallLogRepository.tableName);
        final List<Calllog> calllogList = CallLogRepository.getAll(context, condition);
        String api_key = Utils.getinfo(context).getApiKey();
        String user_name = Utils.getinfo(context).getUserInfo().getUsername();


        for (final Calllog calllog :
                calllogList) {

            PostService postService = RetrofitClient.getInstance().create(PostService.class);
            Call<ResponseCallLog> call = postService.callLogSync(

                    api_key,
                    user_name,
                    calllog.getPhoneNumber(),
                    calllog.getCallType(),
                    calllog.getCallDuration(),
                    calllog.getCallDatetime(),
                    calllog.getAppCallLogId()
            );
            call.enqueue(new Callback<ResponseCallLog>() {
                @Override
                public void onResponse(Call<ResponseCallLog> call, Response<ResponseCallLog> response) {

                    if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {


                        SyncRepository.insert(context, CallLogRepository.tableName, calllog.getCallDatetime());
                    }

                    callback.onSuccess();
                }

                @Override
                public void onFailure(Call<ResponseCallLog> call, Throwable t) {

                    callback.onFailure();

                }
            });

        }


    }


    public static void syncAskedQuestion(Context context) {
        syncAskedQuestion(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "syncApplication");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "syncApplication");

            }
        });
    }

    public static void syncAskedQuestion(final Context context, final DataLoaderCallback callback) {

        if (!isAskQuestionSyncDone()) {
            Log.i(TAG, "syncAskedQuestion: old sync in progress");
            return;
        }

        Log.i(TAG, "syncAskedQuestion: new sync ...");

        //
        List<AskedQuestion> askedQuestionList = AskedQuestionRepository.getUnsynced(context);
        String api_key = Utils.getinfo(context).getApiKey();
        String user_name = Utils.getinfo(context).getUserInfo().getUsername();


        askQuestionRequestCount = askedQuestionList.size();
        askQuestionResponseCount = 0;

        for (AskedQuestion aq :
                askedQuestionList) {

            PostService postService = RetrofitClient.getInstance().create(PostService.class);
            Call<ResponseAskQuestion> call = postService.submitAskedQuestion(

                    api_key,
                    user_name,
                    aq.getQuestionId(),
                    aq.getQuestionText(),
                    aq.getFromMobile(),
                    aq.getAppsAskQuestionId(),
                    aq.getQuestionChannel()
            );
            call.enqueue(new Callback<ResponseAskQuestion>() {
                @Override
                public void onResponse(Call<ResponseAskQuestion> call, Response<ResponseAskQuestion> response) {

                    if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                        AskedQuestionRepository.update(context, response.body());
                    }

                    askQuestionResponseCount++;
                    callback.onSuccess();
                }

                @Override
                public void onFailure(Call<ResponseAskQuestion> call, Throwable t) {

                    askQuestionResponseCount++;
                    callback.onFailure();

                }
            });

        }


    }


    public static void syncAskedQuestionReply(Context context) {
        syncAskedQuestionReply(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "syncAskedQuestionReply");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "syncAskedQuestionReply");

            }
        });
    }

    public static void syncAskedQuestionReply(final Context context, final DataLoaderCallback callback) {

        if (!isAskQuestionReplySyncDone()) {
            Log.i(TAG, "syncAskedQuestionReply: old sync in progress");
            return;
        }

        Log.i(TAG, "syncAskedQuestionReply: new sync ...");

        //
        List<AskedQuestionReply> askedQuestionList = AskedQuestionReplyRepository.getUnsynced(context);
        String api_key = Utils.getinfo(context).getApiKey();
        String user_name = Utils.getinfo(context).getUserInfo().getUsername();


        askQuestionReplyRequestCount = askedQuestionList.size();
        askQuestionReplyResponseCount = 0;

        for (AskedQuestionReply aqr :
                askedQuestionList) {

            PostService postService = RetrofitClient.getInstance().create(PostService.class);
            Call<ResponseAskQuestionReply> call = postService.askQuestionReply(

                    api_key,
                    user_name,
                    aqr.getAskQuestionId(),
                    aqr.getQuestionChannel(),
                    aqr.getResponseText(),
                    aqr.getCreatedAt(),
                    aqr.getAppsAskQuesResponseId()

            );
            call.enqueue(new Callback<ResponseAskQuestionReply>() {
                @Override
                public void onResponse(Call<ResponseAskQuestionReply> call, Response<ResponseAskQuestionReply> response) {

                    if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                        AskedQuestionReplyRepository.update(context, response.body());
                    }

                    askQuestionReplyResponseCount++;
                    callback.onSuccess();
                }

                @Override
                public void onFailure(Call<ResponseAskQuestionReply> call, Throwable t) {

                    askQuestionReplyResponseCount++;
                    callback.onFailure();

                }
            });

        }


    }


    public static void loadAskedQuestion(Context context) {
        loadAskedQuestion(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "loadAskedQuestion");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "loadAskedQuestion");

            }
        });
    }

    public static void loadAskedQuestion(final Context context, final DataLoaderCallback callback) {
        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);

        String userMobileNo = Utils.getMobileNo(context);
        String api_key = Utils.getinfo(context).getApiKey();
        if (TextUtils.isEmpty(userMobileNo)) {
            loadCount++;
            Log.i(TAG, "loadAskedQuestion: no mobile number");
            return;
        }

        Call<AskedQuestionData> call = getService.getMyQuestion(api_key, userMobileNo, "");
        call.enqueue(new Callback<AskedQuestionData>() {
            @Override
            public void onResponse(Call<AskedQuestionData> call, Response<AskedQuestionData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                    AskedQuestionRepository.insert(context, response.body().getMyQuestions());
                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.AskedQuestion.TABLE);
            }

            @Override
            public void onFailure(Call<AskedQuestionData> call, Throwable t) {

                loadCount++;
                callback.onFailure();

            }
        });
    }


    public static void loadAllAskedQuestionReply(Context context) {
        loadAllAskedQuestionReply(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "loadAllAskedQuestionReply");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "loadAllAskedQuestionReply");

            }
        });
    }

    public static void loadAllAskedQuestionReply(final Context context, final DataLoaderCallback callback) {
        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);

        /*String userMobileNo=Utils.getMobileNo(context);
        String api_key = Utils.getinfo(context).getApiKey();*/

        String api_key = "BLAST02301120171010V1";
        String user_name = "blast";
        String date_from = "";
        if (TextUtils.isEmpty(user_name)) {
            loadCount++;
            Log.i(TAG, "loadAllAskedQuestionReply: no user_name ");
            return;
        }

        Call<AskedQuestionReplyData> call = getService.getAllAskedQuestionReply(api_key, user_name, "");
        call.enqueue(new Callback<AskedQuestionReplyData>() {
            @Override
            public void onResponse(Call<AskedQuestionReplyData> call, Response<AskedQuestionReplyData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                    // TODO: 11/29/2017 add db insert
                    //AskedQuestionRepository.insert(context, response.body().getMyQuestions());
                    Log.i(TAG, "loadAllAskedQuestionReply: " + response.body().getAskedQuestionReply());
                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.AskedQuestionReply.TABLE);
            }

            @Override
            public void onFailure(Call<AskedQuestionReplyData> call, Throwable t) {

                t.printStackTrace();
                Log.i(TAG, "loadAllAskedQuestionReply: " + t.getMessage());

                loadCount++;
                callback.onFailure();

            }
        });
    }


    public static void loadMyAskedQuestionReply(Context context) {
        loadMyAskedQuestionReply(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "loadMyAskedQuestionReply");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "loadMyAskedQuestionReply");

            }
        });
    }

    public static void loadMyAskedQuestionReply(final Context context, final DataLoaderCallback callback) {
        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);

        String mobile = Utils.getMobileNo(context);
        String api_key = Utils.getinfo(context).getApiKey();
        String date_from = "";


        if (TextUtils.isEmpty(mobile)) {
            loadCount++;
            Log.i(TAG, "loadMyAskedQuestionReply: no mobile number");
            return;
        }

        Call<AskedQuestionReplyData> call = getService.getMyAskedQuestionReply(mobile, date_from);
        call.enqueue(new Callback<AskedQuestionReplyData>() {
            @Override
            public void onResponse(Call<AskedQuestionReplyData> call, Response<AskedQuestionReplyData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                    // TODO: 11/29/2017 add db insert
                    AskedQuestionReplyRepository.insert(context, response.body().getAskedQuestionReply());
                    Log.i(TAG, "loadMyAskedQuestionReply: " + response.body().getAskedQuestionReply());
                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.AskedQuestionReply.TABLE);
            }

            @Override
            public void onFailure(Call<AskedQuestionReplyData> call, Throwable t) {

                t.printStackTrace();
                Log.i(TAG, "loadAllAskedQuestionReply: " + t.getMessage());

                loadCount++;
                callback.onFailure();

            }
        });
    }


    public static void syncApplication(Context context) {
        syncApplication(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "syncApplication");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "syncApplication");

            }
        });
    }

    public static void syncApplication(final Context context, final DataLoaderCallback callback) {

        if (!isSyncDone()) {
            Log.i(TAG, "syncApplication: old sync in progress");
            return;
        }

        Log.i(TAG, "syncApplication: new sync ...");

        //
        List<Application> applicationList = ApplicationRepository.getUnsynced(context);
        String api_key = Utils.getinfo(context).getApiKey();
        String user_name = Utils.getinfo(context).getUserInfo().getUsername();


        applicationRequestCount = applicationList.size();
        applicationResponseCount = 0;

        for (Application app : applicationList) {

            PostService postService = RetrofitClient.getInstance().create(PostService.class);
            Call<ResponseApplication> call = postService.submitApplication(
                    app.getAppsApplicationId(),
                    api_key,
                    user_name,
                    //// TODO: 11/5/2017  add real application id  app.getQuestionId() ,
                    app.getQuestionId(),
                    app.getQuestionText(),
                    app.getApplicantName(),
                    app.getMobileNumber(),
                    app.getAlternativeMobile(),
                    app.getDesignation(),
                    app.getSalaryRange(),
                    app.getGender(),
                    app.getAge(),
                    app.getJoiningDate(),
                    app.getFactoryName(),
                    app.getFactoryAddress(),
                    app.getAreaId());
            call.enqueue(new Callback<ResponseApplication>() {
                @Override
                public void onResponse(Call<ResponseApplication> call, Response<ResponseApplication> response) {

                    if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                        ApplicationRepository.update(context, response.body());
                    }

                    applicationResponseCount++;
                    callback.onSuccess();
                }

                @Override
                public void onFailure(Call<ResponseApplication> call, Throwable t) {

                    applicationResponseCount++;
                    callback.onFailure();

                }
            });

        }


    }


    public static void syncApplicationStatus(Context context) {
        syncApplicationStatus(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "syncApplicationStatus");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "syncApplicationStatus");

            }
        });
    }

    public static void syncApplicationStatus(final Context context, final DataLoaderCallback callback) {

        if (!isStatusSyncDone()) {
            Log.i(TAG, "syncApplicationStatus: old sync in progress");
            return;
        }

        Log.i(TAG, "syncApplicationStatus: new sync ...");

        //
        List<Application> applicationListUpdated = ApplicationRepository.getUnsyncedStatus(context);
        String api_key = Utils.getinfo(context).getApiKey();
        String user_name = Utils.getinfo(context).getUserInfo().getUsername();


        applicationStatusRequestCount = applicationListUpdated.size();
        applicationStatusResponseCount = 0;


        //update status
        for (Application app : applicationListUpdated) {

            PostService postService = RetrofitClient.getInstance().create(PostService.class);
            Call<ResponseApplication> call = postService.updateApplication(
                    api_key,
                    user_name,
                    app.getApplicationId(),
                    app.getAppsApplicationId(),
                    app.getApplicationStatus()
            );
            call.enqueue(new Callback<ResponseApplication>() {
                @Override
                public void onResponse(Call<ResponseApplication> call, Response<ResponseApplication> response) {

                    if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                        ApplicationRepository.update(context, response.body());
                    }

                    applicationStatusResponseCount++;
                    callback.onSuccess();
                }

                @Override
                public void onFailure(Call<ResponseApplication> call, Throwable t) {

                    applicationStatusResponseCount++;
                    callback.onFailure();

                }
            });

        }


    }


    //

    public static void getApplication(Context context) {
        getApplication(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "getApplication");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "getApplication");

            }
        });
    }

    public static void getApplication(final Context context, final DataLoaderCallback callback) {
        String api_key = Utils.getinfo(context).getApiKey();
        String user_name = Utils.getinfo(context).getUserInfo().getUsername();
        String lastSync = Utils.getinfo(context).getUserInfo().getUsername();


        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);
        Call<ApplicationData> call = getService.getMyLocationApplication(api_key, user_name, lastSync);
        call.enqueue(new Callback<ApplicationData>() {
            @Override
            public void onResponse(Call<ApplicationData> call, Response<ApplicationData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                    List<Application> applicationList = response.body().getMyApplications();

                   if (applicationList != null){
                       for (Application application : applicationList) {
                           application.setSync("y");
                           application.setStatusSync("y");
                       }

                       ApplicationRepository.insert(context, applicationList);
                   }

                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.Applications.TABLE);
            }

            @Override
            public void onFailure(Call<ApplicationData> call, Throwable t) {

                loadCount++;
                callback.onFailure();

            }
        });
    }


    private static boolean isStatusSyncDone() {
        return applicationStatusRequestCount == applicationStatusResponseCount;
    }

    private static boolean isSyncDone() {
        return applicationRequestCount == applicationResponseCount;
    }

    private static boolean isAskQuestionSyncDone() {
        return askQuestionRequestCount == askQuestionResponseCount;
    }

    private static boolean isAskQuestionReplySyncDone() {
        return askQuestionReplyRequestCount == askQuestionReplyResponseCount;
    }

    private static boolean isSyncNotificationLogDone() {
        return notificationLogRequestCount == askQuestionReplyResponseCount;
    }


    public static void loadServiceProvider(Context context) {
        loadServiceProvider(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "loadSearchCategory");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "loadSearchCategory");

            }
        });
    }

    public static void loadServiceProvider(final Context context, final DataLoaderCallback callback) {
        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);
        Call<ServiceProviderList> call = getService.getCategoryWiseServiceProviderList();
        call.enqueue(new Callback<ServiceProviderList>() {
            @Override
            public void onResponse(Call<ServiceProviderList> call, Response<ServiceProviderList> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                    ServiceProviderRepository.insertCat(context, response.body().getServiceProviderCategory());
                }

                loadCount++;
                callback.onSuccess();

                SyncRepository.insert(context, DB.ServiceProviderCategory.TABLE);
                SyncRepository.insert(context, DB.ServiceProvider.TABLE);
            }

            @Override
            public void onFailure(Call<ServiceProviderList> call, Throwable t) {

                loadCount++;
                callback.onFailure();

            }
        });
    }


    public static void loadSearchCategory(Context context) {
        loadSearchCategory(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "loadSearchCategory");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "loadSearchCategory");

            }
        });
    }

    public static void loadSearchCategory(final Context context, final DataLoaderCallback callback) {
        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);
        Call<CategoryData> call = getService.getCategory();
        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                    CategoryRepository.insert(context, response.body().getCategory());
                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.Category.TABLE);
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {

                loadCount++;
                callback.onFailure();

            }
        });
    }


    public static void loadTopicsOfCategory(Context context) {
        loadTopicsOfCategory(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "loadTopicsOfCategory");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "loadTopicsOfCategory");

            }
        });
    }

    public static void loadTopicsOfCategory(final Context context, final DataLoaderCallback callback) {
        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);
        Call<TopicData> call = getService.getTopic(1);
        call.enqueue(new Callback<TopicData>() {
            @Override
            public void onResponse(Call<TopicData> call, Response<TopicData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                    TopicRepository.insert(context, response.body());
                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.Topic.TABLE);
            }

            @Override
            public void onFailure(Call<TopicData> call, Throwable t) {

                loadCount++;
                callback.onFailure();

            }
        });
    }


    public static void loadQuestionAnswer(Context context) {
        loadQuestionAnswer(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "loadQuestionAnswer");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "loadQuestionAnswer");

            }
        });
    }

    public static void loadQuestionAnswer(final Context context, final DataLoaderCallback callback) {
        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);
        Call<QuestionAnswerData> call = getService.getQuestionAnswer(1);
        call.enqueue(new Callback<QuestionAnswerData>() {
            @Override
            public void onResponse(Call<QuestionAnswerData> call, Response<QuestionAnswerData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                    QuestionAnswerRepository.insert(context, response.body());
                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.QuestionAnswer.TABLE);
            }

            @Override
            public void onFailure(Call<QuestionAnswerData> call, Throwable t) {

                loadCount++;
                callback.onFailure();

            }
        });
    }


    public static void loadArea(Context context) {
        loadArea(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "loadArea");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "loadArea");

            }
        });
    }

    public static void loadArea(final Context context, final DataLoaderCallback callback) {
        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);
        Call<AreaData> call = getService.getArea("BLAST02301120171010V1");
        call.enqueue(new Callback<AreaData>() {
            @Override
            public void onResponse(Call<AreaData> call, Response<AreaData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                    AreaRepository.insert(context, response.body());
                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.Area.TABLE);
            }

            @Override
            public void onFailure(Call<AreaData> call, Throwable t) {

                loadCount++;
                callback.onFailure();

            }
        });
    }


    public static void loadUserArea(Context context) {
        loadUserArea(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "loadUserArea");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "loadUserArea");

            }
        });
    }

    public static void loadUserArea(final Context context, final DataLoaderCallback callback) {
        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);
        Call<UserAreaData> call = getService.getUserArea("BLAST02301120171010V1", "admin");
        call.enqueue(new Callback<UserAreaData>() {
            @Override
            public void onResponse(Call<UserAreaData> call, Response<UserAreaData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                    UserAreaRepository.insert(context, response.body());
                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.UserArea.TABLE);
            }

            @Override
            public void onFailure(Call<UserAreaData> call, Throwable t) {

                loadCount++;
                callback.onFailure();

            }
        });
    }


    public static void loadSalaryRange(Context context) {
        loadSalaryRange(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "loadSalaryRange");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "loadSalaryRange");

            }
        });
    }

    public static void loadSalaryRange(final Context context, final DataLoaderCallback callback) {
        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);
        Call<SalaryRangeData> call = getService.getSalaryRangeList("BLAST02301120171010V1");
        call.enqueue(new Callback<SalaryRangeData>() {
            @Override
            public void onResponse(Call<SalaryRangeData> call, Response<SalaryRangeData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {
                    SalaryRangeRepository.insert(context, response.body());
                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.SalaryRange.TABLE);
            }

            @Override
            public void onFailure(Call<SalaryRangeData> call, Throwable t) {

                loadCount++;
                callback.onFailure();

            }
        });
    }


    public static void loadCategoryWiseQuestionsList(Context context) {
        loadCategoryWiseQuestionsList(context, new DataLoaderCallback() {
            @Override
            public void onSuccess() {
                Log.i(TAG, "onSuccess: " + "loadCategoryWiseQuestionsList");

            }

            @Override
            public void onFailure() {
                Log.i(TAG, "onFailure: " + "loadCategoryWiseQuestionsList");

            }
        });
    }

    public static void loadCategoryWiseQuestionsList(final Context context, final DataLoaderCallback callback) {
        loadInit++;
        GetService getService = RetrofitClient.getInstance().create(GetService.class);
        Call<CategoryQuestionData> call = getService.getCategoryWiseQuestionsList();
        call.enqueue(new Callback<CategoryQuestionData>() {
            @Override
            public void onResponse(Call<CategoryQuestionData> call, Response<CategoryQuestionData> response) {

                if (response.isSuccessful() && response.body().getSuccess() == isSuccess) {

                    QuestionRepository.insert(context, response.body());
                }

                loadCount++;
                callback.onSuccess();
                SyncRepository.insert(context, DB.Question.TABLE);
            }

            @Override
            public void onFailure(Call<CategoryQuestionData> call, Throwable t) {

                loadCount++;
                callback.onFailure();

            }
        });
    }


}
