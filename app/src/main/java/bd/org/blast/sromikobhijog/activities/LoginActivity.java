package bd.org.blast.sromikobhijog.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;

/**
 * Created by This pc on 8/6/2017.
 */


import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bd.org.blast.sromikobhijog.AppConfig;
import bd.org.blast.sromikobhijog.AppController;
import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.model.Calllog;
import bd.org.blast.sromikobhijog.model.SmsLog;
import bd.org.blast.sromikobhijog.repository.CallLogRepository;
import bd.org.blast.sromikobhijog.repository.SMSLogRepository;
import bd.org.blast.sromikobhijog.repository.SyncRepository;
import bd.org.blast.sromikobhijog.sync.SyncAdapter;
import bd.org.blast.sromikobhijog.utilis.SessionManager;
import bd.org.blast.sromikobhijog.utilis.Utils;
import bd.org.blast.sromikobhijog.http.AuthService;
import bd.org.blast.sromikobhijog.http.RetrofitClient;
import bd.org.blast.sromikobhijog.response.AuthResponse;
import retrofit2.Call;
import retrofit2.Callback;


public class LoginActivity extends Activity {
    private static final String TAG = RegisterActivity.class.getSimpleName();
    private static final int REQUEST_READ_CALL_LOG = 100;
    public static String LOGIN_REDIRECT = "login_redirect";

    private Button btnLogin;
    private Button btnLinkToRegister;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLinkToRegister = (Button) findViewById(R.id.btnLinkToRegisterScreen);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        // SQLite database handler

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to ProfileActivity activity
            //Intent intent = new Intent(LoginActivity.this, ProfileActivity.class);
            Intent intent = new Intent(LoginActivity.this, ApplicationFormActivity.class);
            startActivity(intent);
            finish();
        }

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                if (!Utils.checkInternetConnection(LoginActivity.this)) {
                    Toast.makeText(LoginActivity.this, "Check Internet Connection", Toast.LENGTH_SHORT).show();
                    return;
                }


                // Check for empty data in the form
                if (!email.isEmpty() && !password.isEmpty()) {
                    // login user
                    //checkLogin(email, password);


                    checkLoginLocal(encode(email), encode(password));
                } else {
                    // Prompt user to enter credentials
                    Toast.makeText(getApplicationContext(),
                            "Please enter the credentials!", Toast.LENGTH_LONG)
                            .show();
                }
            }

        });

        // Link to Register Screen
        btnLinkToRegister.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });



        //alert();
        List<SmsLog> all = SMSLogRepository.getAll(this, null);



        //only for debug
        setAuth();

    }

    private void setAuth() {
        //admin
        inputEmail.setText("blast");
        inputPassword.setText("blast@123");

        //TU leader user_name:bGVhZGVy
        /*inputEmail.setText("leader");
        inputPassword.setText("123456");*/

        //TU leader user_name:bGVhZGVy
        /*inputEmail.setText("lawyer");
        inputPassword.setText("123456");*/
    }


    private String encode(String value) {
        byte[] data = null;
        try {
            data = value.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (data != null) {
            String valueEncoded = Base64.encodeToString(data, Base64.NO_WRAP);
            Log.e(TAG, "Encoded :" + valueEncoded);

            return valueEncoded;

        }

        return "";
    }

    private void checkLoginLocal(String email, String password) {

        pDialog.setMessage("Logging in ...");
        showDialog();

        AuthService authService = RetrofitClient.getInstance().create(AuthService.class);

        Call<AuthResponse> call = authService.signin(email, password);
        call.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, retrofit2.Response<AuthResponse> response) {
                Log.e(TAG, "onResponse: " + response.body().getApiKey());
                if (response.isSuccessful() && response.body().getSuccess() == 1) {

                    Utils.savePreference(LoginActivity.this, response.body());

                    session.setLogin(true);

                    if (response.body().getUserInfo().getRole().equalsIgnoreCase(Constant.USER_ADMIN)
                            && hasPermission(Manifest.permission.READ_SMS)
                            && hasPermission(Manifest.permission.SEND_SMS)
                            && hasPermission(Manifest.permission.RECEIVE_SMS)
                            && hasPermission(Manifest.permission.READ_CALL_LOG)) {

                        redirect();

                    } else {

                        alert();
                    }


                } else {
                    Toast.makeText(getApplicationContext(), "Invalid Server Response.", Toast.LENGTH_LONG);
                }

                hideDialog();
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {

                t.printStackTrace();

                Toast.makeText(getApplicationContext(), "Server is not responding", Toast.LENGTH_LONG);
                hideDialog();

            }


        });


    }

    private void redirect() {


       SyncRepository.clear(getApplicationContext());
        //call sync after login
        SyncAdapter.syncImmediately(getApplicationContext());

        finish();
        if (!TextUtils.isEmpty(getIntent().getStringExtra(LOGIN_REDIRECT))) {


            try {

                String classname = getIntent().getStringExtra(LOGIN_REDIRECT);
                Class<?> clazz = Class.forName(classname);
                Intent i = new Intent(LoginActivity.this, clazz);
                startActivity(i);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }


        }
    }

    /**
     * function to verify login details in mysql db
     */
    private void checkLogin(final String email, final String password) {
        // Tag used to cancel the request
        String tag_string_req = "req_login";

        pDialog.setMessage("Logging in ...");
        showDialog();

        StringRequest strReq = new StringRequest(Method.POST,
                AppConfig.URL_LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Login Response: " + response.toString());
                hideDialog();

                try {
                    JSONObject jObj = new JSONObject(response);
                    boolean error = jObj.getBoolean("error");

                    // Check for error node in json
                    if (!error) {
                        // user successfully logged in
                        // Create login session
                        session.setLogin(true);

                        // Now store the user in SQLite
                        String uid = jObj.getString("uid");

                        JSONObject user = jObj.getJSONObject("user");
                        String name = user.getString("name");
                        String email = user.getString("email");
                        String created_at = user
                                .getString("created_at");

                        // Inserting row in users table

                        // Launch main activity
                        Intent intent = new Intent(LoginActivity.this,
                                ProfileActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
                        // Error in login. Get the error message
                        String errorMsg = jObj.getString("error_msg");
                        Toast.makeText(getApplicationContext(),
                                errorMsg, Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // JSON error
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Json error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Login Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                        error.getMessage(), Toast.LENGTH_LONG).show();
                hideDialog();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                // Posting parameters to login url
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", email);
                params.put("password", password);

                return params;
            }

        };

        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(strReq, tag_string_req);
    }

    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    private void alert() {
        askPermission();
    }


    private void accessCallLog() {
        if (!mayRequestCallLog()) {
            return;
        }
        // This Build is < 6 , you can Access contacts here.
        getCallDetails();
    }

  /*  @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CALL_LOG: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //getCallDetails();

                    redirect();
                } else {
                    Utils.clearPreference(this);
                    //session.setLogin(false);
                    Log.d(TAG, "onRequestPermissionsResult: REQUEST_READ_CALL_LOG permission denied ");
                    finish();

                }
                return;
            }
        }
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                // Initial
                perms.put(Manifest.permission.READ_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.SEND_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.RECEIVE_SMS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_CALL_LOG, PackageManager.PERMISSION_GRANTED);
                // Fill with results
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                // Check for ACCESS_FINE_LOCATION
                if (perms.get(Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.RECEIVE_SMS) == PackageManager.PERMISSION_GRANTED
                        && perms.get(Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {

                    // All Permissions Granted
                    redirect();

                } else {
                    // Permission Denied
                    Toast.makeText(this, "Some Permission is Denied", Toast.LENGTH_SHORT)
                            .show();

                    Utils.clearPreference(this);
                    //session.setLogin(false);
                    Log.d(TAG, "onRequestPermissionsResult: REQUEST_READ_CALL_LOG permission denied ");
                    finish();
                }


            }
            break;


            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        }
    }

    final private int REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124;

    private void askPermission() {
        List<String> permissionsNeeded = new ArrayList<String>();

        final List<String> permissionsList = new ArrayList<String>();
        if (!addPermission(permissionsList, Manifest.permission.READ_SMS))
            permissionsNeeded.add(Manifest.permission.READ_SMS);
        if (!addPermission(permissionsList, Manifest.permission.SEND_SMS))
            permissionsNeeded.add(Manifest.permission.SEND_SMS);
        if (!addPermission(permissionsList, Manifest.permission.RECEIVE_SMS))
            permissionsNeeded.add(Manifest.permission.RECEIVE_SMS);
        if (!addPermission(permissionsList, Manifest.permission.READ_CALL_LOG))
            permissionsNeeded.add(Manifest.permission.READ_CALL_LOG);

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                // Need Rationale
                String message = "You need to grant access to " + permissionsNeeded.get(0);
                for (int i = 1; i < permissionsNeeded.size(); i++)
                    message = message + ", " + permissionsNeeded.get(i);
                showMessageOKCancel(message, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(LoginActivity.this, permissionsList.toArray(new String[permissionsList.size()]),
                                REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
                    }
                });
                return;
            }

            ActivityCompat.requestPermissions(this, permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS);
            return;
        }

        redirect();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener onClickListener) {

        new AlertDialog.Builder(this).setMessage(message).setPositiveButton("OK", onClickListener).create().show();

    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            // Check for Rationale Option
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission))
                return false;
        }
        return true;
    }


    private boolean mayRequestCallLog() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(Manifest.permission.READ_CALL_LOG)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CALL_LOG}, REQUEST_READ_CALL_LOG);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CALL_LOG}, REQUEST_READ_CALL_LOG);
        }
        return false;
    }

    private boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        return false;
    }


    private void getCallDetails() {

        List<Calllog> all = CallLogRepository.getAll(this, SyncRepository.getRow(this, CallLogRepository.tableName));
        StringBuffer sb = new StringBuffer();
        for (Calllog calllog : all) {
            sb.append("\n" + calllog);
        }


        /*Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DATE+" desc");


        StringBuffer sb = new StringBuffer();
        int id = managedCursor.getColumnIndex(CallLog.Calls._ID);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        sb.append("Call Details :");
        while (managedCursor.moveToNext()) {
            int log_id = managedCursor.getInt(id);
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            sb.append("\n id: "+log_id+"\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");
        }
        managedCursor.close();*/
        //call.setText(sb);
    }
}


