package bd.org.blast.sromikobhijog.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import bd.org.blast.sromikobhijog.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by This pc on 10/18/2017.
 */

public class InformationDetailsActivity extends AppCompatActivity {


    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.WebView)
    android.webkit.WebView WebView;

    @BindView(R.id.txtQA)
    TextView txtQA;

        private String topicsId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_info_details);
        ButterKnife.bind(this);

        //enable up navigation
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //WebView wv = (WebView) findViewById(R.id.WebView);

        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        String html = getIntent().getStringExtra("topicsDescription");
        String topics = getIntent().getStringExtra("topics");

        topicsId = getIntent().getStringExtra("topicsId");
        title.setText(topics);


        WebView.loadDataWithBaseURL("", html, mimeType, encoding, "");
        WebView.getSettings().setAppCacheEnabled(true);
        WebView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        WebView.getSettings().setAppCachePath(this.getCacheDir().getPath());


    }


    public void onClickQA(View view) {


    }

    @OnClick(R.id.txtQA)
    public void onViewClicked() {

        Intent intent = new Intent(this, InformationQAActivity.class);
        intent.putExtra("topicsId",topicsId);

        startActivity(intent);
    }
}
