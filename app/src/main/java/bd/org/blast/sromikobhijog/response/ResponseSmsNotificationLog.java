package bd.org.blast.sromikobhijog.response;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 12/3/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseSmsNotificationLog {

    @SerializedName("notification_log_id")
    @Expose
    private Integer notificationLogId;
    @SerializedName("apps_notification_log_id")
    @Expose
    private String appsNotificationLogId;
    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getNotificationLogId() {
        return notificationLogId;
    }

    public void setNotificationLogId(Integer notificationLogId) {
        this.notificationLogId = notificationLogId;
    }

    public String getAppsNotificationLogId() {
        return appsNotificationLogId;
    }

    public void setAppsNotificationLogId(String appsNotificationLogId) {
        this.appsNotificationLogId = appsNotificationLogId;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}