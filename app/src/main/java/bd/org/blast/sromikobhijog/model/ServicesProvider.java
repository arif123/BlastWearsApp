
package bd.org.blast.sromikobhijog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServicesProvider {

    @SerializedName("service_provider_id")
    @Expose
    private String serviceProviderId;
    @SerializedName("service_provider_category_id")
    @Expose
    private String serviceProviderCategoryId;
    @SerializedName("service_provider_name_en")
    @Expose
    private String serviceProviderNameEn;
    @SerializedName("service_provider_name_bn")
    @Expose
    private String serviceProviderNameBn;
    @SerializedName("business_description_en")
    @Expose
    private String businessDescriptionEn;
    @SerializedName("business_description_bn")
    @Expose
    private String businessDescriptionBn;
    @SerializedName("service_provider_phone")
    @Expose
    private String serviceProviderPhone;
    @SerializedName("service_provider_email")
    @Expose
    private String serviceProviderEmail;
    @SerializedName("service_provider_website")
    @Expose
    private String serviceProviderWebsite;
    @SerializedName("service_provider_logo")
    @Expose
    private String serviceProviderLogo;
    @SerializedName("publish_status")
    @Expose
    private String publishStatus;
    @SerializedName("is_govt")
    @Expose
    private String isGovt;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(String serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public String getServiceProviderCategoryId() {
        return serviceProviderCategoryId;
    }

    public void setServiceProviderCategoryId(String serviceProviderCategoryId) {
        this.serviceProviderCategoryId = serviceProviderCategoryId;
    }

    public String getServiceProviderNameEn() {
        return serviceProviderNameEn;
    }

    public void setServiceProviderNameEn(String serviceProviderNameEn) {
        this.serviceProviderNameEn = serviceProviderNameEn;
    }

    public String getServiceProviderNameBn() {
        return serviceProviderNameBn;
    }

    public void setServiceProviderNameBn(String serviceProviderNameBn) {
        this.serviceProviderNameBn = serviceProviderNameBn;
    }

    public String getBusinessDescriptionEn() {
        return businessDescriptionEn;
    }

    public void setBusinessDescriptionEn(String businessDescriptionEn) {
        this.businessDescriptionEn = businessDescriptionEn;
    }

    public String getBusinessDescriptionBn() {
        return businessDescriptionBn;
    }

    public void setBusinessDescriptionBn(String businessDescriptionBn) {
        this.businessDescriptionBn = businessDescriptionBn;
    }

    public String getServiceProviderPhone() {
        return serviceProviderPhone;
    }

    public void setServiceProviderPhone(String serviceProviderPhone) {
        this.serviceProviderPhone = serviceProviderPhone;
    }

    public String getServiceProviderEmail() {
        return serviceProviderEmail;
    }

    public void setServiceProviderEmail(String serviceProviderEmail) {
        this.serviceProviderEmail = serviceProviderEmail;
    }

    public String getServiceProviderWebsite() {
        return serviceProviderWebsite;
    }

    public void setServiceProviderWebsite(String serviceProviderWebsite) {
        this.serviceProviderWebsite = serviceProviderWebsite;
    }

    public String getServiceProviderLogo() {
        return serviceProviderLogo;
    }

    public void setServiceProviderLogo(String serviceProviderLogo) {
        this.serviceProviderLogo = serviceProviderLogo;
    }

    public String getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

    public String getIsGovt() {
        return isGovt;
    }

    public void setIsGovt(String isGovt) {
        this.isGovt = isGovt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
