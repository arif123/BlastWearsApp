package bd.org.blast.sromikobhijog.model;

import java.util.List;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/8/2017.
 */

public class TopicsQuestionAnswer extends Topic {

    public List<QuestionAnswer> getQuestionAnswerList() {
        return questionAnswerList;
    }

    public void setQuestionAnswerList(List<QuestionAnswer> questionAnswerList) {
        this.questionAnswerList = questionAnswerList;
    }

    List<QuestionAnswer> questionAnswerList  ;
}
