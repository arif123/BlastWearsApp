package bd.org.blast.sromikobhijog.adapter;
/**
 * Created by This pc on 10/11/2017.
 */


import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.sectionedrecyclerview.SectionedRecyclerViewAdapter;
import com.afollestad.sectionedrecyclerview.SectionedViewHolder;

import java.util.List;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.model.ServiceProviderCategory;


@SuppressLint("DefaultLocale")
public class ExpandableRecyclerViewAdapterSP extends SectionedRecyclerViewAdapter<ExpandableRecyclerViewAdapterSP.MainVH> {

    private final List<ServiceProviderCategory> serviceProviderCategoryList;
    //String title[] =new String[]{"GO/NGOs", "লিগ্যাল সার্ভিস ", "ট্রেড ইউনিয়ন ", "টি.ইউ ফেডারেশন্স"};

    public ExpandableRecyclerViewAdapterSP(List<ServiceProviderCategory> serviceProviderCategoryList) {
        this.serviceProviderCategoryList = serviceProviderCategoryList;
    }

    @Override
    public int getSectionCount() {
        return serviceProviderCategoryList.size();
    }

    @Override
    public int getItemCount(int section) {

        if (serviceProviderCategoryList != null)
            if (serviceProviderCategoryList.get(section) != null)
                if (serviceProviderCategoryList.get(section).getServicesProviderList() != null)
                    return serviceProviderCategoryList.get(section).getServicesProviderList().size();

        return 0;


    }


    @Override
    public void onBindHeaderViewHolder(MainVH holder, int section, boolean expanded) {
        holder.title.setText(serviceProviderCategoryList.get(section).getCategoryNameBn());
        holder.caret.setImageResource(expanded ? R.drawable.ic_collapse : R.drawable.ic_expand);
    }

    @Override
    public void onBindFooterViewHolder(MainVH holder, int section) {
        holder.title.setText(String.format("Section footer %d", section));
    }

    @Override
    public void onBindViewHolder(
            MainVH holder, int section, int relativePosition, int absolutePosition) {
        holder.title.setText(serviceProviderCategoryList.get(section).getServicesProviderList().get(relativePosition).getServiceProviderNameBn());//String.format("S:%d, P:%d, A:%d", section, relativePosition, absolutePosition));
    }

    @Override
    public int getItemViewType(int section, int relativePosition, int absolutePosition) {
        if (section == 1) {
            // VIEW_TYPE_FOOTER is -3, VIEW_TYPE_HEADER is -2, VIEW_TYPE_ITEM is -1.
            // You can return 0 or greater.
            return 0;
        }
        return super.getItemViewType(section, relativePosition, absolutePosition);
    }

    @Override
    public MainVH onCreateViewHolder(ViewGroup parent, int viewType) {
        int layout;
        switch (viewType) {
            case VIEW_TYPE_HEADER:
                layout = R.layout.header_sp;
                break;
            case VIEW_TYPE_ITEM:
                layout = R.layout.list_item_sp;
                break;
            case VIEW_TYPE_FOOTER:
                layout = R.layout.header_sp;
                break;
            default:
                // Our custom item, which is the 0 returned in getItemViewType() above
                layout = R.layout.list_item_sp;
                break;
        }

        View v = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new MainVH(v, this);
    }

    static class MainVH extends SectionedViewHolder implements View.OnClickListener {

        final TextView title;
        final ImageView caret;
        final ExpandableRecyclerViewAdapterSP adapter;
        Toast toast;

        MainVH(View itemView, ExpandableRecyclerViewAdapterSP adapter) {
            super(itemView);
            this.title = itemView.findViewById(R.id.title);
            this.caret = itemView.findViewById(R.id.caret);
            this.adapter = adapter;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (isFooter()) {
                // ignore footer clicks
                return;
            }

            if (isHeader()) {
                adapter.toggleSectionExpanded(getRelativePosition().section());
            } else {
                if (toast != null) {
                    toast.cancel();
                }
                /*toast = Toast.makeText(view.getContext(), getRelativePosition().toString(), Toast.LENGTH_SHORT);
                toast.show();*/
            }
        }
    }


}