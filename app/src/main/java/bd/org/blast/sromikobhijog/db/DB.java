package bd.org.blast.sromikobhijog.db;

import android.provider.BaseColumns;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/24/2017.
 */

public class DB {
    private static final String CREATE_TABLE = "CREATE TABLE";
    private static final String SPACE = " ";
    private static final String TEXT = " TEXT";
    private static final String INT = " INTEGER";

    private static final String PRIMARY_KEY = " PRIMARY KEY";
    private static final String UNIQUE = " UNIQUE";

    private static final String VAL_NULL = "NULL";
    private static final String SEP = ",";
    private static final String BRAKET_START = "(";
    private static final String BRAKET_END = ");";
    private static final String DROP_TABLE_IF_EXISTS = "DROP TABLE IF EXISTS ";


    public static class Category implements BaseColumns {
        public static final String TABLE = "search_category";


        public static String infoSearchCategoryId = "info_search_category_id";
        public static String categoryNameEn = "category_name_en";
        public static String categoryNameBn = "category_name_bn";
        public static String publishStatus = "publish_status";
        public static String updatedAt = "updated_at";

        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + Category.infoSearchCategoryId + INT + PRIMARY_KEY + SEP
                + Category.categoryNameEn + TEXT + SEP
                + Category.categoryNameBn + TEXT + SEP
                + Category.publishStatus + TEXT + SEP
                + Category.updatedAt + TEXT
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class Topic implements BaseColumns {
        public static final String TABLE = "topics";

        public static final String infoSearchCategoryId = "info_search_category_id";
        public static final String categoryNameEn = "category_name_en";
        public static final String categoryNameBn = "category_name_bn";
        public static final String infoSearchTopicsId = "info_search_topics_id";
        public static final String topicsNameEn = "topics_name_en";
        public static final String topicsNameBn = "topics_name_bn";
        public static final String topicsDescriptionEn = "topics_description_en";
        public static final String topicsDescriptionBn = "topics_description_bn";
        public static final String publishStatus = "publish_status";
        public static final String updatedAt = "updated_at";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + infoSearchCategoryId + INT + SEP
                + categoryNameEn + TEXT + SEP
                + categoryNameBn + TEXT + SEP
                + infoSearchTopicsId + INT + PRIMARY_KEY + SEP
                + topicsNameEn + TEXT + SEP
                + topicsNameBn + TEXT + SEP
                + topicsDescriptionEn + TEXT + SEP
                + topicsDescriptionBn + TEXT + SEP
                + publishStatus + TEXT + SEP
                + updatedAt + TEXT
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class QuestionAnswer implements BaseColumns {
        public static final String TABLE = "question_answer";


        public static final String infoSearchTopicsId = "info_search_topics_id";
        public static final String topicsNameEn = "topics_name_en";
        public static final String topicsNameBn = "topics_name_bn";
        public static final String infoSearchQuesAnsId = "info_search_ques_ans_id";
        public static final String questionEn = "question_en";
        public static final String questionBn = "question_bn";
        public static final String ansEn = "ans_en";
        public static final String ansBn = "ans_bn";
        public static final String publishStatus = "publish_status";
        public static final String updatedAt = "updated_at";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + infoSearchQuesAnsId + INT + PRIMARY_KEY + SEP
                + infoSearchTopicsId + INT + SEP
                + questionEn + TEXT + SEP
                + questionBn + TEXT + SEP
                + topicsNameEn + TEXT + SEP
                + topicsNameBn + TEXT + SEP
                + ansEn + TEXT + SEP
                + ansBn + TEXT + SEP
                + publishStatus + TEXT + SEP
                + updatedAt + TEXT
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class Area implements BaseColumns {
        public static final String TABLE = "area";


        public static final String areaId = "area_id";
        public static final String areaName = "area_name";
        public static final String areaLocation = "area_location";
        public static final String areaLatitude = "area_latitude";
        public static final String areaLongitude = "area_longitude";
        public static final String publishStatus = "publish_status";
        public static final String updatedAt = "updated_at";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + areaId + INT + PRIMARY_KEY + SEP
                + areaName + TEXT + SEP
                + areaLocation + TEXT + SEP
                + areaLatitude + TEXT + SEP
                + areaLongitude + TEXT + SEP
                + publishStatus + TEXT + SEP
                + updatedAt + TEXT
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class UserArea implements BaseColumns {
        public static final String TABLE = "userarea";


        public static final String id = "id";
        public static final String username = "username";
        public static final String email = "email";
        public static final String areaId = "area_id";
        public static final String areaName = "area_name";
        public static final String areaLocation = "area_location";
        public static final String areaLatitude = "area_latitude";
        public static final String areaLongitude = "area_longitude";
        public static final String areaDivision = "area_division";
        public static final String areaDistrict = "area_district";
        public static final String areaUpazila = "area_upazila";
        public static final String areaUnion = "area_union";
        public static final String areaDescription = "area_description";
        public static final String publishStatus = "publish_status";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + id + INT + PRIMARY_KEY + SEP
                + username + TEXT + SEP
                + email + TEXT + SEP
                + areaId + INT + SEP
                + areaName + TEXT + SEP
                + areaLocation + TEXT + SEP
                + areaLatitude + TEXT + SEP
                + areaLongitude + TEXT + SEP
                + areaDivision + TEXT + SEP
                + areaDistrict + TEXT + SEP
                + areaUpazila + TEXT + SEP
                + areaUnion + TEXT + SEP
                + areaDescription + TEXT + SEP
                + publishStatus + TEXT
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class SalaryRange implements BaseColumns {
        public static final String TABLE = "salaryrange";


        public static final String salaryRangeId = "salary_range_id";
        public static final String salaryRangeEn = "salary_range_en";
        public static final String salaryRangeBn = "salary_range_bn";
        public static final String publishStatus = "publish_status";
        public static final String updatedAt = "updated_at";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + salaryRangeId + INT + PRIMARY_KEY + SEP
                + salaryRangeEn + TEXT + SEP
                + salaryRangeBn + TEXT + SEP
                + publishStatus + TEXT + SEP
                + updatedAt + TEXT
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class Question implements BaseColumns {
        public static final String TABLE = "question";

        public static final String questionId = "question_id";
        public static final String questionEn = "question_en";
        public static final String questionBn = "question_bn";
        public static final String publishStatus = "publish_status";
        public static final String updatedAt = "updated_at";
        public static final String infoSearchCategoryId = "info_search_category_id";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + questionId + INT + PRIMARY_KEY + SEP
                + infoSearchCategoryId + TEXT + SEP
                + questionEn + TEXT + SEP
                + questionBn + TEXT + SEP
                + publishStatus + TEXT + SEP
                + updatedAt + TEXT
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class Applications implements BaseColumns {
        public static final String TABLE = "applications";

        public static final String userName = "user_name";
        public static final String applicationId = "application_id";
        public static final String questionId = "question_id";
        public static final String questionText = "question_text";
        public static final String applicantName = "applicant_name";
        public static final String designation = "designation";
        public static final String mobileNumber = "mobile_number";
        public static final String alternativeMobile = "alternative_mobile";
        public static final String salaryRange = "salary_range";
        public static final String gender = "gender";
        public static final String age = "age";
        public static final String joiningDate = "joining_date";
        public static final String factoryName = "factory_name";
        public static final String factoryAddress = "factory_address";
        public static final String areaId = "area_id";
        public static final String applicationSubmittedBy = "application_submitted_by";
        public static final String applicationStatus = "application_status";
        public static final String statusSync = "status_sync";
        public static final String appsApplicationId = "apps_application_id";
        public static final String syncDatetime = "sync_datetime";
        public static final String createdBy = "created_by";
        public static final String createdAt = "created_at";
        public static final String updatedBy = "updated_by";
        public static final String updatedAt = "updated_at";
        public static final String sync = "sync";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + appsApplicationId + INT + SEP
                + applicationId + INT + SEP
                + userName + TEXT + SEP
                + questionId + TEXT + SEP
                + questionText + TEXT + SEP
                + applicantName + TEXT + SEP
                + mobileNumber + TEXT + SEP
                + alternativeMobile + TEXT + SEP
                + designation + TEXT + SEP
                + salaryRange + TEXT + SEP
                + gender + TEXT + SEP
                + age + TEXT + SEP
                + joiningDate + TEXT + SEP
                + factoryName + TEXT + SEP
                + factoryAddress + TEXT + SEP
                + areaId + TEXT + SEP
                + applicationSubmittedBy + TEXT + SEP
                + applicationStatus + TEXT + SEP
                + statusSync + TEXT + SEP
                + syncDatetime + TEXT + SEP
                + createdBy + TEXT + SEP
                + createdAt + TEXT + SEP
                + updatedBy + TEXT + SEP
                + updatedAt + TEXT + SEP
                + sync + TEXT + SEP
                + PRIMARY_KEY + "(" + appsApplicationId + SEP + applicationId + ")"
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class AskedQuestion implements BaseColumns {

        public static final String TABLE = "askedquestion";

        public static final String appsAskQuestionId = "apps_ask_question_id";
        public static final String askQuestionId = "ask_question_id";
        public static final String questionId = "question_id";
        public static final String fromMobile = "from_mobile";
        public static final String questionText = "question_text";
        public static final String questionChannel = "question_channel";

        public static final String createdBy = "created_by";
        public static final String createdAt = "created_at";
        public static final String updatedBy = "updated_by";
        public static final String updatedAt = "updated_at";
        public static final String askQuestionStatus = "ask_question_status";
        public static final String syncDatetime = "sync_datetime";
        public static final String sync = "sync";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + appsAskQuestionId + INT + SEP
                + askQuestionId + INT + SEP
                + questionId + TEXT + SEP
                + questionText + TEXT + SEP
                + fromMobile + TEXT + SEP
                + questionChannel + TEXT + SEP
                + createdBy + TEXT + SEP
                + createdAt + TEXT + SEP
                + updatedBy + TEXT + SEP
                + updatedAt + TEXT + SEP
                + askQuestionStatus + TEXT + SEP
                + syncDatetime + TEXT + SEP
                + sync + TEXT + SEP
                + PRIMARY_KEY + "(" + appsAskQuestionId + SEP + askQuestionId + ")"
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class AskedQuestionReply implements BaseColumns {

        public static final String TABLE = "asked_question_reply";


        public static final String responseId = "response_id";
        public static final String appsAskQuesResponseId = "apps_ask_ques_response_id";
        public static final String responseText = "response_text";
        public static final String askQuestionId = "ask_question_id";
        public static final String questionText = "question_text";
        public static final String fromMobile = "from_mobile";
        public static final String questionChannel = "question_channel";
        public static final String askQuesStatus = "ask_ques_status";


        public static final String createdBy = "created_by";
        public static final String createdAt = "created_at";
        public static final String askQuestionStatus = "ask_question_status";
        public static final String syncDate = "sync_date";
        public static final String sync = "sync";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + appsAskQuesResponseId + INT + SEP
                + responseId + INT + SEP
                + responseText + TEXT + SEP

                + askQuestionId + INT + SEP
                + questionText + TEXT + SEP
                + fromMobile + TEXT + SEP
                + questionChannel + TEXT + SEP
                + askQuesStatus + TEXT + SEP

                + createdBy + TEXT + SEP
                + createdAt + TEXT + SEP

                + syncDate + TEXT + SEP
                + sync + TEXT + SEP
                + PRIMARY_KEY + "(" + appsAskQuesResponseId + SEP + responseId + ")"
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class ServiceProviderCategory implements BaseColumns {
        public static final String TABLE = "services_provider_category";


        public static String servicesProviderCategoryId = "services_provider_category_id";
        public static String categoryNameEn = "category_name_en";
        public static String categoryNameBn = "category_name_bn";
        public static String publishStatus = "publish_status";
        public static String updatedAt = "updated_at";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + servicesProviderCategoryId + INT + PRIMARY_KEY + SEP
                + categoryNameEn + TEXT + SEP
                + categoryNameBn + TEXT + SEP
                + publishStatus + TEXT + SEP
                + updatedAt + TEXT
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class ServiceProvider implements BaseColumns {
        public static final String TABLE = "service_provider";


        public static final String serviceProviderId = "service_provider_id";
        public static final String serviceProviderCategoryId = "service_provider_category_id";
        public static final String serviceProviderNameEn = "service_provider_name_en";
        public static final String serviceProviderNameBn = "service_provider_name_bn";
        public static final String businessDescriptionEn = "business_description_en";
        public static final String businessDescriptionBn = "business_description_bn";
        public static final String serviceProviderPhone = "service_provider_phone";
        public static final String serviceProviderEmail = "service_provider_email";
        public static final String serviceProviderWebsite = "service_provider_website";
        public static final String serviceProviderLogo = "service_provider_logo";
        public static final String publishStatus = "publish_status";
        public static final String isGovt = "is_govt";
        public static final String createdBy = "created_by";
        public static final String createdAt = "created_at";
        public static final String updatedBy = "updated_by";
        public static final String updatedAt = "updated_at";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + serviceProviderId + INT + PRIMARY_KEY + SEP
                + serviceProviderCategoryId + TEXT + SEP
                + serviceProviderNameEn + TEXT + SEP
                + serviceProviderNameBn + TEXT + SEP
                + businessDescriptionEn + TEXT + SEP
                + businessDescriptionBn + TEXT + SEP
                + serviceProviderPhone + TEXT + SEP
                + serviceProviderEmail + TEXT + SEP
                + serviceProviderWebsite + TEXT + SEP
                + serviceProviderLogo + TEXT + SEP
                + isGovt + TEXT + SEP
                + createdBy + TEXT + SEP
                + createdAt + TEXT + SEP
                + updatedBy + TEXT + SEP
                + publishStatus + TEXT + SEP
                + updatedAt + TEXT
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


    public static class SmsNotificationLog implements BaseColumns {

        public static final String TABLE = "SmsNotificationLog";

        public static final String appsNotificationLogId = "apps_notification_log_id";
        public static final String logId = "log_id";
        public static final String smsFrom = "sms_from";
        public static final String smsTo = "sms_to";
        public static final String smsText = "sms_text";
        public static final String notificationFor = "notification_for";

        public static final String applicationOrQuestionId = "application_or_question_id";

        public static final String createdBy = "created_by";
        public static final String createdAt = "created_at";
        public static final String sync = "sync";



        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + appsNotificationLogId + INT + SEP
                + logId + INT + SEP
                + smsFrom + TEXT + SEP
                + smsTo + INT + SEP
                + smsText + TEXT + SEP
                + notificationFor + TEXT + SEP
                + applicationOrQuestionId + TEXT + SEP
                + createdBy + TEXT + SEP
                + createdAt + TEXT + SEP
                + sync + TEXT + SEP
                + PRIMARY_KEY + "(" + notificationFor + SEP + applicationOrQuestionId + ")"
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;



    }


    public static class Sync implements BaseColumns {
        public static final String TABLE = "sync";


        public static final String tableName = "table_name";
        public static final String updated_at = "updated_at";


        public static final String SQL_CREATE = CREATE_TABLE + SPACE + TABLE + SPACE + BRAKET_START
                + tableName + TEXT + PRIMARY_KEY + SEP
                + updated_at + TEXT
                + BRAKET_END;

        public static final String SQL_DROP = DROP_TABLE_IF_EXISTS + TABLE;
    }


}
