package bd.org.blast.sromikobhijog.activities;

import android.Manifest;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.adapter.MessageperUserAdapter;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.model.AskedQuestion;
import bd.org.blast.sromikobhijog.model.AskedQuestionReply;
import bd.org.blast.sromikobhijog.receiver.DeliveredStatusReceiver;
import bd.org.blast.sromikobhijog.receiver.SMSReceiver;
import bd.org.blast.sromikobhijog.receiver.SentStatusReceiver;
import bd.org.blast.sromikobhijog.repository.AskedQuestionReplyRepository;
import bd.org.blast.sromikobhijog.utilis.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.Manifest.permission.SEND_SMS;

/**
 * Created by This pc on 8/6/2017.
 */

public class MessageActivity extends AppCompatActivity {

    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.etMsg)
    EditText etMsg;
    @BindView(R.id.imageViewSend)
    ImageView imageViewSend;
    @BindView(R.id.typ_msg_area)
    LinearLayout typMsgArea;

    List<AskedQuestion> conversation;

    private BroadcastReceiver sentStatusReceiver, deliveredStatusReceiver;

    private SMSReceiver smsReceiver = new SMSReceiver();
    private static final int REQUEST_SMS = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);


       conversation = new ArrayList<>();

        AskedQuestion askedQuestion  = (AskedQuestion) getIntent().getSerializableExtra("AskedQuestion");
       /* String askQuestionId = intent.getStringExtra(DB.AskedQuestion.askQuestionId);
        String questionText = intent.getStringExtra(DB.AskedQuestion.questionText);
        String questionChannel = intent.getStringExtra(DB.AskedQuestion.questionChannel);

        AskedQuestion askedQuestion = new AskedQuestion();
        askedQuestion.setAskQuestionId(askQuestionId);
        askedQuestion.setQuestionText(questionText);
        askedQuestion.setQuestionChannel(questionChannel);*/
        conversation.add(askedQuestion);



        if (askedQuestion.getAskQuestionId() != null){

            List<AskedQuestionReply> replyList = AskedQuestionReplyRepository.getRow(this, askedQuestion.getAskQuestionId());
            if (replyList.isEmpty()) {

                typMsgArea.setVisibility(View.VISIBLE);

            }else{
                typMsgArea.setVisibility(View.GONE);
                AskedQuestionReply aqr = replyList.get(0);
                AskedQuestion reply = new AskedQuestion();
                reply.setAskQuestionId(aqr.getAskQuestionId());
                reply.setQuestionText(aqr.getResponseText());
                reply.setQuestionChannel(aqr.getQuestionChannel());
                conversation.add(reply);

            }
        }else {
            Toast.makeText(this, "Sync first. Then reply to this question.", Toast.LENGTH_SHORT).show();
            typMsgArea.setVisibility(View.GONE);
        }



        final MessageperUserAdapter messageAdapter = new MessageperUserAdapter(this, conversation);
        list.setAdapter(messageAdapter);
    }

    @OnClick(R.id.imageViewSend)
    public void onViewClicked() {
        if (TextUtils.isEmpty(etMsg.getText()))return;

        /*Intent intent = getIntent();
        String askQuestionId = intent.getStringExtra(DB.AskedQuestion.askQuestionId);
        String questionText = intent.getStringExtra(DB.AskedQuestion.questionText);
        String questionChannel = intent.getStringExtra(DB.AskedQuestion.questionChannel);*/

        AskedQuestion askedQuestion  = (AskedQuestion) getIntent().getSerializableExtra("AskedQuestion");

        AskedQuestionReply aqr  = new AskedQuestionReply();
        aqr.setResponseText(etMsg.getText().toString());
        aqr.setAskQuestionId(askedQuestion.getAskQuestionId());
        aqr.setQuestionText(askedQuestion.getQuestionText());
        aqr.setQuestionChannel(askedQuestion.getQuestionChannel());
        aqr.setFromMobile(askedQuestion.getFromMobile());
        aqr.setCreatedAt(Utils.getCurrentDateTime());

        AskedQuestionReplyRepository.insert(this, aqr);

        // TODO: 11/30/2017  if Channel is sms
        if (askedQuestion.getQuestionChannel().equals(Constant.CHANNEL_SMS)){
            sendSMSButtonClick(aqr.getFromMobile(), aqr.getResponseText());
        }


        AskedQuestion reply = new AskedQuestion();
        reply.setAskQuestionId(aqr.getAskQuestionId());
        reply.setQuestionText(aqr.getResponseText());
        reply.setQuestionChannel(aqr.getQuestionChannel());
        conversation.add(reply);

        ListAdapter adapter = list.getAdapter();
        final ArrayAdapter arrayAdapter = ((ArrayAdapter)adapter);
        runOnUiThread(new Runnable() {
            public void run() {
                arrayAdapter.notifyDataSetChanged();
            }
        });

        etMsg.setText("");
        typMsgArea.setVisibility(View.GONE);



    }



    public void sendSMSButtonClick(String phone ,
                                   String message){


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int hasSMSPermission = checkSelfPermission(Manifest.permission.SEND_SMS);
            if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                if (!shouldShowRequestPermissionRationale(Manifest.permission.SEND_SMS)) {
                    showMessageOKCancel("You need to allow access to Send SMS",
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[] {Manifest.permission.SEND_SMS},
                                                REQUEST_SMS);
                                    }
                                }
                            });
                    return;
                }
                requestPermissions(new String[] {Manifest.permission.SEND_SMS},
                        REQUEST_SMS);
                return;
            }
            sendMySMS(phone, message);
        }
    }


    public void sendMySMS(String phone ,
                          String message) {



        //Check if the phoneNumber is empty
        if (phone.isEmpty()) {

            Toast.makeText(getApplicationContext(), "Contact Number Not Found", Toast.LENGTH_SHORT).show();

        } else {

            SmsManager sms = SmsManager.getDefault();
            // if message length is too long messages are divided
            List<String> messages = sms.divideMessage(message);
            for (String msg : messages) {

                PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
                PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
                sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);

            }
        }
    }


    public void onResume() {
        super.onResume();
        sentStatusReceiver= new SentStatusReceiver();
        deliveredStatusReceiver= new DeliveredStatusReceiver();


        registerReceiver(sentStatusReceiver, new IntentFilter("SMS_SENT"));
        registerReceiver(deliveredStatusReceiver, new IntentFilter("SMS_DELIVERED"));

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            registerReceiver(smsReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
    }
    public void onPause() {
        super.onPause();

        unregisterReceiver(sentStatusReceiver);
        unregisterReceiver(deliveredStatusReceiver);

        if (Build.VERSION.SDK_INT>=Build.VERSION_CODES.O)
            unregisterReceiver(smsReceiver);
    }


    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_SMS:
                if (grantResults.length > 0 &&  grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(getApplicationContext(), "Permission Granted, Now you can send sms. Try now", Toast.LENGTH_LONG).show();
                    //sendMySMS();

                }else {

                    Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and sms", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(SEND_SMS)) {
                            showMessageOKCancel("You need to allow access to both the permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{SEND_SMS},
                                                        REQUEST_SMS);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}
