
package bd.org.blast.sromikobhijog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Topic {

    @SerializedName("info_search_category_id")
    @Expose
    private String infoSearchCategoryId;
    @SerializedName("category_name_en")
    @Expose
    private String categoryNameEn;
    @SerializedName("category_name_bn")
    @Expose
    private String categoryNameBn;
    @SerializedName("info_search_topics_id")
    @Expose
    private String infoSearchTopicsId;
    @SerializedName("topics_name_en")
    @Expose
    private String topicsNameEn;
    @SerializedName("topics_name_bn")
    @Expose
    private String topicsNameBn;
    @SerializedName("topics_description_en")
    @Expose
    private String topicsDescriptionEn;
    @SerializedName("topics_description_bn")
    @Expose
    private String topicsDescriptionBn;
    @SerializedName("publish_status")
    @Expose
    private String publishStatus;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getInfoSearchCategoryId() {
        return infoSearchCategoryId;
    }

    public void setInfoSearchCategoryId(String infoSearchCategoryId) {
        this.infoSearchCategoryId = infoSearchCategoryId;
    }

    public String getCategoryNameEn() {
        return categoryNameEn;
    }

    public void setCategoryNameEn(String categoryNameEn) {
        this.categoryNameEn = categoryNameEn;
    }

    public String getCategoryNameBn() {
        return categoryNameBn;
    }

    public void setCategoryNameBn(String categoryNameBn) {
        this.categoryNameBn = categoryNameBn;
    }

    public String getInfoSearchTopicsId() {
        return infoSearchTopicsId;
    }

    public void setInfoSearchTopicsId(String infoSearchTopicsId) {
        this.infoSearchTopicsId = infoSearchTopicsId;
    }

    public String getTopicsNameEn() {
        return topicsNameEn;
    }

    public void setTopicsNameEn(String topicsNameEn) {
        this.topicsNameEn = topicsNameEn;
    }

    public String getTopicsNameBn() {
        return topicsNameBn;
    }

    public void setTopicsNameBn(String topicsNameBn) {
        this.topicsNameBn = topicsNameBn;
    }

    public String getTopicsDescriptionEn() {
        return topicsDescriptionEn;
    }

    public void setTopicsDescriptionEn(String topicsDescriptionEn) {
        this.topicsDescriptionEn = topicsDescriptionEn;
    }

    public String getTopicsDescriptionBn() {
        return topicsDescriptionBn;
    }

    public void setTopicsDescriptionBn(String topicsDescriptionBn) {
        this.topicsDescriptionBn = topicsDescriptionBn;
    }

    public String getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
