
package bd.org.blast.sromikobhijog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAreaData {

    @SerializedName("user_area_list")
    @Expose
    private List<UserArea> userAreaList = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<UserArea> getUserAreaList() {
        return userAreaList;
    }

    public void setUserAreaList(List<UserArea> userAreaList) {
        this.userAreaList = userAreaList;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
