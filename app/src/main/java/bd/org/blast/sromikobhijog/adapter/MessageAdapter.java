package bd.org.blast.sromikobhijog.adapter;

/**
 * Created by This pc on 8/7/2017.
 */


import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.model.Application;
import bd.org.blast.sromikobhijog.model.AskedQuestion;
import bd.org.blast.sromikobhijog.model.Message;

// ServiceProviderAdapter

public class MessageAdapter extends ArrayAdapter<AskedQuestion> {
    // View lookup cache
    private static class ViewHolder {
        TextView tvTitle;
        TextView tvDescription;
        TextView tvDate;


    }


    private   List<AskedQuestion> allitems;
    private   List<AskedQuestion> data;

    public MessageAdapter(Context context, List<AskedQuestion> messageArrayList) {
        super(context, R.layout.list_item_msg, messageArrayList);

        allitems = new ArrayList<>(messageArrayList);
        data = new ArrayList<>(messageArrayList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        AskedQuestion message = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_msg, parent, false);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            viewHolder.tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
            viewHolder.tvDate = (TextView) convertView.findViewById(R.id.tvDate);

            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.tvTitle.setText(message.getFromMobile());
        viewHolder.tvDescription.setText(message.getQuestionText());
        viewHolder.tvDate.setText(message.getCreatedAt());






        // Return the completed view to render on screen
        return convertView;
    }



    private Filter filter;
    @NonNull
    @Override
    public Filter getFilter() {
        if (null == filter){
            filter = new ItemFilter();
        }
        return filter;
    }




    private class ItemFilter extends Filter
    {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            constraint = constraint.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if(constraint != null && constraint.toString().length() > 0)
            {
                ArrayList<AskedQuestion> filteredItems = new ArrayList<>();

                for(int i = 0, l = allitems.size(); i < l; i++)
                {
                    AskedQuestion nameList = allitems.get(i);
                    if(nameList.toString().toLowerCase().contains(constraint))
                        filteredItems.add(nameList);
                }
                result.count = filteredItems.size();
                result.values = filteredItems;
            }
            else
            {
                synchronized(this)
                {
                    result.values = allitems;
                    result.count = allitems.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {

            data = (ArrayList<AskedQuestion>)results.values;
            notifyDataSetChanged();
            clear();
            for(int i = 0, l = data.size(); i < l; i++)
                add(data.get(i));
            notifyDataSetInvalidated();
        }
    }
}
