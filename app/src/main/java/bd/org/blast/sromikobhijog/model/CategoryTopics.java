package bd.org.blast.sromikobhijog.model;

import java.util.List;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/8/2017.
 */

public class CategoryTopics extends Category {

    List<Topic> topicList;

    public List<Topic> getTopicList() {
        return topicList;
    }

    public void setTopicList(List<Topic> topicList) {
        this.topicList = topicList;
    }
}
