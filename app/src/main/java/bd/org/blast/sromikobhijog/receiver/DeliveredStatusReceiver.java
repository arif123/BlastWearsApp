package bd.org.blast.sromikobhijog.receiver;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

import bd.org.blast.sromikobhijog.activities.AskQuestionActivity;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/22/2017.
 */

public class DeliveredStatusReceiver extends BroadcastReceiver
{


    @Override
    public void onReceive(Context context, Intent intent) {
        String s = "Message Not Delivered";
        switch(getResultCode()) {
            case Activity.RESULT_OK:
                s = "Message Delivered Successfully";
                break;
            case Activity.RESULT_CANCELED:
                break;
        }

        //deliveryStatus
        Toast.makeText(context, s, Toast.LENGTH_SHORT).show();

    }
}
