package bd.org.blast.sromikobhijog.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.model.Category;
import bd.org.blast.sromikobhijog.model.CategoryData;
import bd.org.blast.sromikobhijog.model.Topic;
import bd.org.blast.sromikobhijog.model.TopicData;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/31/2017.
 */

public class TopicRepository {

    private static final String TAG="TopicRepository";


    public static void insert(Context context, TopicData list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(Topic item: list.getTopicList()){
            insert(context, item);
        }
        Log.i(TAG, "insert: "+list.getTopicList().size() );
    }


    public static void insert(Context context, Topic item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);


        ContentValues cv = new ContentValues();

        cv.put(DB.Topic.infoSearchCategoryId , item.getInfoSearchCategoryId());
        cv.put(DB.Topic.categoryNameEn  , item.getCategoryNameEn());
        cv.put(DB.Topic.categoryNameBn , item.getCategoryNameBn());
        cv.put(DB.Topic.infoSearchTopicsId , item.getInfoSearchTopicsId());
        cv.put(DB.Topic.topicsNameEn , item.getTopicsNameEn());
        cv.put(DB.Topic.topicsNameBn , item.getTopicsNameBn());
        cv.put(DB.Topic.topicsDescriptionEn , item.getTopicsDescriptionEn());
        cv.put(DB.Topic.topicsDescriptionBn , item.getTopicsDescriptionBn());
        cv.put(DB.Topic.publishStatus  , item.getPublishStatus());
        cv.put(DB.Topic.updatedAt , item.getUpdatedAt());

        cv.put(DB.Topic.infoSearchCategoryId, item.getInfoSearchCategoryId());
        cv.put(DB.Topic.categoryNameEn, item.getCategoryNameEn());
        cv.put(DB.Topic.categoryNameBn, item.getCategoryNameBn());
        cv.put(DB.Topic.publishStatus, item.getPublishStatus());
        cv.put(DB.Topic.updatedAt, item.getUpdatedAt());

        db.insert(DB.Topic.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert: "+1 );

    }
}
