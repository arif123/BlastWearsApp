package bd.org.blast.sromikobhijog.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.model.Application;
import bd.org.blast.sromikobhijog.model.AskedQuestion;
import bd.org.blast.sromikobhijog.repository.ApplicationRepository;
import bd.org.blast.sromikobhijog.repository.AskedQuestionRepository;
import bd.org.blast.sromikobhijog.response.UserInfo;
import bd.org.blast.sromikobhijog.sync.SyncAdapter;
import bd.org.blast.sromikobhijog.utilis.SessionManager;
import bd.org.blast.sromikobhijog.utilis.Utils;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    final String TAG = MainActivity.class.getSimpleName();


    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    LinearLayout llHelpLine, llAskQ, llApplication, llInfo, llServiceProvider;
    NavigationView navigationView;

    TextView textViewApplication, textViewAskQ;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        llHelpLine = (LinearLayout) findViewById(R.id.llHelpLine);
        llAskQ = (LinearLayout) findViewById(R.id.llAskQ);
        llApplication = (LinearLayout) findViewById(R.id.llApplication);
        llInfo = (LinearLayout) findViewById(R.id.lInfo);
        llServiceProvider = (LinearLayout) findViewById(R.id.llServiceProvider);

        llHelpLine.setOnClickListener(this);
        llAskQ.setOnClickListener(this);
        llApplication.setOnClickListener(this);
        llInfo.setOnClickListener(this);
        llServiceProvider.setOnClickListener(this);

         textViewAskQ = (TextView) findViewById(R.id.textViewAskQ);
         textViewApplication = (TextView) findViewById(R.id.textViewApplication);






        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);






    }

    private void setRole(){
        try {
            String userRole=Utils.getinfo(this).getUserInfo().getRole();
            if (userRole.equalsIgnoreCase(Constant.USER_ADMIN))
                loadForAdmin();
            else if (userRole.equalsIgnoreCase(Constant.USER_LAWYER))
                loadForLawyer();
            else if (userRole.equalsIgnoreCase(Constant.USER_TU_LEADER))
                loadForTULeader();
            else
                loadForNormal();

        }catch (Exception e){
            e.printStackTrace();
            loadForNormal();
        }
    }

    private void loadForNormal() {
      //  Toast.makeText(this, "normal user", Toast.LENGTH_SHORT).show();
        Log.i(TAG, "loadForNormal: ");

        textViewAskQ.setText(getString(R.string.ask_question));
        textViewApplication.setText(getString(R.string.submit_application));

    }

    private void loadForTULeader() {
        Log.i(TAG, "loadForTULeader: ");
        //Toast.makeText(this, "tu leader", Toast.LENGTH_SHORT).show();
    }

    private void loadForLawyer() {
        Log.i(TAG, "loadForLawyer: ");
        //Toast.makeText(this, "lawyer", Toast.LENGTH_SHORT).show();

        textViewAskQ.setText(getString(R.string.ask_question));
        List<Application> applicationReceived = ApplicationRepository.getAll(this, DB.Applications.applicationStatus + " like ?", "RECEIVED");
        if (applicationReceived != null ){
            textViewApplication.setText(getString(R.string.submit_application)+ " ("+applicationReceived.size()+")");
        }else {
            textViewApplication.setText(getString(R.string.submit_application));
        }
    }

    private void loadForAdmin() {
        Log.i(TAG, "loadForAdmin: ");
        //Toast.makeText(this, "admin", Toast.LENGTH_SHORT).show();

        List<AskedQuestion> received = AskedQuestionRepository.getAll(this, DB.AskedQuestion.askQuestionStatus + " like ?", "RECEIVED");
        if (received!= null ){
            textViewAskQ.setText(getString(R.string.ask_question)+"("+received.size()+")");
        }else{
            textViewAskQ.setText(getString(R.string.ask_question));
        }

        List<Application> applicationReceived = ApplicationRepository.getAll(this, DB.Applications.applicationStatus + " like ?", "RECEIVED");

        if (applicationReceived != null ){
            textViewApplication.setText(getString(R.string.submit_application)+ " ("+applicationReceived.size()+")");
        }else {
            textViewApplication.setText(getString(R.string.submit_application));
        }



    }


    public void setNavigationViewHeaderData(NavigationView navigationView, UserInfo userInfo) {






        View navHeader = navigationView.getHeaderView(0);
        ImageView imgProfile = (ImageView) navHeader.findViewById(R.id.imageView);
        // Loading profile image
       /* Glide.with(this).load("")
                .crossFade()
                .centerCrop()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTransform(this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgProfile);*/


        TextView txtEmail = (TextView) navHeader.findViewById(R.id.txtEmail);
        TextView txtName = (TextView) navHeader.findViewById(R.id.txtName);
        TextView txtRole = (TextView) navHeader.findViewById(R.id.txtRole);


        txtEmail.setText(userInfo == null ? "" : userInfo.getEmail());
        txtName.setText(userInfo == null ? "" : userInfo.getName());
        txtRole.setText(userInfo == null ? "" : userInfo.getRole());

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       // getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        /*int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;

        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_sync) {
            //loading all data
            //DataLoader.loadAll(this);
            SyncAdapter.syncImmediately(this);
        } else if (id == R.id.nav_helpline) {
            callHelpline();
            //ApplicationRepository.getUnsynced(this);

        } else if (id == R.id.nav_ask) {
            startActivity(new Intent(MainActivity.this, AskQuestionActivity.class));


        } else if (id == R.id.nav_application) {
            callApplicationForm();


        } else if (id == R.id.nav_info) {
            startActivity(new Intent(MainActivity.this, InformationActivity.class));
            //startActivity(new Intent(MainActivity.this, CallLogAct.class));




        } else if (id == R.id.nav_service) {
            startActivity(new Intent(MainActivity.this, ServiceProviderActivity.class));


        } else if (id == R.id.nav_logout) {

            boolean loggedIn = new SessionManager(this).isLoggedIn();

            if (loggedIn) {


                new android.support.v7.app.AlertDialog.Builder(this)
                        .setTitle("Logout")
                        .setMessage("Are you sure?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                //
                                Utils.clearPreference(MainActivity.this);
                                new SessionManager(MainActivity.this).setLogin(false);

                                navigationView.getMenu().clear();
                                navigationView.inflateMenu(R.menu.activity_main_drawer);

                                setNavigationViewHeaderData(navigationView, null);

                                Toast.makeText(MainActivity.this, "Logout Successful", Toast.LENGTH_SHORT).show();

                                // setting view based on user role
                                setRole();


                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create()
                        .show();



            } else {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }


        }else if (id == R.id.nav_change_mobileNo ){
             Utils.changeNumber(this);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view == llHelpLine) {
            callHelpline();

        } else if (view == llAskQ) {

            startActivity(new Intent(MainActivity.this, AskQuestionActivity.class));

        } else if (view == llApplication) {

            callApplicationForm();

        } else if (view == llInfo) {
            startActivity(new Intent(MainActivity.this, InformationActivity.class));

        } else if (view == llServiceProvider) {
            startActivity(new Intent(MainActivity.this, ServiceProviderActivity.class));

        }
    }

    private void callApplicationForm() {

        boolean loggedIn = new SessionManager(this).isLoggedIn();
        if (loggedIn) {

            startActivity(new Intent(MainActivity.this, ApplicationFormActivity.class));

        } else {
            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.putExtra(LoginActivity.LOGIN_REDIRECT, "bd.org.blast.sromikobhijog.activities.ApplicationFormActivity");
            startActivity(intent);

        }
    }

    private void callHelpline() {
        //call help line
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + getString(R.string.blast_helpline_no)));
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //
        boolean loggedIn = new SessionManager(this).isLoggedIn();
        if (navigationView!=null){
            if (loggedIn ){

                navigationView.getMenu().clear();
                navigationView.inflateMenu(R.menu.activity_main_drawer_loggedin);

                UserInfo userInfo = Utils.getinfo(this).getUserInfo();
                setNavigationViewHeaderData(navigationView, userInfo);

            }else{

                navigationView.getMenu().clear();
                navigationView.inflateMenu(R.menu.activity_main_drawer);
                setNavigationViewHeaderData(navigationView, null);
            }
        }

        // setting view based on user role
        setRole();


    }



}
