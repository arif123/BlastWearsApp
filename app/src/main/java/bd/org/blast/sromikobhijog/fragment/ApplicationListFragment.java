package bd.org.blast.sromikobhijog.fragment;

/**
 * Created by This pc on 8/10/2017.
 */


import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.adapter.ApplicationAdapter;
import bd.org.blast.sromikobhijog.model.Application;
import bd.org.blast.sromikobhijog.model.DataModel;
import bd.org.blast.sromikobhijog.repository.ApplicationRepository;
import bd.org.blast.sromikobhijog.utilis.Utils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import jp.co.recruit_lifestyle.android.widget.WaveSwipeRefreshLayout;

/**
 * A ApplicationListFragment fragment containing a simple view.
 */
public class ApplicationListFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    int type;
    @BindView(R.id.etSearch)
    EditText etSearch;
    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.main_swipe)
    WaveSwipeRefreshLayout mainSwipe;
    Unbinder unbinder;


    ApplicationAdapter applicationAdapter;

    public static ApplicationListFragment newInstance(int sectionNumber) {
        ApplicationListFragment fragment = new ApplicationListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_info_and_service_tab, container, false);
        unbinder = ButterKnife.bind(this, rootView);





        populateList();





        mainSwipe.setColorSchemeColors(Color.WHITE, Color.WHITE);
        mainSwipe.setWaveColor(Color.argb(255, 203, 32, 38));
        mainSwipe.setOnRefreshListener(new WaveSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        populateList();
                    }
                }, 2000);
            }
        });







        return rootView;
    }

    private void populateList(){

        final List<Application> applications = ApplicationRepository.getAll(getContext(), null, new String[0]);



        final List<DataModel> dataModels = new ArrayList<>();
        dataModels.add(new DataModel().setId("1").setValueEN(ApplicationRepository.ACCEPTED).setValueBN(ApplicationRepository.ACCEPTED));
        dataModels.add(new DataModel().setId("2").setValueEN(ApplicationRepository.DISPOSAL).setValueBN(ApplicationRepository.DISPOSAL));
        dataModels.add(new DataModel().setId("3").setValueEN(ApplicationRepository.ON_PROGRESS).setValueBN(ApplicationRepository.ON_PROGRESS));
        dataModels.add(new DataModel().setId("4").setValueEN(ApplicationRepository.PANDING_FOR_DOC).setValueBN(ApplicationRepository.PANDING_FOR_DOC));
        dataModels.add(new DataModel().setId("5").setValueEN(ApplicationRepository.RECEIVED).setValueBN(ApplicationRepository.RECEIVED));
        dataModels.add(new DataModel().setId("6").setValueEN(ApplicationRepository.REJECTED).setValueBN(ApplicationRepository.REJECTED));
        dataModels.add(new DataModel().setId("7").setValueEN(ApplicationRepository.SETTLED).setValueBN(ApplicationRepository.SETTLED));


        applicationAdapter = new ApplicationAdapter(getContext(), applications);
        list.setAdapter(applicationAdapter);

        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                applicationAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });



        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {


                try {
                    String role = Utils.getinfo(getContext()).getUserInfo().getRole();
                    if (role.equalsIgnoreCase(Constant.USER_ADMIN ) || role.equalsIgnoreCase(Constant.USER_LAWYER ) )
                        popupSingle(getContext(), dataModels, applications.get(position));
                    else
                        Toast.makeText(getContext(), applications.get(position).getApplicationStatus(), Toast.LENGTH_SHORT).show();

                }catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(getContext(), applications.get(position).getApplicationStatus(), Toast.LENGTH_SHORT).show();
                }
            }
        });

        mainSwipe.setRefreshing(false);
    }


    public void popupSingle(final Context ctx, List<DataModel> allLabels, final Application selectedItemValue) {


        String values[] = new String[allLabels.size()];
        for (int i = 0; i < values.length; i++) values[i] = allLabels.get(i).toString();


        final AlertDialog.Builder builderSingle = new AlertDialog.Builder(ctx);
        builderSingle.setIcon(R.mipmap.ic_application);
        builderSingle.setTitle(R.string.select_any);


        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                ctx,
                android.R.layout.select_dialog_singlechoice);
        arrayAdapter.addAll(values);

        builderSingle.setNegativeButton(
                R.string.cancel,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


        if (selectedItemValue != null && TextUtils.isEmpty(selectedItemValue.getApplicationStatus())) {
            builderSingle.setAdapter(
                    arrayAdapter,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            String strName = arrayAdapter.getItem(which);
                            if (!selectedItemValue.getApplicationStatus().equals(strName)) {
                                //((ApplicationAdapter)listView.getAdapter()).getItem(1).setApplicationStatus(strName);
                                selectedItemValue.setApplicationStatus(strName);
                                ((ApplicationAdapter) list.getAdapter()).notifyDataSetChanged();
                                selectedItemValue.setStatusSync("n");
                                ApplicationRepository.updateStatus(ctx, selectedItemValue);
                            }
                            dialog.dismiss();

                        }
                    });
        } else {

            int selectedItemIndex = 0;
            for (int i = 0; i < values.length; i++) {
                if (values[i].equals(selectedItemValue.getApplicationStatus())) {
                    selectedItemIndex = i;
                    break;
                }
            }

            builderSingle.setSingleChoiceItems(values, selectedItemIndex, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String strName = arrayAdapter.getItem(which);

                    if (!selectedItemValue.getApplicationStatus().equals(strName)) {
                        //((ApplicationAdapter)listView.getAdapter()).getItem(1).setApplicationStatus(strName);
                        selectedItemValue.setApplicationStatus(strName);
                        ((ApplicationAdapter) list.getAdapter()).notifyDataSetChanged();
                        selectedItemValue.setStatusSync("n");
                        ApplicationRepository.updateStatus(ctx, selectedItemValue);
                    }
                    //selectedItemValue=strName;
                    dialog.dismiss();

                }
            });

        }


        builderSingle.show();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


}