
package bd.org.blast.sromikobhijog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalaryRange {

    @SerializedName("salary_range_id")
    @Expose
    private String salaryRangeId;
    @SerializedName("salary_range_en")
    @Expose
    private String salaryRangeEn;
    @SerializedName("salary_range_bn")
    @Expose
    private String salaryRangeBn;
    @SerializedName("publish_status")
    @Expose
    private String publishStatus;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getSalaryRangeId() {
        return salaryRangeId;
    }

    public void setSalaryRangeId(String salaryRangeId) {
        this.salaryRangeId = salaryRangeId;
    }

    public String getSalaryRangeEn() {
        return salaryRangeEn;
    }

    public void setSalaryRangeEn(String salaryRangeEn) {
        this.salaryRangeEn = salaryRangeEn;
    }

    public String getSalaryRangeBn() {
        return salaryRangeBn;
    }

    public void setSalaryRangeBn(String salaryRangeBn) {
        this.salaryRangeBn = salaryRangeBn;
    }

    public String getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
