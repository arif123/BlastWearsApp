
package bd.org.blast.sromikobhijog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TopicData {

    @SerializedName("topics_list")
    @Expose
    private List<Topic> topicList = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<Topic> getTopicList() {
        return topicList;
    }

    public void setTopicList(List<Topic> topicList) {
        this.topicList = topicList;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
