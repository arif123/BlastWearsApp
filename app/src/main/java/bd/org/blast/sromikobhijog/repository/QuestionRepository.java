package bd.org.blast.sromikobhijog.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.model.Category;
import bd.org.blast.sromikobhijog.model.CategoryQuestionData;
import bd.org.blast.sromikobhijog.model.CategoryQuestionList;
import bd.org.blast.sromikobhijog.model.Question;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/31/2017.
 */

public class QuestionRepository {

    private static final String TAG="QuestionRepository";


    public static void insert(Context context, CategoryQuestionData list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        for (CategoryQuestionList categoryQuestionList : list.getCategoryQuestionList()) {

            int infoSearchCategoryId =categoryQuestionList.getInfoSearchCategoryId();
            CategoryRepository.insert(context, (Category)categoryQuestionList);
            for(Question item: categoryQuestionList.getQuesList()){
                insert(context, item, infoSearchCategoryId);
            }
            Log.i(TAG, "insert: "+list.getCategoryQuestionList().size() );

        }


    }


    public static void insert(Context context, Question item, int infoSearchCategoryId){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);


        ContentValues cv = new ContentValues();

        cv.put(DB.Question.questionId , item.getQuestionId());
        cv.put(DB.Question.questionEn , item.getQuestionEn());
        cv.put(DB.Question.questionBn , item.getQuestionBn());
        cv.put(DB.Question.publishStatus , item.getPublishStatus());
        cv.put(DB.Question.updatedAt , item.getUpdatedAt());
        cv.put(DB.Question.infoSearchCategoryId ,infoSearchCategoryId);





        

        db.insert(DB.Question.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert: "+1 );

    }

    public static List<CategoryQuestionList> getAll(Context context){
        List<CategoryQuestionList> categoryQuestionList  =new ArrayList<>();

        BriteDatabase database = BriteDbHelper.getBriteDatabase(context);
        Cursor cursor = database.query("select s.info_search_category_id, s.category_name_en, s.category_name_bn, q.question_id, q.question_en, q.question_bn  from search_category s, question q " +
                "where s.info_search_category_id= q.info_search_category_id order by s.info_search_category_id", null);


        CategoryQuestionList temp=null;
        if (cursor!= null & cursor.moveToFirst()){
            do {

                int infoSearchCategoryId=cursor.getInt(cursor.getColumnIndex(DB.Category.infoSearchCategoryId));
                if (temp == null || temp.getInfoSearchCategoryId() != infoSearchCategoryId){
                    temp = new CategoryQuestionList();

                    temp.setInfoSearchCategoryId(cursor.getInt(cursor.getColumnIndex(DB.Category.infoSearchCategoryId)));
                    temp.setCategoryNameEn(cursor.getString(cursor.getColumnIndex(DB.Category.categoryNameEn)));
                    temp.setCategoryNameBn(cursor.getString(cursor.getColumnIndex(DB.Category.categoryNameBn)));


                    List<Question> questionsList = new ArrayList<>();
                    Question question = new Question();
                    question.setQuestionId(cursor.getString(cursor.getColumnIndex(DB.Question.questionId)));
                    question.setQuestionEn(cursor.getString(cursor.getColumnIndex(DB.Question.questionEn)));
                    question.setQuestionBn(cursor.getString(cursor.getColumnIndex(DB.Question.questionBn)));
                    questionsList.add(question);

                    temp.setQuesList(questionsList);

                    categoryQuestionList.add(temp);

                }else {
                    List<Question> questionsList = temp.getQuesList();
                    Question question = new Question();
                    question.setQuestionId(cursor.getString(cursor.getColumnIndex(DB.Question.questionId)));
                    question.setQuestionEn(cursor.getString(cursor.getColumnIndex(DB.Question.questionEn)));
                    question.setQuestionBn(cursor.getString(cursor.getColumnIndex(DB.Question.questionBn)));
                    questionsList.add(question);
                }






                //application.setJoiningDate(cursor.getString(cursor.getColumnIndex(DB.Applications.joiningDate)));



            }while (cursor.moveToNext());
            cursor.close();
        }

        return categoryQuestionList;

    }
}
