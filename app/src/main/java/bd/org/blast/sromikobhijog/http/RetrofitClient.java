package bd.org.blast.sromikobhijog.http;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/25/2017.
 */

public class RetrofitClient {

    private static Retrofit retrofit;

    public static Retrofit getInstance(){
        if(retrofit==null) {


            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();

            retrofit = new Retrofit.Builder()
                    .baseUrl(ServerConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }

        return retrofit;
    }
}
