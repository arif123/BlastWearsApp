package bd.org.blast.sromikobhijog.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import bd.org.blast.sromikobhijog.Constant;
import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.fragment.ApplicationFormFragment;
import bd.org.blast.sromikobhijog.fragment.ApplicationListFragment;
import bd.org.blast.sromikobhijog.fragment.FragmentAskQuestion;
import bd.org.blast.sromikobhijog.utilis.Utils;

public class ApplicationFormActivity extends AppCompatActivity  implements FragmentAskQuestion.OnItemSelectListener{

    private static final String TAG = ApplicationFormActivity.class.getSimpleName();
    public static final String TAG_QUESTION = "question";
    public static final String TAG_QUESTION_ID = "question_id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);
        /*Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            if (Utils.getinfo(this).getUserInfo().getRole().equalsIgnoreCase(Constant.USER_ADMIN ))
                showApplicationList();
            else
                showAskQuestion();

        }catch (Exception e){
            e.printStackTrace();
            showAskQuestion();
        }






    }


    public void showAskQuestion(){
        Fragment fragment = new FragmentAskQuestion();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
        ft.replace(R.id.fragment_placeholder, fragment, TAG);
        ft.addToBackStack(null);
        ft.commit();//commitAllowingStateLoss();

        currentFragment =form;
    }

    public void showApplicationList() {
        Fragment fragment = new ApplicationListFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
        ft.replace(R.id.fragment_placeholder, fragment, TAG);
        ft.addToBackStack(null);
        ft.commit();//commitAllowingStateLoss();
        currentFragment =list;


    }



    @Override
    public void onItemSelected(String question_id, String question) {

        Fragment fragment = new ApplicationFormFragment();
        Bundle args= new Bundle();
        args.putString(TAG_QUESTION, question);
        args.putString(TAG_QUESTION_ID, question_id);
        fragment.setArguments(args);

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,android.R.anim.fade_out);
        fragmentTransaction.replace(R.id.fragment_placeholder, fragment, TAG);
        fragmentTransaction.commitAllowingStateLoss();

    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.form_action_menu, menu);
        return true;
    }

    int currentFragment=0;
    private final int form =0, list=1;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {

            //showApplicationList();
            if (item.getTitle().toString().equals(getString(R.string.list))){
                item.setTitle(getString(R.string.form));
                item.setIcon(R.mipmap.ic_application);
                showApplicationList();
            }else{
                item.setTitle(getString(R.string.list));
                item.setIcon(R.mipmap.ic_application_list);
                showAskQuestion();
            }
            Log.e(TAG, "onOptionsItemSelected: "+ item.getTitle().toString() );


            return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
