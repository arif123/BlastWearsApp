package bd.org.blast.sromikobhijog;

import android.app.Application;

import com.facebook.stetho.Stetho;
import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/24/2017.
 */

public class MyApplication extends Application {
    public void onCreate() {
        super.onCreate();
        //Fabric.with(this, new Crashlytics());

        final Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);
        Stetho.initializeWithDefaults(this);
    }
}