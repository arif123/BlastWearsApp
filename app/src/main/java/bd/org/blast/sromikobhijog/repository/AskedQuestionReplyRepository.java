package bd.org.blast.sromikobhijog.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.db.DbHelper;
import bd.org.blast.sromikobhijog.model.AskedQuestionReply;
import bd.org.blast.sromikobhijog.response.ResponseAskQuestion;
import bd.org.blast.sromikobhijog.response.ResponseAskQuestionReply;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/29/2017.
 */

public class AskedQuestionReplyRepository {

    private static final String TAG=AskedQuestionReplyRepository.class.getSimpleName();


    public static void insert(Context context, List<AskedQuestionReply> list){
        if (list==null || list.isEmpty())return;

        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(AskedQuestionReply item: list){
            insert(context, item);
        }
        Log.i(TAG, "insert: "+list.size() );
    }


    public static void insert(Context context, AskedQuestionReply item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        ContentValues cv = itemToContentValue(item);

        if (item.getAppsAskQuesResponseId()==null || item.getAppsAskQuesResponseId().equals("0")){
            long newAppsApplicationId= getLastRowID(context, DB.AskedQuestionReply.TABLE)+1;
            cv.put(DB.AskedQuestionReply.appsAskQuesResponseId , newAppsApplicationId);

        }
        else
            cv.put(DB.AskedQuestionReply.appsAskQuesResponseId  , item.getAppsAskQuesResponseId());



         long l=  db.insert(DB.AskedQuestionReply.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert id: "+l );

    }

    public static ContentValues itemToContentValue(AskedQuestionReply item){
        ContentValues cv = new ContentValues();

        cv.put(DB.AskedQuestionReply.responseId , item.getResponseId());
        cv.put(DB.AskedQuestionReply.appsAskQuesResponseId , item.getAppsAskQuesResponseId());
        cv.put(DB.AskedQuestionReply.responseText , item.getResponseText());
        cv.put(DB.AskedQuestionReply.askQuestionId , item.getAskQuestionId());
        cv.put(DB.AskedQuestionReply.questionText , item.getQuestionText());
        cv.put(DB.AskedQuestionReply.fromMobile , item.getFromMobile());
        cv.put(DB.AskedQuestionReply.questionChannel , item.getQuestionChannel());
        cv.put(DB.AskedQuestionReply.askQuesStatus  , item.getAskQuesStatus());
        cv.put(DB.AskedQuestionReply.createdAt  , item.getCreatedAt());
        // TODO: 11/29/2017 add sync value and date
        //cv.put(DB.AskedQuestionReply.syncDate  , item.get());
        //cv.put(DB.AskedQuestionReply.sync  , item.get());



        return  cv;
    }

    public static void update(Context context, ResponseAskQuestionReply body) {

        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        ContentValues cv = new ContentValues();



        cv.put(DB.AskedQuestionReply.responseId  , body.getAskQuestionResponseId());

        cv.put(DB.AskedQuestionReply.sync, "y");


        int l = db.update(DB.AskedQuestionReply.TABLE, cv, DB.AskedQuestionReply.appsAskQuesResponseId + "=?", new String[]{body.getAppsAskQuesResponseId()});

        Log.i(TAG, "updated: "+l );
    }

    public static List<AskedQuestionReply> getAll(Context context, String condition, String... args){
        List<AskedQuestionReply> list = new ArrayList<>();
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        Cursor cursor = null;

        if (null== condition)
            cursor = db.query("select * from "+DB.AskedQuestionReply.TABLE , args);
        else
            cursor = db.query("select * from "+DB.AskedQuestionReply.TABLE+ " where "+condition, args);

        System.out.println("select * from "+DB.AskedQuestionReply.TABLE+" where "+condition);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                AskedQuestionReply aq = new AskedQuestionReply();


                aq.setResponseId(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.responseId  )));
                aq.setAppsAskQuesResponseId(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.appsAskQuesResponseId  )));
                aq.setResponseText(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.responseText  )));
                aq.setAskQuestionId(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.askQuestionId  )));
                aq.setQuestionText(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.questionText  )));
                aq.setFromMobile(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.fromMobile  )));
                aq.setQuestionChannel(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.questionChannel  )));
                aq.setAskQuesStatus(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.askQuesStatus   )));
                aq.setCreatedAt(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.createdAt   )));

                // TODO: 11/29/2017 add sync value and date
                //aq.set(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.syncDate   )));
                //aq.set(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.sync   )));





                list.add(aq);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static List<AskedQuestionReply> getUnsynced(Context context){
        return getAll(context, DB.AskedQuestionReply.sync+" IS NULL ", null);
    }


    public static List<AskedQuestionReply> getRow(Context context, String askQuestionId){
        return getAll(context, DB.AskedQuestionReply.askQuestionId+" =?",new String[]{askQuestionId});
    }



    public static List<AskedQuestionReply>  getAll(Context context){
        List<AskedQuestionReply> list = new ArrayList<>();

        SQLiteDatabase db = DbHelper.getDbHelper(context).getReadableDatabase();
        list.clear();
        Cursor cursor = db.rawQuery("select * from "+DB.AskedQuestionReply.TABLE+" ORDER BY "+DB.AskedQuestionReply.appsAskQuesResponseId, null);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                AskedQuestionReply aq = new AskedQuestionReply();

                aq.setResponseId(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.responseId  )));
                aq.setAppsAskQuesResponseId(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.appsAskQuesResponseId  )));
                aq.setResponseText(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.responseText  )));
                aq.setAskQuestionId(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.askQuestionId  )));
                aq.setQuestionText(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.questionText  )));
                aq.setFromMobile(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.fromMobile  )));
                aq.setQuestionChannel(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.questionChannel  )));
                aq.setAskQuesStatus(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.askQuesStatus   )));
                aq.setCreatedAt(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.createdAt   )));
                aq.setAskQuestionStatus(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.askQuestionStatus   )));
                // TODO: 11/29/2017 add sync value and date
                //aq.set(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.syncDate   )));
                //aq.set(cursor.getString(cursor.getColumnIndex(DB.AskedQuestionReply.sync   )));



                list.add(aq);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }


    public static  synchronized long getLastRowID(Context context,String Table){
        long lastId = 0;
        String query = "SELECT ROWID from "+ Table+ " order by ROWID DESC limit 1";
        BriteDatabase db =BriteDbHelper.getBriteDatabase(context);
        Cursor c = null;
        try {
            c = db.query(query);
            if (c != null && c.moveToFirst()) {
                lastId = c.getLong(0); //The 0 is the column index, we only have 1 column, so the index is 0
            }
        }catch (Exception e){e.printStackTrace();}
        finally {
            if (c!=null)
                c.close();
        }

        return lastId;
    }
}
