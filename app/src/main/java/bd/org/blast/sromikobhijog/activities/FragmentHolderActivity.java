package bd.org.blast.sromikobhijog.activities;

import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import bd.org.blast.sromikobhijog.ItemListDialogFragment;
import bd.org.blast.sromikobhijog.R;

/**
 * Created by This pc on 8/7/2017.
 */

public class FragmentHolderActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_holder);




    }

    public void showBottomSheet(View view) {
        //Initializing a bottom sheet
        BottomSheetDialogFragment bottomSheetDialogFragment = new ItemListDialogFragment();

        //show it
        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
    }
}
