package bd.org.blast.sromikobhijog.http;

import bd.org.blast.sromikobhijog.response.AuthResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/25/2017.
 */

public interface AuthService {

        @FormUrlEncoded
        @POST("sign_in")
        Call<AuthResponse> signin(@Field("user_name") String username,
                                 @Field("password") String password);


}
