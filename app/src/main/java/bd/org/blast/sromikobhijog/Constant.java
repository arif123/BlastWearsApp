package bd.org.blast.sromikobhijog;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/2/2017.
 */

public class Constant {
    public static final int MONTH_OFFSET = 1;
    public static final String SMS_PREFIX = "JIGYASHA";
    public static final String CHANNEL_SMS = "SMS";
    public static final String CHANNEL_APP = "APP-TO-APP";

    public static final String USER_ADMIN = "admin";
    public static final String USER_LAWYER = "Staff Lawyer";
    public static final String USER_TU_LEADER = "TU Leader";


    public static final String NOTIFICATION_TYPE_APPLICATION = "APPLICATION";
    public static final String NOTIFICATION_TYPE_QUESTION = "QUESTION";

}
