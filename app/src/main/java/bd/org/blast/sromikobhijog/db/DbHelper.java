package bd.org.blast.sromikobhijog.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/24/2017.
 */

public class DbHelper extends SQLiteOpenHelper{
    private static String DB_NAME= "bso.db";
    private static int VERSION = 1;

    public static DbHelper getDbHelper(Context context) {
        if (dbHelper== null)
            dbHelper = new DbHelper(context);

        return dbHelper;
    }

    private static  DbHelper dbHelper;

    private DbHelper(Context context){
        super(context, DB_NAME,null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(DB.Category.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.Topic.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.QuestionAnswer.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.Area.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.UserArea.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.SalaryRange.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.Question.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.Applications.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.AskedQuestion.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.ServiceProviderCategory.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.ServiceProvider.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.Sync.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.AskedQuestionReply.SQL_CREATE);
        sqLiteDatabase.execSQL(DB.SmsNotificationLog.SQL_CREATE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL(DB.Category.SQL_DROP);
        sqLiteDatabase.execSQL(DB.Topic.SQL_DROP);
        sqLiteDatabase.execSQL(DB.QuestionAnswer.SQL_DROP);
        sqLiteDatabase.execSQL(DB.Area.SQL_DROP);
        sqLiteDatabase.execSQL(DB.UserArea.SQL_DROP);
        sqLiteDatabase.execSQL(DB.SalaryRange.SQL_DROP);
        sqLiteDatabase.execSQL(DB.Question.SQL_DROP);
        sqLiteDatabase.execSQL(DB.Applications.SQL_DROP);
        sqLiteDatabase.execSQL(DB.AskedQuestion.SQL_DROP);
        sqLiteDatabase.execSQL(DB.ServiceProviderCategory.SQL_DROP);
        sqLiteDatabase.execSQL(DB.ServiceProvider.SQL_DROP);
        sqLiteDatabase.execSQL(DB.Sync.SQL_DROP);
        sqLiteDatabase.execSQL(DB.AskedQuestionReply.SQL_DROP);
        sqLiteDatabase.execSQL(DB.SmsNotificationLog.SQL_DROP);

        onCreate(sqLiteDatabase);

    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
