package bd.org.blast.sromikobhijog.model;

/**
 * Created by This pc on 8/7/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import bd.org.blast.sromikobhijog.db.DB;

public class Application {


    @SerializedName("user_name")
    @Expose
    private String userName;


    @SerializedName("application_id")
    @Expose
    private int applicationId;
    @SerializedName("question_id")
    @Expose
    private int questionId;
    @SerializedName("question_text")
    @Expose
    private String questionText;
    @SerializedName("applicant_name")
    @Expose
    private String applicantName;
    @SerializedName("designation")
    @Expose
    private String designation;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("alternative_mobile")
    @Expose
    private String alternativeMobile;
    @SerializedName("salary_range")
    @Expose
    private String salaryRange;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("age")
    @Expose
    private int age;
    @SerializedName("joining_date")
    @Expose
    private String joiningDate;
    @SerializedName("factory_name")
    @Expose
    private String factoryName;
    @SerializedName("factory_address")
    @Expose
    private String factoryAddress;
    @SerializedName("area_id")
    @Expose
    private int areaId;
    @SerializedName("application_submitted_by")
    @Expose
    private String applicationSubmittedBy;
    @SerializedName("application_status")
    @Expose
    private String applicationStatus;
    @SerializedName("apps_application_id")
    @Expose
    private int appsApplicationId;
    @SerializedName("sync_datetime")
    @Expose
    private String syncDatetime;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    @Expose
    private String sync;

    @Expose
    private String statusSync;


    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public String getApplicantName() {
        return applicantName;
    }

    public void setApplicantName(String applicantName) {
        this.applicantName = applicantName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getAlternativeMobile() {
        return alternativeMobile;
    }

    public void setAlternativeMobile(String alternativeMobile) {
        this.alternativeMobile = alternativeMobile;
    }

    public String getSalaryRange() {
        return salaryRange;
    }

    public void setSalaryRange(String salaryRange) {
        this.salaryRange = salaryRange;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public String getFactoryAddress() {
        return factoryAddress;
    }

    public void setFactoryAddress(String factoryAddress) {
        this.factoryAddress = factoryAddress;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getApplicationSubmittedBy() {
        return applicationSubmittedBy;
    }

    public void setApplicationSubmittedBy(String applicationSubmittedBy) {
        this.applicationSubmittedBy = applicationSubmittedBy;
    }

    public String getApplicationStatus() {
        return applicationStatus;
    }

    public void setApplicationStatus(String applicationStatus) {
        this.applicationStatus = applicationStatus;
    }

    public int getAppsApplicationId() {
        return appsApplicationId;
    }

    public void setAppsApplicationId(int appsApplicationId) {
        this.appsApplicationId = appsApplicationId;
    }

    public String getSyncDatetime() {
        return syncDatetime;
    }

    public void setSyncDatetime(String syncDatetime) {
        this.syncDatetime = syncDatetime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getSync() {
        return sync;
    }

    public void setSync(String sync) {
        this.sync = sync;
    }


    public String getStatusSync() {
        return statusSync;
    }

    public void setStatusSync(String statusSync) {
        this.statusSync = statusSync;
    }

    @Override
    public String toString() {
        return applicantName+ designation + questionText + factoryName + factoryAddress + mobileNumber+ applicationStatus ;
    }
}
