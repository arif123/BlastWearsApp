
package bd.org.blast.sromikobhijog.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseAskQuestion {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ask_question_id")
    @Expose
    private Integer askQuestionId;
    @SerializedName("apps_ask_question_id")
    @Expose
    private String appsAskQuestionId;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getAskQuestionId() {
        return askQuestionId;
    }

    public void setAskQuestionId(Integer askQuestionId) {
        this.askQuestionId = askQuestionId;
    }

    public String getAppsAskQuestionId() {
        return appsAskQuestionId;
    }

    public void setAppsAskQuestionId(String appsAskQuestionId) {
        this.appsAskQuestionId = appsAskQuestionId;
    }

}
