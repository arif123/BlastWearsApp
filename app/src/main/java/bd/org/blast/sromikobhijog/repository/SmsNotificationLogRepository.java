package bd.org.blast.sromikobhijog.repository;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.squareup.sqlbrite2.BriteDatabase;

import java.util.ArrayList;
import java.util.List;

import bd.org.blast.sromikobhijog.db.BriteDbHelper;
import bd.org.blast.sromikobhijog.db.DB;
import bd.org.blast.sromikobhijog.db.DbHelper;
import bd.org.blast.sromikobhijog.model.SmsNotificationLog;
import bd.org.blast.sromikobhijog.response.ResponseSmsNotificationLog;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 10/24/2017.
 */

public class SmsNotificationLogRepository {

    private static final String TAG=SmsNotificationLogRepository.class.getSimpleName();


    public static void insert(Context context, List<SmsNotificationLog> list){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);
        for(SmsNotificationLog item: list){
            insert(context, item);
        }
        Log.i(TAG, "insert: "+list.size() );
    }


    public static void insert(Context context, SmsNotificationLog item){
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        ContentValues cv = itemToContentValue(item);

        if (item.getAppsNotificationLogId()==null || item.getAppsNotificationLogId().equals("0")){
            long newAppsApplicationId= getLastRowID(context, DB.SmsNotificationLog.TABLE)+1;
            cv.put(DB.SmsNotificationLog.appsNotificationLogId , newAppsApplicationId);

        }
        else
            cv.put(DB.SmsNotificationLog.appsNotificationLogId  , item.getAppsNotificationLogId());



         long l=  db.insert(DB.SmsNotificationLog.TABLE, cv, SQLiteDatabase.CONFLICT_REPLACE);

        Log.i(TAG, "insert id: "+l );

    }

    public static ContentValues itemToContentValue(SmsNotificationLog item){
        ContentValues cv = new ContentValues();


        cv.put(DB.SmsNotificationLog.appsNotificationLogId , item.getAppsNotificationLogId());
        cv.put(DB.SmsNotificationLog.logId  , item.getLogId());
        cv.put(DB.SmsNotificationLog.smsFrom  , item.getSmsFrom());
        cv.put(DB.SmsNotificationLog.smsTo  , item.getSmsTo());
        cv.put(DB.SmsNotificationLog.smsText  , item.getSmsText());
        cv.put(DB.SmsNotificationLog.notificationFor  , item.getNotificationFor());
        cv.put(DB.SmsNotificationLog.applicationOrQuestionId  , item.getApplicationOrQuestionId());
        cv.put(DB.SmsNotificationLog.createdBy  , item.getCreatedBy());
        cv.put(DB.SmsNotificationLog.createdAt  , item.getCreatedAt());
        cv.put(DB.SmsNotificationLog.sync  , item.getSync());



        return  cv;
    }

    public static void update(Context context, ResponseSmsNotificationLog body) {

        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        ContentValues cv = new ContentValues();



        cv.put(DB.SmsNotificationLog.logId  , body.getNotificationLogId());

        cv.put(DB.SmsNotificationLog.sync, "y");


        db.update(DB.SmsNotificationLog.TABLE, cv, DB.SmsNotificationLog.appsNotificationLogId+"=?" , new String[]{body.getAppsNotificationLogId()});

        Log.i(TAG, "updated: "+1 );
    }

    public static List<SmsNotificationLog> getAll(Context context, String condition, String... args){
        List<SmsNotificationLog> list = new ArrayList<>();
        BriteDatabase db = BriteDbHelper.getBriteDatabase(context);

        Cursor cursor = null;

        if (null== condition)
            cursor = db.query("select * from "+DB.SmsNotificationLog.TABLE , args);
        else
            cursor = db.query("select * from "+DB.SmsNotificationLog.TABLE+ " where "+condition, args);

        System.out.println("select * from "+DB.SmsNotificationLog.TABLE+" where "+condition);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                SmsNotificationLog aq = new SmsNotificationLog();


                aq.setAppsNotificationLogId( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.appsNotificationLogId  )));
                aq.setLogId( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.logId  )));
                aq.setSmsFrom( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.smsFrom   )));
                aq.setSmsTo( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.smsTo   )));
                aq.setSmsText( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.smsText  )));
                aq.setNotificationFor( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.notificationFor   )));
                aq.setApplicationOrQuestionId( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.applicationOrQuestionId   )));
                aq.setCreatedBy( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.createdBy  )));
                aq.setCreatedAt( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.createdAt   )));
                aq.setSync( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.sync   )));





                list.add(aq);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }

    public static List<SmsNotificationLog> getUnsynced(Context context){
        return getAll(context, DB.SmsNotificationLog.sync+" IS NULL ", null);
    }



    public static List<SmsNotificationLog>  getAll(Context context){
        List<SmsNotificationLog> list = new ArrayList<>();

        SQLiteDatabase db = DbHelper.getDbHelper(context).getReadableDatabase();
        list.clear();
        Cursor cursor = db.rawQuery("select * from "+DB.SmsNotificationLog.TABLE+" ORDER BY "+DB.SmsNotificationLog.appsNotificationLogId, null);
        if(cursor!=null && cursor.moveToFirst()){
            do{
                SmsNotificationLog aq = new SmsNotificationLog();

                aq.setAppsNotificationLogId( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.appsNotificationLogId  )));
                aq.setLogId( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.logId  )));
                aq.setSmsFrom( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.smsFrom   )));
                aq.setSmsTo( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.smsTo   )));
                aq.setSmsText( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.smsText  )));
                aq.setNotificationFor( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.notificationFor   )));
                aq.setApplicationOrQuestionId( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.applicationOrQuestionId   )));
                aq.setCreatedBy( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.createdBy  )));
                aq.setCreatedAt( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.createdAt   )));
                aq.setSync( cursor.getString(cursor.getColumnIndex( DB.SmsNotificationLog.sync   )));


                list.add(aq);
            }while(cursor.moveToNext());
            cursor.close();
        }

        return list;
    }


    public static  synchronized long getLastRowID(Context context,String Table){
        long lastId = 0;
        String query = "SELECT ROWID from "+ Table+ " order by ROWID DESC limit 1";
        BriteDatabase db =BriteDbHelper.getBriteDatabase(context);
        Cursor c = null;
        try {
            c = db.query(query);
            if (c != null && c.moveToFirst()) {
                lastId = c.getLong(0); //The 0 is the column index, we only have 1 column, so the index is 0
            }
        }catch (Exception e){e.printStackTrace();}
        finally {
            if (c!=null)
                c.close();
        }

        return lastId;
    }


    public static boolean isNotificationSent(Context context, String applicationOrQuestionId, String notificationFor){
        List<SmsNotificationLog> all = getAll(context, DB.SmsNotificationLog.applicationOrQuestionId + " like ?  AND "+ DB.SmsNotificationLog.notificationFor+" like ?"  , new String[]{applicationOrQuestionId, notificationFor});
        if (all != null && !all.isEmpty() && all.get(0) != null && all.get(0).getApplicationOrQuestionId() != null && !all.get(0).getApplicationOrQuestionId().equals(""))
            return true;
        else
            return false;

    }
}
