package bd.org.blast.sromikobhijog.adapter;

/**
 * Created by This pc on 8/7/2017.
 */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.model.ServiceProviderList;

// ServiceProviderAdapter

public class ServiceProviderAdapter extends ArrayAdapter<ServiceProviderList> {
    // View lookup cache
    private static class ViewHolder {
        TextView tvTitle;
        TextView tvAddress;
        TextView tvtype;
        TextView tvContactNo;
        TextView tvEmail;
    }

    public ServiceProviderAdapter(Context context, ArrayList<ServiceProviderList> serviceProviders) {
        super(context, R.layout.list_item_service_provider, serviceProviders);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        ServiceProviderList serviceProvider = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            // If there's no view to re-use, inflate a brand new view for row
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.list_item_service_provider, parent, false);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitle);
            viewHolder.tvAddress = (TextView) convertView.findViewById(R.id.tvAddress);
            viewHolder.tvtype = (TextView) convertView.findViewById(R.id.tvtype);
            viewHolder.tvContactNo = (TextView) convertView.findViewById(R.id.tvContactNo);
            viewHolder.tvEmail = (TextView) convertView.findViewById(R.id.tvEmail);

            // Cache the viewHolder object inside the fresh view
            convertView.setTag(viewHolder);
        } else {
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();
        }
        // Populate the data from the data object via the viewHolder object
        // into the template view.
        viewHolder.tvTitle.setText(serviceProvider.getServiceProviderCategory().get(position).getCategoryNameBn());
        viewHolder.tvAddress.setText(serviceProvider.getServiceProviderCategory().get(position).getCategoryNameEn());
        viewHolder.tvtype.setText("");
        viewHolder.tvContactNo.setText("");
        viewHolder.tvEmail.setText("");

        // Return the completed view to render on screen
        return convertView;
    }
}
