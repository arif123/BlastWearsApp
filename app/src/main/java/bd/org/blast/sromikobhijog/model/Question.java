
package bd.org.blast.sromikobhijog.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Question {

    @SerializedName("question_id")
    @Expose
    private String questionId;
    @SerializedName("question_en")
    @Expose
    private String questionEn;
    @SerializedName("question_bn")
    @Expose
    private String questionBn;
    @SerializedName("publish_status")
    @Expose
    private String publishStatus;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getQuestionEn() {
        return questionEn;
    }

    public void setQuestionEn(String questionEn) {
        this.questionEn = questionEn;
    }

    public String getQuestionBn() {
        return questionBn;
    }

    public void setQuestionBn(String questionBn) {
        this.questionBn = questionBn;
    }

    public String getPublishStatus() {
        return publishStatus;
    }

    public void setPublishStatus(String publishStatus) {
        this.publishStatus = publishStatus;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

}
