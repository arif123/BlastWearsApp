
package bd.org.blast.sromikobhijog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SmsNotificationLogData {

    @SerializedName("all_sms_notification")
    @Expose
    private List<SmsNotificationLog> allSmsNotification = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<SmsNotificationLog> getAllSmsNotification() {
        return allSmsNotification;
    }

    public void setAllSmsNotification(List<SmsNotificationLog> allSmsNotification) {
        this.allSmsNotification = allSmsNotification;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
