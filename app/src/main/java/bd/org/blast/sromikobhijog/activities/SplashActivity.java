package bd.org.blast.sromikobhijog.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.sync.SyncAdapter;

/**
 * Created by Arif on 10/15/2017.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_splash);
        ImageView imageView = (ImageView) findViewById(R.id.imageView);

        Glide.with(this).load(R.drawable.splash).into(imageView);

        //initialize sync adapter
        SyncAdapter.initializeSyncAdapter(getApplicationContext());

    }

    @Override
    protected void onStart() {
        super.onStart();

        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {
                //AnimationTween.zoomInOut(view, LauncherActivity.this);

            }

            public void onFinish() {
/*
                String init= Utils.getPreference( SplashActivity.this, Utils.KEY_INIT);
                if (init.equals("1")){
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }else {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }*/

                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }.start();


    }
}
