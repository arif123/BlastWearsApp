package bd.org.blast.sromikobhijog.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import java.util.List;

import bd.org.blast.sromikobhijog.R;
import bd.org.blast.sromikobhijog.model.Calllog;
import bd.org.blast.sromikobhijog.repository.CallLogRepository;
import bd.org.blast.sromikobhijog.repository.SyncRepository;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arif Hossen<arif123@gmail.com> on 11/19/2017.
 */

public class CallLogAct extends AppCompatActivity {

    private static final int REQUEST_READ_CALL_LOG = 100;
    private static final String TAG = CallLogAct.class.getSimpleName();
    @BindView(R.id.call)
    TextView call;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_call_log);

        ButterKnife.bind(this);

        call.setText("call log");


        SyncRepository.insert(this, CallLogRepository.tableName);

        alert();




    }

    private void alert(){
        AlertDialog.Builder builder= new AlertDialog.Builder(this);
        AlertDialog alertDialog = builder.create();
        alertDialog.setTitle("Hell!");
        alertDialog.setMessage("'READ CALL LOG' permission is require to sync 'Call Log' to server?");
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                accessCallLog();
            }
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Ignore", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                AlertDialog.Builder alert = new AlertDialog.Builder(CallLogAct.this);
                alert.setTitle("Message");
                alert.setMessage("Call Log will not sync to server");
                alert.setPositiveButton("OK", null);
                alert.show();

            }
        });

        alertDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                alert();
            }
        });

        alertDialog.show();
    }


    private void accessCallLog(){
        if (!mayRequestCallLog()) {
            return;
        }
        // This Build is < 6 , you can Access contacts here.
        getCallDetails();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_CALL_LOG: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                   getCallDetails();
                }
                else
                {
                    Log.d(TAG, "onRequestPermissionsResult: REQUEST_READ_CALL_LOG permission denied " );

                }
                return;
            }
        }
    }


    private boolean mayRequestCallLog() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(Manifest.permission.READ_CALL_LOG)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CALL_LOG},REQUEST_READ_CALL_LOG);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CALL_LOG},REQUEST_READ_CALL_LOG);
        }
        return false;
    }


    private void getCallDetails() {

        List<Calllog> all = CallLogRepository.getAll(this, SyncRepository.getRow(this, CallLogRepository.tableName));
        StringBuffer sb = new StringBuffer();
        for (Calllog calllog : all) {
            sb.append("\n"+calllog);
        }


        /*Cursor managedCursor = managedQuery(CallLog.Calls.CONTENT_URI, null, null, null, CallLog.Calls.DATE+" desc");


        StringBuffer sb = new StringBuffer();
        int id = managedCursor.getColumnIndex(CallLog.Calls._ID);
        int number = managedCursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = managedCursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = managedCursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = managedCursor.getColumnIndex(CallLog.Calls.DURATION);
        sb.append("Call Details :");
        while (managedCursor.moveToNext()) {
            int log_id = managedCursor.getInt(id);
            String phNumber = managedCursor.getString(number);
            String callType = managedCursor.getString(type);
            String callDate = managedCursor.getString(date);
            Date callDayTime = new Date(Long.valueOf(callDate));
            String callDuration = managedCursor.getString(duration);
            String dir = null;
            int dircode = Integer.parseInt(callType);
            switch (dircode) {
                case CallLog.Calls.OUTGOING_TYPE:
                    dir = "OUTGOING";
                    break;

                case CallLog.Calls.INCOMING_TYPE:
                    dir = "INCOMING";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    dir = "MISSED";
                    break;
            }
            sb.append("\n id: "+log_id+"\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
            sb.append("\n----------------------------------");
        }
        managedCursor.close();*/
        call.setText(sb);
    }
}
