
package bd.org.blast.sromikobhijog.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class QuestionAnswerData {

    @SerializedName("ques_ans_list")
    @Expose
    private List<QuestionAnswer> quesAns = null;
    @SerializedName("success")
    @Expose
    private Integer success;

    public List<QuestionAnswer> getQuesAns() {
        return quesAns;
    }

    public void setQuesAns(List<QuestionAnswer> quesAns) {
        this.quesAns = quesAns;
    }

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

}
